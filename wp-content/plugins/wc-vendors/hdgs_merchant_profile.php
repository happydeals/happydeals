<?php

/************************
**** MERCHANT PROFILE ***
************************/

//get shared functions
include_once "hdgs_functions.php";

//render the input fields in the form
add_action( 'show_user_profile', 'hdgs_render_user_info' );
add_action( 'edit_user_profile', 'hdgs_render_user_info' );
function hdgs_render_user_info($user) {

	//admin views
	if (is_super_admin()) {
		
		//admin view of merchant profiles
		if (in_array('vendor', $user->roles)) {
			include_once "hdgs_merchant_dashboard.php";
			$hdgs_active_package = get_user_meta($user->ID, 'hdgs_active_package', true);
			hdgs_show_profile_styling();
			hdgs_add_stats_dashboard_widget($user->ID);
			hdgs_show_account_info($user);
			hdgs_show_charges($user);
			if ($hdgs_active_package == '1-LED') {
				hdgs_show_led_balance($user->ID);
				hdgs_show_checked_products( $user->ID );
			}
			hdgs_show_last_ip_addresses($user); 

		//admin view of branch profiles
		} elseif (in_array('branch_account', $user->roles)) {
			hdgs_show_account_info($user);
			hdgs_show_last_ip_addresses($user);
		}

	//nonadmin views for merchant (aka viewing of own profile)
	} else {
		if (current_user_can('vendor')) {
			hdgs_show_account_info_to_merchant($user);
			hdgs_show_sales_type_to_merchant($user);
			hdgs_show_charges_to_merchant($user);
			hdgs_show_branch_users_to_merchant($user);
			hdgs_show_two_step_auth_to_merchant($user);
			hdgs_show_last_ip_addresses($user);
		} elseif (in_array('branch_account', $user->roles)) {
			remove_all_actions( 'admin_notices' );
		}
	}	
}

//styles to be applied to the fields
function hdgs_show_profile_styling(){

?>
	<style type="text/css">
		#add_new_charge:target,
		#add_account_info:target {
			display: none;
		}
		#add_new_charge:target ~ .add_new_charge,
		#add_account_info:target ~ .add_account_info {
			display:block !important;
		}
		.hdgs_charges td,
		.hdgs_account_info td {
			width: 210px;
			display: inline-block;
			vertical-align: top;
		}
		.hdgs_charges label,
		.hdgs_account_info label {
			font-weight: bold;
		}
		.hdgs_charges input[type="checkbox"] {
			float:left;
		}
		.hdgs_account_info .hdgs_can_post_goods {
			margin-left: 3em;
		}
	</style>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			$('input').blur();
			$(".hdgs_product_list #check_all_products").click(function () {
			   $('.hdgs_product_list input:checkbox').not(this).prop('checked', this.checked);
			});
		});
	</script>
<?php
}

function hdgs_show_account_info_to_merchant($user) {

	//get data
	$hdgs_account_info = get_user_meta( $user->ID, 'hdgs_account_info', true );
	$new_fields = get_user_meta($user->ID, 'hdgs_new_account_fields', true);

	$merchant_photo = get_user_meta($user->ID, 'hdgs_merchant_photo', true);

	$hdgs_can_post_deals = get_user_meta( $user->ID, 'hdgs_can_post_deals', true );
	$hdgs_can_post_goods = get_user_meta( $user->ID, 'hdgs_can_post_goods', true );

	if ($hdgs_can_post_deals AND $hdgs_can_post_goods) {
		$sales_category = 'Deals and Goods';
	} elseif ($hdgs_can_post_deals) {
		$sales_category = 'Deals';
	} elseif ($hdgs_can_post_goods) {
		$sales_category = 'Goods';
	}

	$state = get_user_meta($user->ID, 'shipping_state', true);
	$country = get_user_meta($user->ID, 'shipping_country', true);

	$province = get_user_meta($user->ID, 'pickup_province', true);
	if (!$province) {
		$province = WC()->countries->states[$country][$state];
	}

	$city = get_user_meta($user->ID, 'pickup_city', true);
	if (!$city) {
		$city = get_user_meta($user->ID, 'shipping_city', true);
	}

	$bgy = get_user_meta($user->ID, 'pickup_brgy', true);
	if (!$bgy) {
		$bgy = get_user_meta($user->ID, 'shipping_address_1', true).PHP_EOL;
		$bgy .= get_user_meta($user->ID, 'shipping_address_2', true);
	}

	$zipcode = get_user_meta($user->ID, 'pickup_zipcode', true);
	if (!$zipcode) {
		$zipcode = get_user_meta($user->ID, 'shipping_postcode', true);
	}

	if ($hdgs_account_info['date_registered']) {
		$date_reg = date('F j, Y',strtotime($hdgs_account_info['date_registered']));
	}

?>
	<h2>Account Information</h2>
	<img src="<?php $merchant_photo; ?>" style="float:right;"/>
	<table class="form-table hdgs_account_info"><tbody>
		<tr>
			<td><label>Company Name</label></td>
			<td><?php echo $hdgs_account_info['company_name']; ?></td>
		</tr>
		<tr>
			<td><label>Company Legal Address</label></td>
			<td><?php echo $hdgs_account_info['legal_address']; ?></td>
		</tr>
		<tr>
			<td><label>Pick up Address</label></td>
		</tr>
		<tr>
			<td><label style="margin-left:3em;">Province</label></td>
			<td><?php echo $province; ?></td>
		</tr>
		<tr>
			<td><label style="margin-left:3em;">City, Municipality</label></td>
			<td><?php echo $city; ?></td>
		</tr>
		<tr>
			<td><label style="margin-left:3em;">Barangay</label></td>
			<td><?php echo $bgy; ?></td>
		</tr>
		<tr>
			<td><label style="margin-left:3em;">Zip Code</label></td>
			<td><?php echo $zipcode; ?></td>
		</tr>
		<tr>
			<td><label>Contact Person</label></td>
			<td><?php echo $hdgs_account_info['contact_person']; ?></td>
		</tr>
		<tr>
			<td><label>Contact No.</label></td>
			<td><?php echo $hdgs_account_info['contact_number']; ?></td>
		</tr>
		<tr>
			<td><label>Email Address</label></td>
			<td><?php echo $hdgs_account_info['contact_email']; ?></td>
		</tr>
		<tr>
			<td><label>Date Registered</label></td>
			<td><?php echo $date_reg; ?></td>
		</tr>
		<tr>
			<td><label>Merchant Sales Category</label></td>
			<td><?php echo $sales_category; ?></td>
		</tr>
	</tbody></table>
<?php
}

//add sales info to order pages in admin area
add_action( 'add_meta_boxes_shop_order', 'hdgs_add_sales_type_to_shop_order' );
function hdgs_add_sales_type_to_shop_order() {
  add_meta_box( 'hdgs_add_sales_type_to_shop_order', 
              __('Sales Type','hdgs_text_domain'), 
              'hdgs_show_sales_type_admin', 
              'shop_order',
              'side'
              );
}
function hdgs_show_sales_type_admin ($post) {
	$is_metabox = true;
	$merchant_ids = hdgs_get_merchants_from_order($post->ID);
	foreach ($merchant_ids as $merchant_id) {
		$user = get_user_by('id',$merchant_id);
		hdgs_show_sales_type_to_merchant ($user, $is_metabox);
		echo '<hr/>';
	}
}

function hdgs_show_sales_type_to_merchant($user, $is_metabox = false) {

	$hdgs_active_package = get_user_meta($user->ID, 'hdgs_active_package', true);


	$hdgs_effectivity_date = get_user_meta($user->ID, 'hdgs_effectivity_date', true);
	if ($hdgs_effectivity_date) {
		$effectivity_date = date('F j, Y',strtotime($hdgs_effectivity_date));
	}

?>
	
	<?php if (!$is_metabox): ?>
		<h2>Sales Type</h2>
	<?php endif; ?>
	<table class="form-table hdgs_sales_type"><tbody>
		<tr>
			<td><label>Contract No.</label></td>
			<td><?php echo get_user_meta($user->ID, 'hdgs_contract_id', true); ?></td>
		</tr>
		<tr>
			<td><label>Active Package</label></td>
			<td><?php echo $hdgs_active_package; ?></td>
		</tr>
		<tr>
			<td><label>Status</label></td>
			<td><?php echo get_user_meta($user->ID, 'hdgs_account_status', true); ?></td>
		</tr>
		<tr>
			<td><label>Effectivity Date</label></td>
			<td><?php echo $effectivity_date; ?></td>
		</tr>
	</tbody></table>

<?php 

	if ($hdgs_active_package == '1-LED'){ 
		hdgs_show_led_balance($user->ID);
	}	
}

function hdgs_show_led_balance($user_id){
	$sales_to_date = hdgs_get_sales_to_date($user_id);

	$total_value = get_user_meta($user_id, 'hdgs_led_options_hdgs_led_total_value', true);

	$balance = $total_value - $sales_to_date;

	$completion = $sales_to_date/$total_value;

?>
	<table class="form-table hdgs_balance_info"><tbody>
		<tr>
			<td><label>Total Value</label></td>
			<td><?php echo 'P'.number_format($total_value,0,'.',','); ?></td>
		</tr>
		<tr>
			<td><label>Additional Notes</label></td>
			<td><?php echo get_user_meta($user_id, 'hdgs_led_options_hdgs_led_additional_notes', true); ?></td>
		</tr>
		<tr>
			<td><label>Gross Sales to Date</label></td>
			<td><?php echo 'P'.number_format($sales_to_date,0,'.',','); ?></td>
		</tr>
		<tr>
			<td><label>Balance</label></td>
			<td><?php echo 'P'.number_format($balance,0,'.',','); ?></td>
		</tr>
		<tr>
			<td><label>% Completion</label></td>
			<td><?php echo number_format($completion*100,0,'.',',').'%'; ?></td>
		</tr>
	</tbody></table>
<?php
}

function hdgs_show_charges_to_merchant($user) { 

	//get the charges, both the basic ones and the custom ones for this user
	$fields = hdgs_merge_input_lists($user->ID, hdgs_get_basic_charge_fields(), 'hdgs_new_charge_fields');

	//get the checkbox status
	$hdgs_charge_status = get_user_meta( $user->ID, 'hdgs_charge_status', true );
	$hdgs_charge_status = array_filter($hdgs_charge_status);

	foreach ($fields as $name => $description) {
		if ($hdgs_charge_status[$name]) {
			$charge_list[$name] = $description;
		}
	}
?>
	<h2>Applicable Charges</h2>
	<table class="form-table hdgs_charges"><tbody>
		<?php foreach ($charge_list as $name => $description): ?>
			<tr>
				<td><label><?php echo $description; ?></label></td>
			</tr>
		<?php endforeach; ?>
 	</tbody></table>

<?php 
}

function hdgs_show_two_step_auth_to_merchant($user) {
?>
	<h2>Two-Step Authentication</h2>
	<div class="hdgs_two_step">
		<a href="<?php echo admin_url('?page=two-factor-auth-user'); ?>">Enable Two Step Authentication here</a>
	</div>
<?php
}
//remove from merchant sidebar
add_action( 'admin_menu', 'hdgs_remove_two_factor_menu' );
function hdgs_remove_two_factor_menu(){
	if (current_user_can('vendor')) {
		remove_menu_page( 'two-factor-auth-user' );	
	}
}
//remove some styling
add_action('admin_head','hdgs_hide_two_factor_text');
function hdgs_hide_two_factor_text() {
	if (!current_user_can('vendor')) return; 
	?>
	<style>
		.toplevel_page_two-factor-auth-user .postbox > .inside:last-child,
		.toplevel_page_two-factor-auth-user #tfa_advanced_box,
		.toplevel_page_two-factor-auth-user .postbox ~ h2,
		.toplevel_page_two-factor-auth-user form > p:first-child {
			display:none;
		}
		.toplevel_page_two-factor-auth-user .simba_tfa_intro_notice strong {
			background:yellow;
			font-size: 1.5em;
		}
	</style>
	<?php
}

/*************************************************
/******* ADMIN VIEW OF MERCHANT PROFILES *********
*************************************************/

/******ACCOUNT INFO********/

function hdgs_show_account_info ($user) {

	//get data
	$hdgs_can_post_deals = get_user_meta( $user->ID, 'hdgs_can_post_deals', true );
	$hdgs_can_post_goods = get_user_meta( $user->ID, 'hdgs_can_post_goods', true );

	//get basic info and merge with user's custom info
	$fields = hdgs_merge_input_lists($user->ID, hdgs_get_basic_account_fields(), 'hdgs_new_account_fields');

	$hdgs_account_info = get_user_meta( $user->ID, 'hdgs_account_info', true );

?>
	<h2>Account Information</h2>
	<table class="form-table hdgs_account_info"><tbody>

		<tr>
			<td><label for="sales_category">Merchant Sales Category</label></td>
			<td>
				<input type="hidden" name="hdgs_can_post_deals" value="no"/>
				<input type="checkbox" name="hdgs_can_post_deals" id="hdgs_can_post_deals" value="yes" <?php if ($hdgs_can_post_deals) echo 'checked="checked"'; ?> /> Deals
				<input type="hidden" name="hdgs_can_post_goods" value="no"/> <input type="checkbox" name="hdgs_can_post_goods" id="hdgs_can_post_goods" value="yes" <?php if ($hdgs_can_post_goods) echo 'checked="checked"'; ?> /> Goods
			</td>
		</tr>


		<?php foreach ($fields as $name => $description): ?>
			<tr>
				<td><label for="<?php echo $name; ?>"><?php echo $description; ?></label></td>
				<td>
					<?php if ($name == 'legal_address'): ?>
						<textarea rows=8 cols=20 name="<?php echo $name; ?>" id="<?php echo $name; ?>" ><?php echo $hdgs_account_info[$name]; ?></textarea>
					<?php elseif ($name == 'date_registered'): ?>
						<input type="date" id="datepicker" name="<?php echo $name; ?>" value="<?php echo $hdgs_account_info[$name]; ?>" class="hdgs_datepicker" />
					<?php else: ?>
						<input type="text" name="<?php echo $name; ?>" id="<?php echo $name; ?>" value="<?php echo $hdgs_account_info[$name]; ?>" />
					<?php endif; ?>
				</td>
			</tr>
		<?php endforeach; ?>


		<tr id="add_account_info">
			<td><a href="#add_account_info" >Add Item</a></td>
		</tr>
		<tr class="add_account_info" style="display:none;">
			<td><input type="text" name="new_account_info_field" id="new_account_info_field" placeholder="Field Name" /></td>
			<td><input type="text" name="new_account_info_value" id="new_account_info_value" placeholder="Value"/></td>
		</tr>
		<tr class="add_account_info" style="display:none;">
			<td></td>
			<td>
				<input name="submit" id="submit" class="button button-primary" value="Add New Item" type="submit" />
			</td>
		</tr>

	</tbody></table>
<?php
}

/******* products list **************/
function hdgs_show_checked_products( $user_id ) {
	$hdgs_checked_products = get_user_meta($user_id,'hdgs_checked_products',true);
	$products = hdgs_get_users_products($user_id);
	$hdgs_effectivity_date = strtotime(get_user_meta($user_id, 'hdgs_effectivity_date', true));

	foreach ($products as $p) {

		$pid = $p->ID;
		$all_pids[] = $pid;
		$product = new WC_Product($pid);

		$items[$pid]['name'] = $product->get_name();
		$items[$pid]['price'] = floatval($product->get_price());
		$items[$pid]['gross_sales'] = 0;

		$orders = hdgs_get_orders_from_products($pid);

		foreach ($orders as $o) {
			$order = wc_get_order( $o->ID );
			$order_data = $order->get_data();
			$order_created = $order_data['date_created']->getTimestamp();
			if ($order_data['status']  == 'completed' AND $order_created > $hdgs_effectivity_date) {
				foreach ($order->get_items() as $item_meta) {
					if ($item_meta->get_product_id() == $pid) {
						if (!isset($items[$pid]['gross_sales'])) {
							$items[$pid]['gross_sales'] = 0;
						}
						$items[$pid]['gross_sales'] += $item_meta->get_subtotal() - $item_meta->get_subtotal_tax();
					}
				}
			}
		}

		if ($product->get_status() == 'publish') {
			$items[$pid]['status'] = 'active';
		} else {
			$items[$pid]['status'] = 'inactive';
		}
	}
?>
	<table class="form-table hdgs_product_list"><tbody>
		<tr>
			<td>
				<?php if (is_super_admin()): ?>
					<input type="checkbox" name="check_all_products" id="check_all_products" value="yes" />
				<?php endif; ?>
			</td>
			<td>Item</td>
			<td>SRP/Unit</td>
			<td>Gross Sales to Date</td>
			<td>Product Status</td>
		</tr>
		<?php foreach ($items as $pid => $item): ?>
			<tr>
				<td>
					<?php 
						if (is_super_admin()): 
							if(!isset($hdgs_checked_products[$pid])) {
								$hdgs_checked_products[$pid] = 0;
							}
					?>
						<input type="hidden" name="<?php echo 'checked_products['.$pid.']'; ?>" value="0" />
						<input type="checkbox" name="<?php echo 'checked_products['.$pid.']'; ?>" value="<?php echo $pid; ?>" <?php checked($hdgs_checked_products[$pid],$pid); ?> />
					<?php elseif ($hdgs_checked_products[$pid]) : ?>
						<span style="font-size:200%;color:forestgreen;">&bull;</span>
					<?php endif; ?>
				</td>
				<td><?php echo $item['name']; ?></td>
				<td><?php echo number_format($item['price'],2,'.',','); ?></td>
				<td><?php echo number_format($item['gross_sales'],2,'.',','); ?></td>
				<td><?php echo $item['status']; ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody></table>
<?php
}

/********ADDITIONAL CHARGES************/

function hdgs_show_charges($user) { 

	//get the charges, both the basic ones and the custom ones for this user
	$fields = hdgs_merge_input_lists($user->ID, hdgs_get_basic_charge_fields(), 'hdgs_new_charge_fields');

	//get the checkbox status
	$hdgs_charge_status = get_user_meta( $user->ID, 'hdgs_charge_status', true );

	$echo = true;
?>
	<h2>Applicable Charges</h2>
	<table class="form-table hdgs_charges"><tbody>
		<?php foreach ($fields as $name => $description): ?>
			<tr>
				<td><label for="<?php echo $name; ?>"><?php echo $description; ?></label></td>
				<td>
					<input type="hidden" name="<?php echo $name; ?>" value="no"/>
					<input type="checkbox" name="<?php echo $name; ?>" id="<?php echo $name; ?>" value="yes" <?php checked($hdgs_charge_status[$name], 'yes', $echo); ?>  />
				</td>
			</tr>
		<?php endforeach; ?>

		
			<tr id="add_new_charge">
				<td><a href="#add_new_charge" >Add new charge</a></td>
			</tr>
			<tr class="add_new_charge" style="display:none;">
				<td><label for="new_charge_name">Charge Name</label></td>
				<td><label for="new_charge_name">Charge Amount</label></td>
			</tr>
			<tr class="add_new_charge" style="display:none;">
				<td><input type="text" name="new_charge_name" id="new_charge_name" /></td>
				<td><input type="number" min="0" name="new_charge_amount" id="new_charge_amount" /></td>
			</tr>
			<tr class="add_new_charge" style="display:none;">
				<td></td>
				<td>
					<input name="submit" id="submit" class="button button-primary" value="Add New Charge" type="submit" />
				</td>
			</tr>
		
 	</tbody></table>

<?php 
}

//show the list of branch users connected to this merchant
add_action( 'acf/render_field/type=user', 'hdgs_get_branch_users_from_acf', 10, 1 );
function hdgs_get_branch_users_from_acf( $field ) {
	if ($field['_name'] != 'hdgs_branch_accounts') return; //only for branch account fields
	$user_ids = $field['value'];
	if (is_array($user_ids)) {
		foreach ($user_ids as $user_id) {
			$users[$user_id] = get_user_by('id',$user_id);
			$merchant = get_user_meta($user_id,'parent_merchant',true);
			if (!$merchant AND isset($_GET['user_id'])) {
				$merchant_id = $_GET['user_id'];
				update_user_meta($user_id,'parent_merchant',$merchant_id);
			}
		}
	}

	if(isset($users)) {
		hdgs_show_branch_users($users);
	}
}

function hdgs_show_branch_users($users) {
?>
	<table class="form-table"><tbody>
		<tr>
			<td>Branch Accounts</td>
			<td>Branch</td>
			<td>Company Name</td>
			<td>#</td>
			<td>Address</td>
		</tr>
		<?php 
			foreach ($users as $user): 
				$hdgs_account_info = get_user_meta( $user->ID, 'hdgs_account_info', true );	
				if (!isset($hdgs_account_info['company_name'])) {
					$hdgs_account_info['company_name'] = '';
				}
				if (!isset($hdgs_account_info['legal_address'])) {
					$hdgs_account_info['legal_address'] = '';
				}
		?>
				<tr>
					<td></td>
					<td><a href="<?php echo admin_url('user-edit.php?user_id='.$user->ID); ?>"><?php echo $user->user_login; ?></a></td>
					<td><?php echo $hdgs_account_info['company_name']; ?></td>
					<td><?php echo $user->ID; ?></td>
					<td><?php echo $hdgs_account_info['legal_address']; ?></td>
				</tr>
		<?php endforeach; ?>
	</tbody></table>

	<?php if (is_super_admin()): ?>
		<a href="<?php echo admin_url('user-new.php'); ?>">Add Branch Account</a>
	<?php endif; ?>
<?php
}

//another way to show branch users outside of ACF, in merchant's profile
function hdgs_show_branch_users_to_merchant($user){
	$user_ids = get_user_meta( $user->ID, 'hdgs_branch_accounts', true );
	foreach ($user_ids as $user_id) {
		$users[$user_id] = get_user_by('id',$user_id);
	}
	hdgs_show_branch_users($users);
}

//show ip addresses of the user
function hdgs_show_last_ip_addresses($user) {
	$uid = $user->ID;
	$sessions = get_user_meta($uid, 'session_tokens', true);
	if ($sessions) {
		foreach($sessions as $session) {
			$ip_details = hdgs_ip_details($session['ip']);
			$date = $session['login'];
			if ($ip_details) {
				foreach ($ip_details as $k => $v) {
					if ($k == 'ip') {
						$ip_data[$date][$k] = '<a href="http://addgadgets.com/whois_ip/index.php?domain='.$v.'" target="_blank">'.$v.'</a>';
					} else {
						$ip_data[$date][$k] = $v;
					}
				}
			} 
		}
	}
	//sort by date and get last 3 data points only
	krsort($ip_data); 
	if (count($ip_data) > 3) {
		$ip_data = array_slice($ip_data, -3); 
	}
?>
	<h2>Recent Login History</h2>
	<table class="form-table"><tbody>
	<tr>
		<td>Date</td>
		<td>IP Address</td>
		<td>ISP</td>
		<td>Location</td>
	</tr>
	<?php 
		foreach ($ip_data as $date => $val):
			$org = '';
			$location = ''; 

			if (isset($val['city'])) {
				$location = $val['city'];
			}
			if(isset($val['region'])) {
				if ($location) {
					$location .= ', ';
				}
				$location = $val['region'];
			}
			if(isset($val['country'])) {
				if ($location) {
					$location .= ', ';
				}
				$location = $val['country'];
			}

			if(isset($val['org'])) {
				$org = $val['org'];
			}
	?>
		<tr>
			<td><?php echo hdgs_date_to_words($date); ?></td>
			<td><?php echo $val['ip']; ?></td>
			<td><?php echo substr(strstr($org," "), 1); ?></td>
			<td><?php echo $location; ?></td>
		</tr>
	<?php endforeach; ?>
	</tbody></table>
<?php
}

//get IP details from ipinfo.io
function hdgs_ip_details($ip) {
  $json = file_get_contents("http://ipinfo.io/{$ip}");
  $details = json_decode($json);
  return $details;
}

/**************** UPDATE THE DB!! ***************/
add_action( 'personal_options_update', 'hdgs_update_user_info' );
add_action( 'edit_user_profile_update', 'hdgs_update_user_info' );

function hdgs_update_user_info( $user_id ) {
	if (!is_super_admin()) return; //only works for admins
 	hdgs_update_account_info($user_id);
 	hdgs_update_checked_products($user_id);
	hdgs_update_charge_data($user_id );

}

//update account info, sales category, and new account info fields
function hdgs_update_account_info($user_id) {

	$hdgs_account_info = get_user_meta( $user_id, 'hdgs_account_info', true );

	$fields = hdgs_merge_input_lists($user_id, hdgs_get_basic_account_fields(), 'hdgs_new_account_fields');
	$hdgs_new_account_fields = get_user_meta( $user_id, 'hdgs_new_account_fields', true);

	//process merchant sales categories (Deals, Goods, both)
	if ($_POST['hdgs_can_post_deals'] == 'yes') {
		$hdgs_can_post_deals = 1;
	} else {
		$hdgs_can_post_deals = 0;
	}
	if ($_POST['hdgs_can_post_goods'] == 'yes') {
		$hdgs_can_post_goods = 1;
	} else {
		$hdgs_can_post_goods = 0;
	}

	update_user_meta( $user_id, 'hdgs_can_post_deals', $hdgs_account_info );
	update_user_meta( $user_id, 'hdgs_can_post_goods', $hdgs_account_info );

	//process the existing account fields
	foreach ($fields as $name => $description) {
		if ($name == 'legal_address') {
			$value = sanitize_textarea_field($_POST[$name]);
		} else {
			$value = sanitize_text_field($_POST[$name]);	
		}
		if ($value != $hdgs_account_info[$name]) {
			$hdgs_account_info[$name] = $value;
		}
	}

	//get new charges and update meta accordingly
	$new_account_info_field = sanitize_text_field($_POST['new_account_info_field']);
	$new_account_info_value = $_POST['new_account_info_value'] + 0;
	if ($new_account_info_field AND $new_account_info_value) {
		$hdgs_new_account_fields[$new_account_info_field] = $new_account_info_field;
		update_user_meta( $user_id, 'hdgs_new_account_fields', $hdgs_new_account_fields );
		$hdgs_account_info[$new_account_info_field] = $new_account_info_value; //set as value
	}

	update_user_meta( $user_id, 'hdgs_account_info', $hdgs_account_info );
}

//update the list of checked products used for computing sales total
function hdgs_update_checked_products($user_id) {

	$hdgs_checked_products = array();

	foreach ($_POST['checked_products'] as $key => $pid) {
		if ($key == $pid) {
			$hdgs_checked_products[$pid] = $pid;
		}
	}

	update_user_meta($user_id,'hdgs_checked_products',$hdgs_checked_products);
}

//get basic charges and update user_meta with options
function hdgs_update_charge_data( $user_id ) {

	$hdgs_charge_status = get_user_meta( $user_id, 'hdgs_charge_status', true);

	$fields = hdgs_merge_input_lists($user_id, hdgs_get_basic_charge_fields(), 'hdgs_new_charge_fields');
	$hdgs_new_charge_fields = get_user_meta( $user_id, 'hdgs_new_charge_fields', true );
	
	
	foreach ($fields as $name => $description) {
		$value = sanitize_text_field($_POST[$name]);
		if ($value == 'yes') {
			$hdgs_charge_status[$name] = 'yes';
		} else {
			$hdgs_charge_status[$name] = 'no';
		}
	}

	//get new charges and update meta accordingly
	$new_charge_name = sanitize_text_field($_POST['new_charge_name']);
	$new_charge_amount = $_POST['new_charge_amount'] + 0;
	if ($new_charge_name AND $new_charge_amount) {
		$hdgs_new_charge_fields[$new_charge_name] = $new_charge_amount;
		update_user_meta( $user_id, 'hdgs_new_charge_fields', $hdgs_new_charge_fields );
		$hdgs_charge_status[$new_charge_name] = 1; //set as checked
	}

	update_user_meta( $user_id, 'hdgs_charge_status', $hdgs_charge_status );
}

//a way to merge the basic list of fields with the custom list for this user
function hdgs_merge_input_lists($user_id, $list = array(), $new_list_name){
	$new_list = get_user_meta( $user_id, $new_list_name, true );
	if (is_array($new_list) AND array_filter($new_list)) { 
		foreach ($new_list as $name => $value) {
			$add_list[$name] = $name;
		}
		$list = array_merge($list,$add_list);
	}

	return $list;
}


//add edit user capabilities if the user viewed is a branch of the vendor. remove the capabilities otherwise
add_action('load-user-edit.php', 'hdgs_allow_branch_edit');
function hdgs_allow_branch_edit($user_id){

	//only change the capabilities of vendors
	if (current_user_can('vendor')) {
		$current_user = wp_get_current_user();
		$user_id = (int)$_GET['user_id'];
		$my_branches = get_user_meta( $current_user->ID, 'hdgs_branch_accounts', true );
		if (in_array($user_id, $my_branches)) {
			$current_user->add_cap( 'edit_users' );
		}
		
		//remove edit user capability once the page changes, or if the user being edited is not his branch
		global $pagenow;
		if ($pagenow != 'user-edit.php' 
				OR ($user_id AND !in_array($user_id, $my_branches)) ) {
			$current_user->remove_cap( 'edit_users' );
		}
	} 
}
//disable the additional capabilities display
add_filter('additional_capabilities_display', '__return_false');



