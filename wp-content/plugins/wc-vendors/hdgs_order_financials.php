<?php
 
/********************************
**** FINANCIAL VIEW OF ORDERS ***
********************************/
 
 
 
/************ FINANCIAL DETAILS VIEW *************/
 
//add Order Financials Page in dashboard
add_action( 'admin_menu', 'hdgs_add_order_financials_to_menu' );
function hdgs_add_order_financials_to_menu() {
  if (current_user_can( 'administrator' )) {
    $role = 'administrator';
  } elseif (current_user_can( 'vendor' )) { 
    $role = 'vendor';
  } else {
    return false;
  }
  add_submenu_page(null, 
                  'Order Financials', 
                  'Order Financials', 
                  $role,  
                  'order_financials', 
                  'hdgs_show_order_financials'
                  );
}
 
//show the order to the merchant
function hdgs_show_order_financials() {
  wp_register_script('hdgs_jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js');
  wp_enqueue_script('hdgs_jquery');   

  $order_id = $_GET['order'];
  if ($order_id <= 1) {
    return false;
  }

  //set order status if changed
  global $allowed_merchant_statuses;
     
  $order = wc_get_order( $order_id );
  $current_user = wp_get_current_user();
  $line_items = $order->get_items();
  $items = array();
 
  //check if the items are merchant's
  foreach ( $line_items as $item_id => $item ) {

    //get product owner
    $merchant_id = get_post_field( 'post_author', $item->get_product_id()); 

    //check if product belongs to this user (or is admin)
    if ($merchant_id == $current_user->ID OR is_super_admin()) {
      $items[$item_id] = $item;
    }
  }

  //if there are no items, exit
  if (!array_filter($items)) {
    wp_die( 'You are not allowed to access this info.' );
  }

  $child_order_ids = hdgs_get_child_order_ids($order_id, $current_user->ID);
  $child_id = $child_order_ids[0];
  $child_order = wc_get_order( $child_id );

   //add styles and scripts
  hdgs_show_merchant_order_styles_scripts();

  //add order header 
  hdgs_show_order_financials_header($child_order);
  hdgs_show_admin_notices();

  //add right side info
  hdgs_show_order_merchant_info($order);

  //add main info
  hdgs_show_payout_info($child_order);
  hdgs_show_contract_details($child_order);
  hdgs_show_order_timeline($child_order);

  //add save
  hdgs_show_save_button();
  
}

function hdgs_show_order_financials_header($order) {
  //setup data
  $order_id = $order->get_id();
  $order_data = $order->get_data();
  $date_created = $order_data['date_created']->getTimestamp();

  $parent_id = wp_get_post_parent_id( $order_id );

  //get previous and next orders
  $current_user = wp_get_current_user();
  $orders = hdgs_get_orders_of_seller($current_user->ID);
  $prev_order_url = '';
  $next_order_url = '';
  $start = '';
  $prev_oid = '';
  $next_oid = '';

  if ($orders) {
    foreach ($orders as $o) {
      $all_ids[$o->ID] = $o->ID; 
    }
    asort($all_ids);

    //get the previous and next order_ids by walking through the array
    reset($all_ids);
    while (!in_array(key($all_ids), [$order_id, null])) {
      $start = current($all_ids);
      next($all_ids);
    }
    if (current($all_ids) !== false) {
      $prev_oid = prev($all_ids);
      next($all_ids);
      $next_oid = next($all_ids);
    }
    //fix if at first order
    if ($start == current($all_ids)) {
      reset($all_ids);
      $next_oid = next($all_ids);
    }

    //set the links for the arrows
    if ($prev_oid) {
      $prev_order_url = '<a href="'.admin_url('admin.php?page=order_financials&order='.$prev_oid).'"><-</a>';
    } else {
      $prev_order_url = '<span class="grayed_out"><-</span>';
    }
    if($next_oid) {
      $next_order_url = '<a href="'.admin_url('admin.php?page=order_financials&order='.$next_oid).'">-></a>';
    } else {
      $next_order_url = '<span class="grayed_out">-></span>';
    }
  }

  $order_status = ucwords(hdgs_translate_order_status($order->get_status(), $order_id));

  if (isset($_GET['page']) 
      AND ('order_financials' == $_GET['page'] 
          OR 'merchant_order' == $_GET['page']) ){
    $page = $_GET['page'];
  } 

  $order_url = admin_url('admin.php?page=merchant_order&order='.$parent_id);
  if (is_super_admin()) {
    $order_url = get_edit_post_link($order_id,'');
  }

  $payout_status = '';
  if (isset($_GET['payout_status'])) {
    $payout_status = $_GET['payout_status'];
  }
  
  if ($payout_status) {
    $result = update_post_meta($order_id,'hdgs_payout_status',$payout_status);
    if ($result) {
      global $allowed_payout_statuses;
      $new_status = $allowed_payout_statuses[$payout_status];
      if ('paid' == $payout_status) {
        $now = current_time( 'timestamp', 0 );
        update_post_meta($order_id,'hdgs_payout_date',$now);
      }
      $note = 'Changed payout status to: '.$new_status;
      hdgs_log_user_actions($order, $note);
      $_SESSION['hdgs_admin_notices'][] = array('type' => 'success', 
                                                'message' => 'Payout Status changed to '. $new_status, 
                                                );
    }
  }

  ?>
    
    <form method="post" action="admin-post.php">
      <input type="hidden" name="action" value="hdgs_update_order" />
      <input type="hidden" name="oid" value="<?php echo $order_id; ?>" />
      <input type="hidden" name="page" value="<?php echo $page; ?>" />
      <div class="order_header">
        <div class="order_top">
          <div class="left_head">
              <a href="<?php echo admin_url('admin.php?page=order_financials'); ?>">Financials</a>
          </div>
            <div class="right_head" style="display:none;">
              <?php echo $prev_order_url; ?>&nbsp;<?php echo $next_order_url; ?>
          </div>
        </div>
      </div>
      <div class="order_number">
        #<?php echo $parent_id; ?> <a href="<?php echo $order_url; ?>">View Order</a>
        <div>
          Order Status - <?php echo $order_status; ?>
        </div>
      </div>
  <?php
}

function hdgs_show_order_merchant_info($order) {

  $order_id = $order->get_id();
  $user_id = hdgs_get_merchants_from_order($order_id)[0];
  $user = get_user_by('id',$user_id);

  $order_ct = count(hdgs_get_orders_of_seller($user_id));
  $order_blurb = 'order';
  if (1 != $order_ct) {
    $order_blurb .= 's';
  }

  $hdgs_account_info = get_user_meta( $user_id, 'hdgs_account_info', true );

  $address = nl2br($hdgs_account_info['legal_address']);

  ?>
  <div class="customer_data">

    <h3>Merchant Info</h3>
    <div class="customer_name">
      <div class="avatar">
        <?php echo get_avatar( $user->user_email, 20); ?>
      </div>
      <h4><?php echo $hdgs_account_info['company_name']; ?></h4>
      <?php echo $order_ct.' '.$order_blurb; ?>
    </div>

    <div class="order_contact">
      <h4>Merchant Contact Person</h4>
      <?php echo $hdgs_account_info['contact_person']; ?>
    </div>

    <div class="billing_phone">
      <h4>Merchant Contact Info</h4>
      <p><?php echo $hdgs_account_info['contact_number']; ?></p>
      <p><?php echo $hdgs_account_info['contact_email']; ?></p>
    </div>

    <div class="shipping">
      <h4>Company Address</h4>
      <div class="shipping_address">
        <?php echo $address; ?>
      </div>
    </div>

    <?php if(is_super_admin()): ?>
      <a href="<?php echo get_edit_user_link($user_id); ?>">Edit Merchant Info</a>
    <?php endif; ?>

  </div>
  <?php
}

function hdgs_show_payout_info($order) {

  //setup data
  $order_id = $order->get_id();
  $order_data = $order->get_data();
  $user_id = hdgs_get_merchants_from_order($order_id)[0];

  //status menu
  global $allowed_payout_statuses;
  $payout_statuses = $allowed_payout_statuses;
  $payout_status = get_post_meta($order_id,'hdgs_payout_status',true);
  unset($payout_statuses[$payout_status]);  

  //get statuses
  $statuses['order'] = hdgs_translate_order_status($order->get_status(), $order_id);

  $fields = hdgs_merge_input_lists($user_id, hdgs_get_basic_charge_fields(), 'hdgs_new_charge_fields');

  //get the checkbox status
  $charge_status = get_user_meta( $user_id, 'hdgs_charge_status', true );

  //NOTE: must be the same as function hdgs_calculate_total_payout($order_id) in hdgs_functions.php. it calculates the same thing but the other only returns the total

  $gross_sales = $order->get_subtotal() - $order->get_total_tax();
  $discount = 0 - $order->get_total_discount();
  $cust_shipping = $order->get_shipping_total();
  $net_sales = $gross_sales + $discount + $cust_shipping;

  //get fees
  $charge_fees = get_option('hdgs_get_basic_charge_fees');
  $charge_status = get_user_meta( $user_id, 'hdgs_charge_status', true );

  $charges = array();
  if (is_array($charge_status)) {
    foreach ($charge_status as $name => $status) {
      if ('no' == $status) {
        continue;
      }

      if ('shipping_fee' == $name){
        if (hdgs_get_total_weight($order_id)) {
          $data = hdgs_get_courier_data($order_id);

          //normal shipping is shouldered by seller. for own packaging, half is shouldered by the buyer.
          $shipping_fee = hdgs_get_shipping_cost($order_id);
          if ($data['extra_charge']) {
            $shipping_fee = $shipping_fee/2;
          }

          $charges[$name] = $shipping_fee;

        } else {
          continue;
        }

      } elseif ('hd_commission' == $name) {
        $charges[$name] = hdgs_get_hd_commission($order_id);

      } else {

        if (!isset($charge_fees[$name])) {
          $charge_fees[$name] = 0;
        }
        $charges[$name] = $charge_fees[$name]*$net_sales/100;  
      }

      //make it negative
      $charges[$name] = 0 - abs($charges[$name]);
    }
  }

  //add extra charges
  $new_charges = get_post_meta($order_id,'hdgs_new_charges', true);
  if (!$new_charges) {
    $new_charges = array();
  }

  $total_payout = $net_sales + array_sum($charges) + array_sum($new_charges);

  $financial_remarks = get_post_meta($order_id, 'hdgs_fin_remarks', true);
  if (!$financial_remarks) {
    $financial_remarks = 'None';
  }

  //TODO: will add this later
  $rewards = 0;

  ?>
    <div class="order_details">
      <h3>
        Total Payout
      </h3> 
      <?php if(is_super_admin()): ?>
        <div class="order_actions">
          <a href="#add_charges">💳 Add Charges</a>
          <div class="add_charges">
            <a href="#cancel_add_charges">Cancel Add Charges</a>
            <div>
              <input type="text" name="charge_name" placeholder="Charge Name" />
              <input type="number" name="charge_amount" placeholder="Charge Amount" />
              <input name="send_status" value="Add Charge" type="submit" />
            </div>
          </div>
          <a href="#more_actions">More Actions</a>
          <a href="#hide_actions">Hide Actions</a>
          <div class="more_actions">
            <?php foreach ($payout_statuses as $status => $blurb): ?>
              <a href="<?php echo admin_url('admin.php?page=order_financials&payout_status='.$status.'&order='.$order_id); ?>">Mark as <?php echo  $blurb; ?></a>
            <?php endforeach; ?>
          </div>
        </div> 
      <?php endif; ?>

      <?php if (isset($allowed_payout_statuses[$payout_status])): ?>
        <div class="statuses">
          <span class="status_tag">
            <?php echo $allowed_payout_statuses[$payout_status]; ?>
          </span>
        </div>
      <?php endif; ?>


      <hr/>

      <div class="payout_details">
        <table>
          <tbody>
            <tr>
              <td>Sales</td><td><?php echo hdgs_payment_format($gross_sales); ?></td>
            </tr>
            <tr>
              <td>Cust. Shipping</td><td><?php echo hdgs_payment_format($cust_shipping); ?></td>
            </tr>
            <tr>
              <td>Rewards</td><td><?php echo hdgs_payment_format($rewards); ?></td>
            </tr>
            <tr>
              <td>Discount</td><td><?php echo hdgs_payment_format($discount); ?></td>
            </tr>
            <tr><td colspan=2><hr/></td></tr>
            <tr>
              <td>Net Sales</td><td><?php echo hdgs_payment_format($net_sales); ?></td>
            </tr>
          </tbody>
        </table>
        <table>
          <tbody>
            <tr>
             <td>Charges</td><td></td>
            </tr>
            <?php foreach ($charges as $name => $amount): ?>
              <tr>
                <td class="indented"><?php echo $fields[$name]; ?></td>
                <td><?php echo hdgs_payment_format($amount); ?></td>
              </tr>
            <?php endforeach; ?>
            <?php 
              foreach ($new_charges as $name => $amount): 
                if (!$name) {
                  continue;
                }
            ?>
              <tr>
                <td class="indented"><?php echo $name; ?></td>
                <td><?php echo hdgs_payment_format($amount); ?></td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        <hr/>
        <table>
          <tbody>
            <tr class="bolded">
              <td>Total Payout</td><td><?php echo hdgs_payment_format($total_payout); ?></td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="fin_remarks">
        <h3>Financial Remarks</h3>
        <?php if(is_super_admin()): ?>
          <textarea name="financial_remarks"><?php echo $financial_remarks; ?></textarea>
        <?php else: ?>
          <?php echo nl2br($financial_remarks); ?>
        <?php endif; ?>
      </div>
       
    </div>
  <?php
}

//format the numbers according to the desired display
function hdgs_payment_format($value) {

  if ($value >=0) {
    $value = '<span style="color:green;">'.wc_price($value).'</span>';
  } else {
    $value = '<span style="color:red;">('.wc_price(-$value).')</span>';
  }

  return $value;
}

function hdgs_show_contract_details($order) {

  $order_id = $order->get_id();
  $user_id = hdgs_get_merchants_from_order($order_id)[0];

  $payment_method = $order->get_payment_method();

  $sales_package = get_user_meta($user_id, 'hdgs_active_package', true);
  $contract_number = get_user_meta($user_id, 'hdgs_contract_id', true);


  //compute when next payout
  $payout_date = get_post_meta($order_id, 'hdgs_payout_date', true);
  if (!$payout_date) {
    $date_completed = $order->get_date_completed();
    if ($date_completed) {
      $date_completed = $date_completed->getTimestamp();
      $payout_date = hdgs_get_next_payout($date_completed);
    } else {
      $payout_date = '';
    }
  }

  $cr_number = get_post_meta($order_id, 'hdgs_cr_number', true);
  $cr_date = get_post_meta($order_id, 'hdgs_cr_date', true);
  $hdgs_account_info = get_user_meta( $user_id, 'hdgs_account_info', true );
  $merchant_tin = '';
  if (isset($hdgs_account_info['merchant_tin'])) {
    $merchant_tin = $hdgs_account_info['merchant_tin'];
  }
  $or_number = get_post_meta($order_id, 'hdgs_or_number', true);
  $or_date = get_post_meta($order_id, 'hdgs_or_date', true);

  $vat_rate = get_option('hdgs_vat_rate');
  $total_amount = hdgs_calculate_total_payout($order_id);
  $vat_amount = $total_amount/(1+($vat_rate/100));

  ?>

    <div class="contract_details">
      <table class="payment_method"><tbody>
        <tr>
          <td>Payment Method:</td>
          <td><?php echo $payment_method; ?></td>
        </tr>
      </tbody></table>

      <table class="sales_package"><tbody>
        <tr>
          <td>Sales Package:</td>
          <td><?php echo $sales_package; ?></td>
        </tr>
        <tr>
          <td>Contract No.:</td>
          <td><?php echo $contract_number; ?></td>
        </tr>
      </tbody></table>

      <table class="payout_schedule"><tbody>
        <tr>
          <td>Payout Schedule:</td>
          <td>
            <?php if (is_super_admin()): ?>
              <input type="date" name="payout_date" value="<?php echo esc_attr(date('Y-m-d',strtotime($payout_date))); ?>" />
            <?php else: ?>
              <?php if ($payout_date) echo esc_attr(date('M d, Y',strtotime($payout_date))); ?>
            <?php endif; ?>
          </td>
        </tr>
      </tbody></table>

      <table class="documentation"><tbody>
        <tr>
          <td><h3>Documentation</h3></td>
          <td></td>
        </tr>
        <tr>
          <td>Collection Receipt Number:</td>
          <td>
            <?php if(is_super_admin()): ?>
              <input type="text" name="cr_number" value="<?php echo $cr_number; ?>" />
            <?php else: ?>
              <?php echo $cr_number; ?>
            <?php endif; ?>
          </td>
        </tr>
        <tr>
          <td>Collection Receipt Date:</td>
          <td>
            <?php if(is_super_admin()): ?>
              <input type="date" name="cr_date" value="<?php echo $cr_date; ?>" /></td>
            <?php else: ?>
              <?php if ($cr_date) echo esc_attr(date('M d, Y',strtotime($cr_date))); ?>
            <?php endif; ?>
        </tr>
        <tr>
          <td>Merchant TIN No.:</td>
          <td><?php echo $merchant_tin; ?></td>
        </tr>
        <tr>
          <td>Official Receipt Number:</td>
          <td>
            <?php if(is_super_admin()): ?>
              <input type="text" name="or_number" value="<?php echo $or_number; ?>" />
            <?php else: ?>
              <?php echo $or_number; ?>
            <?php endif; ?>
          </td>
        </tr>
        <tr>
          <td>Official Receipt Date:</td>
          <td>
            <?php if(is_super_admin()): ?>
              <input type="date" name="or_date" value="<?php echo $or_date; ?>" />
            <?php else: ?>
              <?php if ($or_date) echo esc_attr(date('M d, Y',strtotime($or_date))); ?>
            <?php endif; ?>
          </td>
        </tr>
        <tr>
          <td>Total Amount:<br/><span style="font-size:80%;color:gray;">VAT exclusive</span></td>
          <td><?php echo wc_price($vat_amount); ?></td>
        </tr>
        <tr>
          <td>VAT (<?php echo $vat_rate; ?>%):</td>
          <td><?php echo wc_price($total_amount-$vat_amount); ?></td>
        </tr>
        <tr>
          <td>Final Amount:<br/><span style="font-size:80%;color:gray;">VAT inclusive</span></td>
          <td><?php echo wc_price($total_amount); ?></td>
        </tr>
      </tbody></table>
    </div>

  <?php
}