<?php

/********* RATE MAINTENANCE *****************/


//add Rate Maintainer in dashboard
add_action( 'admin_menu', 'hdgs_rate_maintainer_option', 100 );
function hdgs_rate_maintainer_option() {
	//add_dashboard_page( 'Rate Maintainer', 'Rate Maintainer', 'administrator', 'rate_maintainer', 'hdgs_rate_maintainer' );

	$financials_page = 'hdgs_financials';
	add_submenu_page($financials_page,
							      'Rate Maintainer',
							      'Rate Maintainer',
							      'administrator',
							      $financials_page.'&rate_maintainer=yes',
							      'hdgs_rate_maintainer'
							  		);
}

function hdgs_rate_style() {
?>
	<style type="text/css">
		.edit_recur,
		.edit_payout {
			float:right;
		}
		.hdgs_recur_form {
			width:100%;
			text-align: left;
			display: none;
			clear:both;
		} 
		#show_cutoff:target ~ .hdgs_cutoff_recur_form,
		#show_payout:target ~ .hdgs_payout_recur_form {
			display:block !important;
		}
		#show_cutoff,
		#show_payout {
			width: 100%;
			text-align: left;
			margin-bottom: 2em;
			clear:both;
		}
		#show_cutoff h3,
		#show_cutoff a,
		#show_payout h3,
		#show_payout a {
			margin: 0;
			float:left;
		}
		#show_cutoff a,
		#show_payout a {
			margin-left: 3em;
		}
		}
		
		table.cutoff_data,
		table.payout_data {
			width: 100%;
			clear:both;
			display:block;	
		}
		.hdgs_recur_form h2 {
			width: 100%;
			margin-bottom:0;
		}
		.hdgs_recur_form .pattern,
		.hdgs_recur_form .range {
			display:inline-block;
			padding: 1em;
		}
		.hdgs_recur_form .pattern {
			border-bottom: solid 2px #ddd;
		}
		.hdgs_recur_form .pattern .duration,
		.hdgs_recur_form .pattern .recur_every,
		.hdgs_recur_form .range .start,
		.hdgs_recur_form .range .end {
			float:left;
			padding: 1em;
		}
		.hdgs_recur_form .payout_schedule
		.hdgs_recur_form .pattern .duration {
			border-right: solid 2px #ddd;
		}
		.hdgs_recur_form .pattern .duration div {
			display:block;
			width:100%;
		}

		#add_new_charge:target,
		.add_new_charge {
			display: none;
		}

		#add_new_charge:target ~ .add_new_charge{
			display:block;
		}

		.hdgs_rate_fees tr th,
		.hdgs_rate_fees tr td {
			width: 50%;
		}
		.shipping_table .new_row {
			display:none;
		}
		.shipping_table .addrow:target ~ .new_row {
			display:block;
		}
		.shipping_table tr td input {
			width: 10em !important;
		}
		.shipping_table tr td .priority,
		.shipping_table tr td .allocation,
		.shipping_table tr td .max_weight,
		.shipping_table tr td .denominator,
		.shipping_table tr td .base_rate,
		.shipping_table tr td .extra_charge {
			width: 6em !important;
		}
		input.button-primary {
			margin-top:5em !important;
		}
	</style>
<?php
}

//display the fields
function hdgs_rate_maintainer() {

	if ( !current_user_can( 'administrator' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	//get the values
	$vat_rate = get_option('hdgs_vat_rate');
	$cutoff = get_option('hdgs_cutoff_pattern');
	$payout = get_option('hdgs_payout_pattern');
	$safety_rate = get_option('hdgs_safety_rate');
	if (!$safety_rate) {
		$safety_rate = 80;
	}

	//compute start and end dates
	if($cutoff['start']) {
		$cutoff['startdate'] = strtotime($cutoff['start']);
	} else {
		$cutoff['startdate'] = 'No date yet';
	}

	//get the translated end date
	if ($cutoff['end'] == 'end_by' AND $cutoff['end_by']) {
		$cutoff['enddate'] = strtotime($cutoff['end_by']);
	} elseif ($cutoff['end'] == 'end_after') {
		$cutoff_str = '+'.($cutoff['end_after']*$cutoff['every']);
		$cutoff_str .= ' '.hdgs_duration_to_time($cutoff['duration']).'s';
		$cutoff['enddate'] = strtotime($cutoff_str,strtotime($cutoff['start'])); 
	} else {
		$cutoff['enddate'] = 'No end date';
	}


	if($payout['start']) {
		$payout['startdate'] = strtotime($payout['start']);
	} else {
		$payout['startdate'] = 'No date yet';
	}

	//get the translated end date
	if ($payout['end'] == 'end_by' AND $payout['end_by']) {
		$payout['enddate'] = strtotime($payout['end_by']);
	} elseif ($payout['end'] == 'end_after') {
		$payout_str = '+'.($payout['end_after']*$payout['every']);
		$payout_str .= ' '.hdgs_duration_to_time($payout['duration']).'s';
		$payout['enddate'] = strtotime($payout_str,strtotime($payout['start']));
	} else {
		$payout['enddate'] = 'No end date';
	}

	$fields = hdgs_get_basic_charge_fields();
	$charge_fees = get_option('hdgs_get_basic_charge_fees');

	$commissions_table = hdgs_get_commissions_table();

	//update the fees
	$charge_fees = get_option('hdgs_get_basic_charge_fees');

	//get api credentials
	$api_credentials = get_option('hdgs_api_credentials');

	hdgs_rate_style();
	
?>
	<form method="post" action="admin-post.php">
		<input type="hidden" name="action" value="hdgs_update_rates" />
		<?php wp_nonce_field( 'hdgs_update_rates_verify' ); ?>

		<h1>Rates Maintenance</h1>

		<h2>VAT Rate</h2>
		<table class="vat_rate"><tbody>
			<tr>
				<td><input name="vat_rate" type="number" max="100" min="0" step="1" value="<?php echo $vat_rate; ?>"/>%</td>
			</tr>
		</tbody></table>



		<h2>Payout Schedule</h2>
		<div id="show_cutoff">
			<h3>Cut-off Period</h3><a href="#show_cutoff" class="edit_recur">Edit</a>
		</div>
		<?php hdgs_show_recur_form('cutoff'); ?>
		<table class="cutoff_data" style="width:100%"><tbody>
			<tr>
				<td>Recurrence:</td>
				<td><?php echo hdgs_show_recur_blurb('cutoff'); ?></td>
			</tr>
			<tr><td>Range of Recurrence:</td></tr>
			<tr>
				<td style="margin-left:3em;">Start Date:</td>
				<td><?php echo date('F j, Y', $cutoff['startdate']+0);  ?></td>
			</tr>
			<tr>
				<td style="margin-left:3em;">End Date:</td>
				<td><?php echo date('F j, Y', $cutoff['enddate']+0); ?></td>
			</tr>
		</tbody></table>	

		<div id="show_payout">
			<h3>Payout Period</h3><a href="#show_payout" class="edit_payout">Edit</a>
		</div>
		<?php hdgs_show_recur_form('payout'); ?>
		<table class="payout_data" style="width:100%"><tbody>
			<tr>
				<td>Recurrence:</td>
				<td><?php echo hdgs_show_recur_blurb('payout'); ?></td>
			</tr>
			<tr><td>Range of Recurrence:</td></tr>
			<tr>
				<td style="margin-left:3em;">Start Date:</td>
				<td><?php echo date('F j, Y', $payout['startdate']+0);  ?></td>
			</tr>
			<tr>
				<td style="margin-left:3em;">End Date:</td>
				<td><?php echo date('F j, Y', $payout['enddate']+0); ?></td>
			</tr>
		</tbody></table>



		<h2>Fees</h2>
		<table class="form-table hdgs_rate_fees"><tbody>
			<tr>
				<th>Title</th>
				<th>Value</th>
			</tr>
			<?php foreach ($fields as $name => $description): ?>
				<tr>
					<td><label for="<?php echo $name; ?>"><?php echo $description; ?></label></td>
					<td><input type="number" min="0" name="<?php echo $name; ?>" value="<?php echo $charge_fees[$name]; ?>"/>%</td>
				</tr>
			<?php endforeach; ?>

				<tr id="add_new_charge">
					<td><a href="#add_new_charge" >Add new charge</a></td>
					<td></td>
				</tr>
				<tr class="add_new_charge">
					<td><label for="new_charge_name">Charge Name</label></td>
					<td><label for="new_charge_percent">Charge Percentage</label></td>
				</tr>
				<tr class="add_new_charge">
					<td><input type="text" name="new_charge_name" id="new_charge_name" /></td>
					<td><input type="number" min="0" max="100" step="1" name="new_charge_percent" id="new_charge_percent" />%</td>
				</tr>
				<tr class="add_new_charge">
					<td></td>
					<td>
						<input name="submit" id="submit" class="button button-primary" value="Add New Charge" type="submit" />
					</td>
				</tr>			
	 	</tbody></table>



	 	<h2 id="update_commissions_table">HD Commissions</h2>
		<table class="hdgs_commission_table"><tbody>
		<tr>
			<th>Category</th>
			<th>Subcategory</th>
			<th>Subsubcategory</th>
			<th>Rate</th>
		</tr>
		<?php 
			foreach ($commissions_table as $cat => $row):
				foreach ($row as $subcat => $row2):
					foreach ($row2 as $subsubcat => $rate): ?>
						<tr>
							<td><?php echo get_term($cat,'product_cat')->name; ?></td>
							<td><?php echo get_term($subcat,'product_cat')->name; ?></td>
							<td><?php echo get_term($subsubcat,'product_cat')->name; ?></td>
							<td><input type="number" name="<?php echo 'commission_table['.$cat.']['.$subcat.']['.$subsubcat.']'; ?>" min="0" value="<?php echo $commissions_table[$cat][$subcat][$subsubcat]; ?>" />%</td>
						</tr>
		<?php 
					endforeach; 
				endforeach;
			endforeach; ?>
		</tbody></table>
		<a href="<?php echo hdgs_get_current_url().'&update_commissions_table=yes#update_commissions_table'; ?>">Update Commissions Table</a>

		<h2>Safety Rate</h2>
		<table class="safety_rate"><tbody>
			<tr>
				<td><input name="safety_rate" type="number" max="100" min="0" step="1" value="<?php echo $safety_rate; ?>"/>%</td>
			</tr>
		</tbody></table>

		<h2>Shipping Table</h2>
		<?php hdgs_show_shipping_table(); ?>

		<h2>API keys</h2>
		<p>
			<strong>Quad X:</strong> 
			<input type="text" name="api_credentials[quadx][key]" value="<?php echo $api_credentials['quadx']['key']; ?>" placeholder='API key' />
			<input type="text" name="api_credentials[quadx][secret]" value="<?php echo $api_credentials['quadx']['secret']; ?>" placeholder='Secret key' />
		</p>

		<input name="submit" min="0" id="submit" class="button button-primary" value="Update Data" type="submit" />
	</form>
<?php
}

function hdgs_show_recur_form($name) {

	$recur_options = get_option('hdgs_'.$name.'_pattern');
	//set defaults
	if (!$recur_options['duration']) $recur_options['duration'] = 'Monthly';
	if (!$recur_options['every']) $recur_options['every'] = 1;
	if (!$recur_options['end']) $recur_options['end'] = 'no_end';
	if (!$recur_options['end_after']) $recur_options['end_after'] = 1;

	$durations = array ('Daily', 'Weekly', 'Monthly', 'Yearly');
	$days = array('Sun','Mon','Tues','Wed','Thu','Fri','Sat');

	$duration_blurb = hdgs_duration_to_time($recur_options['duration']);

?>
	<div class="hdgs_recur_form <?php echo 'hdgs_'.$name.'_recur_form'; ?>">
		<h2>Recurrence Pattern</h2>
		<div class="pattern">
			<div class="duration">
				<?php foreach ($durations as $duration): ?>
					<div>
						<input type="radio" id="<?php echo $duration; ?>"
		     name="<?php echo $name.'[duration]'; ?>" value="<?php echo $duration; ?>" <?php checked( $recur_options['duration'], $duration ); ?> />
		     		<label for="<?php echo $duration; ?>"><?php echo $duration; ?></label>
		     	</div>
		    <?php endforeach; ?>
			</div>
			<div class="recur_every">
				Recur every <input type="number" id="every" name="<?php echo $name.'[every]'; ?>" value="<?php echo $recur_options['every']; ?>" min="1" max="52" step="1"  /> <?php echo $duration_blurb; ?>(s):
				<div class="day_list" style="display:none;">
					<?php foreach ($days as $day): ?>
						<input type="hidden" name="<?php echo $name.'['.$day.']'; ?>" value="no" />
						<input type="checkbox" id="<?php echo $day; ?>" name="<?php echo $name.'['.$day.']'; ?>" value="yes" <?php checked( $recur_options[$day], 'yes' ); ?> />
						<label for="<?php echo $day; ?>"><?php echo $day; ?></label>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<h2>Range of Recurrence</h2>
		<div class="range">
			<div class="start">
				Start <input type="date" id="start" name="<?php echo $name.'[start]'; ?>" value="<?php echo $recur_options['start']; ?>" />
			</div>
			<div class="end">
				<div>
					<input type="radio" id="no_end"
	     name="<?php echo $name.'[end]'; ?>" value="no_end" <?php checked( $recur_options['end'], 'no_end' ); ?> />
	     		<label for="no_end">No End Date</label>
	     	</div>
	     	<div>
					<input type="radio" id="end_after"
	     name="<?php echo $name.'[end]'; ?>" value="end_after" <?php checked( $recur_options['end'], 'end_after' ); ?> />
	     		<label for="end_after">End After <input type="number" name="<?php echo $name.'[end_after]'; ?>" value="<?php echo $recur_options['end_after']; ?>" min="1" step="1"  /> occurences</label>
	     	</div>
	     	<div>
					<input type="radio" id="end_by"
	     name="<?php echo $name.'[end]'; ?>" value="end_by" <?php checked( $recur_options['end'], 'end_by' ); ?> />
	     		<label for="end_by">End By <input type="date" name="<?php echo $name.'[end_by]'; ?>" value="<?php echo $recur_options['end_by']; ?>" /></label>
	     	</div>
			</div>
		</div>
	</div>
<?php
}

function hdgs_show_recur_blurb($name) {
	$days = array('Sun','Mon','Tues','Wed','Thu','Fri','Sat');
	$duration_blurb = '';
	$on_blurb = '';
	$ct = 0;
	$recur_options = get_option('hdgs_'.$name.'_pattern');

	//get days checked
	foreach ($days as $day) {
		if ($recur_options[$day] == 'yes') {
			$days_checked[$day] = $day;
		}
	}

	//translate current duration
	$duration_blurb = hdgs_duration_to_time($recur_options['duration']);
  if ($recur_options['duration'] == 'Weekly' AND array_filter($days_checked)) {
		$on_blurb = ', on '.implode(', ',$days_checked);
  }

  //put together the blurb
	if ($recur_options['every'] > 1) {
		$recur_blurb = 'Repeat every '.$recur_options['every'].' '.$duration_blurb.'s '.$on_blurb;
	} elseif ($recur_options['duration']) {
		$recur_blurb = 'Repeat '.strtolower($recur_options['duration']);
	} else {
		$recur_blurb = 'None';
	}

	return $recur_blurb;
}

function hdgs_show_shipping_table(){

	$counter = 0;
	$headers = array('courier' 			=> 'Courier', 
									'priority' 			=> 'Priority', 
									'destination' 	=> 'Destination', 
									'package_size' 	=> 'Package Size', 
									'allocation'		=> 'Allocation', 
									'dimensions'		=> 'Dimensions<br/>in cm', 
									'max_weight'		=> 'Max Weight<br/>in KG',	
									'denominator'		=> 'Denominator', 
									'base_rate'			=> 'Base Rate', 
									'extra_charge'	=> 'Extra Wt Charge');
	$shipping_table = get_option('hdgs_shipping_table');
	if(!$shipping_table) {
		$shipping_table = array(array());
	} 
	
	//push two extra rows for extra input
	$shipping_table[] = array();
	$shipping_table[] = array();

	$number_fields = array('priority','allocation','max_weight','denominator','base_rate','extra_charge'); 

	?>
		<table class="shipping_table">
			<thead>
				<tr>
					<?php 
						foreach ($headers as $header) {
							echo '<td>';
							echo 		$header;
							echo '</td>';
						} 
					?>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($shipping_table as $id => $row): ?>
					<tr>
						<?php 							
							foreach ($headers as $key => $header) {
								echo '<td class="'.$key.'">';
								$type = 'text';
								if (in_array($key, $number_fields)) {
									$type = 'number';
								}
								if (!isset($row[$key])) {
									$row[$key] = '';
								}
								echo 	'<input type="'.$type.'" class="'.$key.'" name="shipping_table['.$id.']['.$key.']" value="'.$row[$key].'" />';
								echo '</td>';
							}
							++$counter;
						?>
					</tr>
				<?php endforeach;?>
			</tbody>
		</table>
	<?php
}



//now we update the rates
add_action( 'admin_post_hdgs_update_rates', 'hdgs_update_rates' );
function hdgs_update_rates(){
	if ( !current_user_can( 'administrator' ) ) {
	  wp_die( 'You are not allowed to be on this page.' );
	}
	// Check that nonce field
	check_admin_referer( 'hdgs_update_rates_verify' );

	//update vat
	update_option('hdgs_vat_rate', $_POST['vat_rate'] + 0);

	//update cutoff and payout info
	update_option('hdgs_cutoff_pattern', $_POST['cutoff']);
	update_option('hdgs_payout_pattern', $_POST['payout']);

	//update the fees
	$charge_fees = get_option('hdgs_get_basic_charge_fees');

	//a list of reserved names, so they don't get overridden by the custom charge fields
	$reserved_names = array('vat_rate', 'cutoff', 'payout', 'new_charge_name', 'new_charge_percent', 'commission_table', 'safety_rate', 'shipping_table', 'api_credentials');
	
	//if blank set up the array first so it has zero values
	$charge_fields = hdgs_get_basic_charge_fields();
	if (!$charge_fees) {
		foreach ($charge_fields as $field => $desc) {
			$charge_fees[$field] = 0;
		}
	}
	//get the fees for each field, update if different
	foreach ($charge_fees as $field => $old_fee) {
		if (in_array($field, $reserved_names)) {
			continue;
		}
		$fee = $_POST[$field];
		if ($old_fee != $fee) {
			$charge_fees[$field] = $fee;
		}
	}

	//if has new charge update the $charge_fees and hdgs_get_basic_charge_fields
	$charge_name = sanitize_text_field($_POST['new_charge_name']);
	$charge_percent = $_POST['new_charge_percent'] + 0;
	if ($charge_name AND $charge_percent) {
		$charge_fees[$charge_name] = $charge_percent;
		$charge_fields[$charge_name] = $charge_name;
		update_option('hdgs_basic_charge_fields', $charge_fields);
	}

	//finally, update the fees
	update_option('hdgs_get_basic_charge_fees', $charge_fees);

	//update the commissions table
	$commissions_table = hdgs_get_commissions_table();
	foreach ($commissions_table as $cat => $row) {
		foreach ($row as $subcat => $row2) {
			foreach ($row2 as $subsubcat => $old_rate) {
				$rate = $_POST['commission_table'][$cat][$subcat][$subsubcat] + 0;
				if ($old_rate != $rate) {
					$commissions_table[$cat][$subcat][$subsubcat] = $rate;
				}
			}
		}
	}
	update_option('hdgs_get_commissions_table',$commissions_table);

	//update the safety
	update_option('hdgs_safety_rate', $_POST['safety_rate'] + 0);

	//update the shipping table
	$shipping_table = $_POST['shipping_table'];

	//ignore blank rows and reset order of table to match priority
	$extra_table = array();
	foreach ($shipping_table as $id => $row) {
		if (array_filter($row)) {
			$priority = $row['priority'];
			//if forgot to put priority, set aside
			if (!$priority) {
				$extra_table[] = $row;
				continue;
			}
			//if we already have a row at this priority, set aside
			if ($new_table[$priority]) {
				$extra_table[] = $row;
			} else {
				$new_table[$priority] = $row;	
			}
		}
	}
	//add the extras
	$counter = count($new_table)+100;
	foreach ($extra_table as $row) {
		$row['priority'] = ++$counter;
		$new_table[$counter] = $row;
	}
	ksort($new_table);
	update_option('hdgs_shipping_table', $new_table);

	//update api keys
	update_option('hdgs_api_credentials', $_POST['api_credentials']);

	//add message to session when stuff is updated
	$_SESSION['hdgs_admin_notices'][] = array ('type' => 'success',
																			'message' => 'Rate Options Updated',
																			);

	//back to the rate page
	$financials_page = 'hdgs_financials';
	wp_redirect(admin_url('admin.php?page='.$financials_page.'&rate_maintainer=yes'));
	exit;
}
