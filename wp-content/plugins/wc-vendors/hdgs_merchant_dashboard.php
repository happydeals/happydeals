<?php

/*****************************
/**** MERCHANT DASHBOARD *****
******************************/

//adjust the column widths: 70% main, 30% side
add_action('admin_head','hdgs_set_dashboard_column_widths');
function hdgs_set_dashboard_column_widths() {
	if (!current_user_can('vendor')) return; 
	?>
	<style>
		#dashboard-widgets #postbox-container-1 {
			width:80% !important;
		}
		#dashboard-widgets #postbox-container-2 {
			width:20% !important;
		}
	</style>
	<?php
}

// add newsfeed dashboard widget
add_action('wp_dashboard_setup', 'hdgs_show_dashboard_widgets' );
function hdgs_show_dashboard_widgets() {
	if (current_user_can('vendor')) {
		remove_meta_box('dashboard_right_now', 'dashboard', 'normal');   // Right Now
	  remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal'); // Recent Comments
	  remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');  // Incoming Links
	  remove_meta_box('dashboard_plugins', 'dashboard', 'normal');   // Plugins
	  remove_meta_box('dashboard_quick_press', 'dashboard', 'side');  // Quick Press
	  remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');  // Recent Drafts
	  remove_meta_box('dashboard_primary', 'dashboard', 'side');   // WordPress blog
	  remove_meta_box('dashboard_secondary', 'dashboard', 'side');   // Other WordPress News
		remove_meta_box('dashboard_activity', 'dashboard', 'normal'); //Activity
		
		wp_add_dashboard_widget('hdgs_stats_dashboard_widget', "What's Happening Now", 'hdgs_add_stats_dashboard_widget');
		wp_add_dashboard_widget('hdgs_newsfeed_dashboard_widget', '', 'hdgs_add_newsfeed_dashboard_widget');
		wp_add_dashboard_widget('hdgs_mainfeed_dashboard_widget', 'Announcements', 'hdgs_add_mainfeed_dashboard_widget');

		add_meta_box('hdgs_add_newsfeed_dashboard_widget', 'Latest Site Updates', 'hdgs_add_newsfeed_dashboard_widget', 'dashboard', 'side', 'high');

	//add widgets to admins as well
	} elseif (current_user_can('administrator')) {
		wp_add_dashboard_widget('hdgs_stats_dashboard_widget', "What's Happening Now", 'hdgs_add_stats_dashboard_widget');
		wp_add_dashboard_widget('hdgs_newsfeed_dashboard_widget', '', 'hdgs_add_newsfeed_dashboard_widget');
		add_meta_box('hdgs_add_newsfeed_dashboard_widget', 'Latest Site Updates', 'hdgs_add_newsfeed_dashboard_widget', 'dashboard', 'side', 'high');
		wp_add_dashboard_widget('hdgs_mainfeed_dashboard_widget', 'Announcements', 'hdgs_add_mainfeed_dashboard_widget');

	} elseif (current_user_can('branch_account')) {
		remove_meta_box('dashboard_right_now', 'dashboard', 'normal');   // Right Now
	  remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal'); // Recent Comments
	  remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');  // Incoming Links
	  remove_meta_box('dashboard_plugins', 'dashboard', 'normal');   // Plugins
	  remove_meta_box('dashboard_quick_press', 'dashboard', 'side');  // Quick Press
	  remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');  // Recent Drafts
	  remove_meta_box('dashboard_primary', 'dashboard', 'side');   // WordPress blog
	  remove_meta_box('dashboard_secondary', 'dashboard', 'side');   // Other WordPress News
		remove_meta_box('dashboard_activity', 'dashboard', 'normal'); //Activity
	}
}

function hdgs_merchant_stats_styling() {
?>
	<style>
		.merchant_stats {
			display:inline-block;
			width: 100%;
		}
		.merchant_stats .stat_links {
			width: 100%;
			margin-bottom:0.5em;
			text-align:right;
		}
		.stat_column {
			float:left;
			width: 21.3%;
			margin-right:1%;
			margin-bottom:1%;
			padding: 1em;
			display:inline-block;
			border: solid 1px #eee;
			min-width: 200px;
			margin-left: 0;
			min-height: 350px;
		}
		.stat_column:last-child {
			margin-right: 0;
		}
		h3 {
			font-size:150% !important;
			font-weight: bold;
		}
		p.total {
			font-size: 300%;
			font-weight: bold;
			height:70px;
			margin-bottom: 0;
		}
		.sales_info p.total {
			font-size: 250%;
		}
		.stat_text {
			display: table-cell;
			margin-bottom:0.5em !important;
		}
		.stat_text h4 {
			margin-bottom:0 !important;
		}
		.stat_text span {
			font-style: italic;
			color:gray;
		}
		.stat_row {
			display:table;
			width:100%;
			min-height: 2.5em;
		}
		.stat_number {
			display:table-cell;
			text-align: right;
		}
		
	</style>
<?php
}

//display the basic, at-a-glance stats needed by the vendor
function hdgs_add_stats_dashboard_widget($user_id = 0) {

	//set up variables
	$product_cat = '';
	$orders = array();
	$product_stats = array('rejected' => 0, 
												);

	//set up time constants
	$now = current_time( 'timestamp', 0 );
	$hour = 3600;
	$half_day_ago = strtotime('-12 hours',$now);
	$one_day_ago = strtotime('-24 hours',$now);
	$today = strtotime('today', $now);
	$this_week = strtotime('last Sunday', $now);
	$next_week = strtotime('11:59pm this Sunday', $now);
	$this_month = strtotime('first day of this month', $now);

	//initialize order stats array
	$net_sales = 0;
	$completion = 0;
	$order_stats = array('sales_revenue' 						=> 0, 
												'for_pickup' 							=> 0, 
												'order_status'						=> array(), 
												'order_age'								=> array('>24H'					=> 0, 
																													'12H-24H'				=> 0, 
																													'<12H'					=> 0, 
																													), 
												'performance_this_week' 	=> array('cancelled'		=> 0, 
																													'ready_in_24H'	=> 0, 
																													'refunded'			=> 0, 
																													'shipped_in_48'	=> 0, 
																													), 
												'sales_count'							=> array('this_week' 		=> 0, 
																													), 
												'performance_rates'				=> array('cancel_rate'	=> 0, 
																													'ready_in_24H'	=> 0, 
																													'return_rate'		=> 0, 
																													'shipped_in_48'	=> 0, 
																													), 
												'sales_total'							=> array('today'				=> 0, 
																													'this_week'			=> 0, 
																													'this_month'		=> 0, 
																													), 
											);

	if (!$user_id) {
		$current_user = wp_get_current_user();
		$user_id = $current_user->ID;
	}
	
	if (isset($_GET['product_cat'])) {
		$product_cat = $_GET['product_cat'];
	}
	$products = hdgs_get_users_products($user_id, $product_cat);

	foreach ($products as $product) {
		$all_products[] = $product->ID;
		if ($product->post_status == 'trash') {
			$reason = get_post_meta($product->ID, 'hdgs_rejection_reason', true);
			++$product_stats['rejected'][$reason];
		} else {
			if (!isset($product_stats[$product->post_status])) {
				$product_stats[$product->post_status] = 0;
			}
			
			++$product_stats[$product->post_status];	
		}
	}

	$orders = hdgs_get_orders_from_products($all_products);

	//grab order data and sort into the right places
	//TODO: check line by line. use data from vendor's order only (child order)
	/*************************************************************************************
		ITEMS that are exclusive to one vendor: 
		1. shipping info
		2. item subtotal
		3. packaging info
		4. timeline (only show their posts and admin posts)
		5. financials
	 *************************************************************************************/

	foreach ($orders as $o) {
		$order = wc_get_order( $o->ID );
		$order_data = $order->get_data();
		$order_timestamp_created = $order_data['date_created']->getTimestamp();
		$net_sales = $order->get_subtotal() - $order->get_total_discount() - $order->get_total_tax();		

		//sort orders by date created (only for paid orders, under procesing)
		if ('processing' == $order_data['status']) {
			if ($order_timestamp_created < $one_day_ago) {
				++$order_stats['order_age']['>24H'];	
			} elseif ($order_timestamp_created < $half_day_ago) {
				++$order_stats['order_age']['12H-24H'];	
			} else {
				++$order_stats['order_age']['<12H'];	
			}

			//check if order is for pickup
			$branch_id = get_post_meta($o->ID, 'hdgs_pickup_branch', true);
			if( $branch_id > 1 ) { 
				++$order_stats['for_pickup'];
			}
		}

		//sort orders by status
		if (!isset($order_stats['order_status'][$order_data['status']])) {
			$order_stats['order_status'][$order_data['status']] = 0;
		}
		++$order_stats['order_status'][$order_data['status']];	
		

		//record sales totals
		if ('completed' == $order_data['status']) {
			$order_stats['sales_revenue'] += $order->get_total();

			//get today's sales
			if ($order_timestamp_created > $today) {
				$order_stats['sales_total']['today'] += $net_sales;
			}
		}
		
		//get this week's stats
		if ($order_timestamp_created > $this_week) {
			++$order_stats['sales_count']['this_week'];

			//get performance data for this week
			++$order_stats['performance_this_week'][$order_data['status']];

			//get sales total for completed orders
			if ($order_data['status'] == 'completed') {
				$order_stats['sales_total']['this_week'] += $net_sales;
			}

			//I am assuming that having a dispatch date means the order is ready to ship by that date
			$ready_to_ship_date = hdgs_get_status_change_timestamp($o->ID, 'ready_to_ship');
			$date_paid = $order->get_date_paid();
			if ($date_paid) {
				$date_paid = $date_paid->getTimestamp();
			}
			if ($ready_to_ship_date AND ($ready_to_ship_date - $date_paid) < 24*$hour) {
				++$order_stats['performance_this_week']['ready_in_24H'];
			}

			//check if shipped in 48H
			//difference is between order date and time it actually shipped out
			if ($order_data['status'] == 'completed') {
				$date_paid = $order->get_date_paid();
				if ($date_paid) {
					$date_paid = $date_paid->getTimestamp();
				}
				$date_completed = $order->get_date_completed();
				if ($date_completed) {
					$date_completed = $date_completed->getTimestamp();
				}
				if (($date_completed - $date_paid) < 48*$hour) {
					++$order_stats['performance_this_week']['shipped_in_48'];
				}
			}
		}

		//get this month's sales, excluding cancelled orders
		if ($order_timestamp_created > $this_month 
				AND $order_data['status'] != 'cancelled') {
			$order_stats['sales_total']['this_month'] += $net_sales;
		}
	}

	//log performance rates
	if ($order_stats['sales_count']['this_week'] >= 1) {
		$order_stats['performance_rates']['cancel_rate'] = $order_stats['performance_this_week']['cancelled']*100 / $order_stats['sales_count']['this_week'];
	}
	$order_stats['performance_blurbs']['cancel_rate'] = hdgs_get_performance_text($order_stats['performance_rates']['cancel_rate']);

	//ready rate is no. of orders ready in 24H orders
	if ($order_stats['sales_count']['this_week'] >= 1) {
		$order_stats['performance_rates']['ready_in_24H'] = $order_stats['performance_this_week']['ready_in_24H']*100 / $order_stats['sales_count']['this_week'];
	}
	$order_stats['performance_blurbs']['ready_in_24H'] = hdgs_get_performance_text($order_stats['performance_rates']['ready_in_24H']);

	//return rate is refunds / total orders
	if ($order_stats['sales_count']['this_week'] >= 1) {
		$order_stats['performance_rates']['return_rate'] =  $order_stats['performance_this_week']['refunded']*100 / $order_stats['sales_count']['this_week'];
	}
	$order_stats['performance_blurbs']['return_rate'] = hdgs_get_performance_text($order_stats['performance_rates']['return_rate']);

	if ($order_stats['sales_count']['this_week'] >= 1) {
		$order_stats['performance_rates']['shipped_in_48'] =  $order_stats['performance_this_week']['shipped_in_48']*100 / $order_stats['sales_count']['this_week'];
	}
	$order_stats['performance_blurbs']['shipped_in_48'] = hdgs_get_performance_text($order_stats['performance_rates']['shipped_in_48']);

	$total_perf_rate = (100-$order_stats['performance_rates']['cancel_rate']) + (100-$order_stats['performance_rates']['return_rate']) + $order_stats['performance_rates']['ready_in_24H'] + 	$order_stats['performance_rates']['shipped_in_48'];
	$order_stats['performance_average'] = $total_perf_rate/count($order_stats['performance_rates']);

	$hdgs_active_package = get_user_meta($user_id, 'hdgs_active_package', true);

	$sales_to_date = hdgs_get_sales_to_date($user_id);

	$total_value = get_user_meta($user_id, 'hdgs_led_options_hdgs_led_total_value', true);

	$balance = $total_value - $sales_to_date;

	if ($total_value > 0) {
		$completion = $sales_to_date/$total_value;	
	}
	

	
	$deals_link = admin_url('?product_cat=deals');
	$goods_link = admin_url('?product_cat=goods');
	$both_link = admin_url();
	/*
	if (current_user_can('administrator') ) {
		$deals_link = admin_url('user-edit.php?user_id='.$user_id.'&product_cat=deals#merchant_stats');
		$goods_link = admin_url('user-edit.php?user_id='.$user_id.'&product_cat=goods#merchant_stats');
		$both_link = admin_url('user-edit.php?user_id='.$user_id.'#merchant_stats');
	}
	*/

	//show custom styles
	hdgs_merchant_stats_styling();
	?>
		
		<div class="merchant_stats" id="merchant_stats">
			<div class="stat_links">
				<a href="<?php echo $deals_link; ?>">Deals</a>
				<a href="<?php echo $goods_link; ?>">Goods</a>
				<a href="<?php echo $both_link; ?>">Both</a>
			</div>
			<div class="stat_column pending_orders">
				<h3>Pending Orders</h3>
				<span>&nbsp;</span>
				<p class="total"><?php echo array_sum($order_stats['order_age']); ?></p>
				<div class="stat_row">
					<div class="stat_text">
						<h4>Since > 24H</h4>
					</div>
					<div class="stat_number"><?php echo intval($order_stats['order_age']['>24H']); ?></div>
				</div>
				<div class="stat_row">
					<div class="stat_text">
						<h4>Since > 12H-24H</h4>
					</div>
					<div class="stat_number"><?php echo intval($order_stats['order_age']['12H-24H']); ?></div>
				</div>
				<div class="stat_row">
					<div class="stat_text">
						<h4>Since < 12H</h4>
					</div>
					<div class="stat_number"><?php echo intval($order_stats['order_age']['<12H']); ?></div>
				</div>
				<div class="stat_row">
					<div class="stat_text">
						<h4>For Pick Up</h4>
					</div>
					<div class="stat_number"><?php echo intval($order_stats['for_pickup']); ?></div>
				</div>	
			</div>

			<div class="stat_column performance">
				<h3>Performance</h3>
				<span><?php echo date('d M Y',$this_week).' - '.date('d M Y',$next_week); ?></span>
				<?php 
					//show deals stats
					if ($product_cat != 'goods') {
						do_shortcode("[WPCR_INSERT]");	
					} 
				?>
				<?php if ($product_cat != 'deals'): //show goods stats ?>
					<p class="total"><?php echo number_format($order_stats['performance_average'],0); ?>%</p>
					<div class="stat_row">
						<div class="stat_text">
							<h4>Cancellation Rate</h4>
							<span><?php echo $order_stats['performance_blurbs']['cancel_rate']; ?></span>
						</div>
						<div class="stat_number"><?php echo number_format($order_stats['performance_rates']['cancel_rate'],0); ?>%</div>
					</div>
					<div class="stat_row">
						<div class="stat_text">
							<h4>Ready to Ship < 24H</h4>
							<span><?php echo $order_stats['performance_blurbs']['ready_in_24H']; ?></span>
						</div>
						<div class="stat_number"><?php echo number_format($order_stats['performance_rates']['ready_in_24H'],0); ?>%</div>
					</div>
					<div class="stat_row">
						<div class="stat_text">
							<h4>Return Rate</h4>
							<span><?php echo $order_stats['performance_blurbs']['return_rate']; ?></span>
						</div>
						<div class="stat_number"><?php echo number_format($order_stats['performance_rates']['return_rate'],0); ?>%</div>
					</div>
					<div class="stat_row">
						<div class="stat_text">
							<h4>Shipped in 48H</h4>
							<span><?php echo $order_stats['performance_blurbs']['shipped_in_48']; ?></span>
						</div>
						<div class="stat_number"><?php echo number_format($order_stats['performance_rates']['shipped_in_48'],0); ?>%</div>
					</div>	
				<?php endif; ?>
			</div>


			<div class="stat_column sales_info">
				<h3>Sales Info</h3>
				<span>&nbsp;</span>
				<p class="total">&nbsp;</p>
				<div class="stat_row">
					<div class="stat_text">
						<h4>Sales Today</h4>
					</div>
					<div class="stat_number"><?php echo 'P'.number_format($order_stats['sales_total']['today'],2,'.',','); ?></div>
				</div>
				<div class="stat_row">
					<div class="stat_text">
						<h4>Sales This Week</h4>
					</div>
					<div class="stat_number"><?php echo 'P'.number_format($order_stats['sales_total']['this_week'],2,'.',','); ?></div>
				</div>
				<div class="stat_row">
					<div class="stat_text">
						<h4>Sales This Month</h4>
					</div>
					<div class="stat_number"><?php echo 'P'.number_format($order_stats['sales_total']['this_month'],2,'.',','); ?></div>
				</div>
				<div class="stat_row">
					<div class="stat_text">
						<h4><?php echo 'Package: '.$hdgs_active_package; ?></h4>
						<span>% completion</span>
					</div>
					<div class="stat_number"><?php echo number_format($completion*100,0).'%'; ?></div>
				</div>		
			</div>

			<div class="stat_column product_listings">
				<h3>Product Listings</h3>
				<span>&nbsp;</span>
				<p class="total"><?php echo $product_stats['publish'] + $product_stats['pending']; ?></p>
				<?php 
					if (is_array($product_stats['rejected'])):
						foreach ($product_stats['rejected'] as $reason => $count): 
				?>
							<div class="stat_row">
								<div class="stat_text">
									<h4>Rejected Products</h4>
									<span><?php echo $reason; ?></span>
								</div>
								<div class="stat_number"><?php echo $count; ?></div>
							</div>
				<?php 
						endforeach; 
					endif;
				?>
				<div class="stat_row">
					<div class="stat_text">
						<h4>Approved Products</h4>
					</div>
					<div class="stat_number"><?php echo intval($product_stats['publish']); ?></div>
				</div>
				<div class="stat_row">
					<div class="stat_text">
						<h4>Pending Products</h4>
					</div>
					<div class="stat_number"><?php echo intval($product_stats['pending']); ?></div>
				</div>		
			</div>
		</div>
	<?php
}

//insert the name version of each rating here
/*
For any Performance metrics, below shall be the rating:

90% to 100% = Excellent
80% to 90% = Good
70% to 80% = Fair
60% to 70% = Poor
50% to 60% = Fail
*/
function hdgs_get_performance_text($rate) {
	if ($rate >= 90) {
		return 'Excellent';
	} elseif ($rate >= 80) {
		return 'Good';
	} elseif ($rate >= 70) {
		return 'Fair';
	} elseif ($rate >= 60) {
		return 'Poor';
	} else {
		return 'Fail';
	}
}

//show the admin updates newsfeed
function hdgs_add_newsfeed_dashboard_widget() {
	$args = array('post_status' 	=> 'publish',
								'post_type' 		=> 'post',
								'orderby'				=> 'date',
								'order'					=> 'DESC',
								'category_name' => 'Newsfeed Announcements',
							);
	$alerts = get_posts($args);
?>

	<style>
		div.newsfeed-announcement {
			padding: 1em;
			margin-bottom: 1em;
			border-bottom: solid 1px #eee;
		}
		div.newsfeed-announcement h3 {
			font-weight: bold !important;
		}
	</style>
	<?php	foreach ($alerts as $alert): ?>
		<div class="newsfeed-announcement">
			<h3><?php echo $alert->post_title; ?></h3>
			<p><?php echo $alert->post_content; ?></p>
		</div>
	<?php endforeach; ?>

<?php
}
//show the main new posts
function hdgs_add_mainfeed_dashboard_widget() {
	$args = array( 'post_status'	=> 'publish',
								'post_type'			=> 'post',
								'orderby'				=> 'date',
								'order'					=> 'DESC',
								'category_name' => 'Mainfeed Announcements',

							);
	$alerts = get_posts($args);
?>

	<style>
		div.mainfeed-announcement {
			padding: 1em;
			margin: 1em;
			margin-bottom: 2em;
			background: white;
			display: table;
			border: solid 1px #eee;
		}
		div.mainfeed-announcement div.feed_content {
			display:table-cell;
		}
		div.mainfeed-announcement div.feed_content h3 {
			font-weight: bold !important;
			font-size:110% !important;
		}

		div.mainfeed-announcement div.view_more {
			display:table-cell;
			vertical-align: middle;
			text-align: center;
		}
		div.mainfeed-announcement div.view_more a {
			border: solid 1px #eee;
			padding: 1em;
			margin: 1em;
		}
	</style>
	<?php	foreach ($alerts as $alert): ?>
		<div class="mainfeed-announcement">
			<div class="feed_content">
				<h3><?php echo $alert->post_title; ?></h3>
				<p><?php echo $alert->post_content; ?></p>
			</div>
			<div class="view_more">
				<a href="<?php get_permalink($alert->ID); ?>">View</a>
			</div>
		</div>
	<?php endforeach; ?>

<?php
}

