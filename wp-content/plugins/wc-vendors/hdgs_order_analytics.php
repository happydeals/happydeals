<?php

//This shows the analytics to merchants as a menu on their dashboard

add_action( 'admin_menu', 'hdgs_add_analytics_menu' );
function hdgs_add_analytics_menu() {
	if (current_user_can( 'administrator' )) {
    $role = 'administrator';
  } elseif (current_user_can( 'vendor' )) { 
    $role = 'vendor';
  } else {
    return false;
  }
	
	add_menu_page('Analytics', 
								'Analytics', 
								$role, 
								'hdgs_analytics', 
								'hdgs_analytics',
								'dashicons-chart-area');

	//enqueue chart.js for the pretty charts
	//$script_src = 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js';
	wp_enqueue_script( 'hdgs_chartjs', plugin_dir_url( __FILE__ ) . '/js/Chart.min.js' );
}

//main analytics page
function hdgs_analytics() {
	$allowed_durations = array('week','month');

	if(isset($_GET['duration']) AND in_array($_GET['duration'], $allowed_durations)) {
		$duration = $_GET['duration'];
	} else {
		$duration = 'week';
	}
	
	$durations = array('Last '.ucwords($duration),'This '.ucwords($duration));

	hdgs_show_duration_link();

	hdgs_show_sales_stats($durations); 
	hdgs_show_conversion_stats($durations);
}

//show links to choosing duration
function hdgs_show_duration_link(){
	$now = current_time( 'timestamp', 0 );
	$end_date = date('Y-m-d',$now);
	$duration = 'week';

	if (isset($_GET['start_date'])) {
		
		$start_date = $_GET['start_date'];
		if (isset($_GET['end_date'])) {
			$end_date = $_GET['end_date'];
		}

	} elseif (isset($_GET['duration']) AND 'month' == $_GET['duration']) {
		$duration = 'week';
		$start_date = date('Y-m-d',strtotime('-1 week',$now));

	} else {
		$duration = 'month';
		$start_date = date('Y-m-d',strtotime('-1 month',$now));
	}

	$switch_url = menu_page_url('hdgs_analytics', false).'&duration='.$duration;

	if(isset($_GET['page'])) {
		$page = $_GET['page'];
	}

	?>

		<div class="duration_links" style="margin-top:1em;">
			<a href="<?php echo $switch_url; ?>" ><?php echo 'Show This '.ucwords($duration).' vs. Last '.ucwords($duration); ?></a>
		</div>

		<h4>or choose a custom duration</h4>
		<form method="get">
			<input type="hidden" name="page" value="<?php echo $page; ?>" />
			<input type="hidden" name="duration" value="custom" />
			From <input type="date" name="start_date" value="<?php echo $start_date; ?>" max="<?php echo $end_date; ?>" /> to <input type="date" name="end_date" value="<?php echo $end_date; ?>" max="<?php echo $end_date; ?>" /> <input type="submit" name="submit" value="Submit" />
		</form>

	<?php

}

//show the sales graph
function hdgs_show_sales_stats($durations = array()) {
	$user_id = get_current_user_id(); 
	$orders = hdgs_get_orders_of_seller($user_id);
	
	$orders_graph = array();
	$labels = array();
	$orders_made = array();
	$orders_ready = array();
	$orders_paid_out = array();
	$sales_made = array();
	$customer_orders = array();
	$sales_total = 0;
	$sales_volume = 0;
	$customers = array('New' => array(),'Returning' => array());

	//get date constants
	if (isset($_GET['start_date'])) {

		$start_date = date('M d, Y', strtotime($_GET['start_date']));
		if (isset($_GET['end_date'])) {
			$end_date = date('M d, Y', strtotime($_GET['end_date']));
		}
		if (!$end_date) {
			$end_date = date('M d, Y', current_time( 'timestamp', 0 ));
		}
		$durations = array('From '.$start_date.' to '.$end_date);

	} elseif (!isset($durations)) {
		$durations = array('Last Week','This Week');
	}

	//set the labels
	if (isset($_GET['duration'])) {
		$d = 'This '.ucwords($_GET['duration']);
	} else {
		$d = 'This Week';
	}
	$now_range = hdgs_date_happened($d);
	$date_loop = $now_range['start'];
	while ($date_loop < $now_range['end']) {
		$label_key = date('Md',$date_loop);
		$labels[$label_key] = $label_key;	
		$date_loop +=	(3600*24); //add a day 
	}

	if (is_array($orders) ) { 
		foreach ( $orders as $order_object ) {
			$order = wc_get_order( $order_object );
			$order_id = $order->get_id();

			$parent_id = wp_get_post_parent_id( $order_id );
			$parent_order = wc_get_order( $parent_id );
			
			//get sales data
			$customer_id = $parent_order->get_customer_id();
			$gross_sales = $order->get_subtotal() - $order->get_total_tax();
			$net_sales = $gross_sales - $order->get_total_discount();
			

			//skip unpaid orders
			$date_paid = $order->get_date_paid();
			if ($date_paid) {
				$date_paid = $date_paid->getTimestamp();
				$date_key = date('Md',$date_paid);
			} else {
				continue;
			}			

			//get sales stats for orders. it's counted on a per day basis (instead of timestamp)
			foreach($durations as $duration) {

				//skip if outside duration
				$range = hdgs_date_happened($duration);
				$start_date = $range['start'];
				$end_date = $range['end'];
				if ($end_date < $date_paid OR $date_paid < $start_date) {
					continue;
				} 

				//log orders ready to ship
				if ($order->has_status('ready-to-ship')) {
					if(!isset($orders_ready[$duration][$date_key])) {
						$orders_ready[$duration][$date_key] = '0';
					}
					++$orders_ready[$duration][$date_key];

				//log sales info on completed orders (sales, order count, customers)
				} elseif ($order->has_status('completed')) {
					//set orders made
					if(!isset($orders_made[$duration][$date_key])) {
						$orders_made[$duration][$date_key] = '0';
					}
					++$orders_made[$duration][$date_key]; 

					//set sales made
					if(!isset($sales_made[$duration][$date_key])) {
						$sales_made[$duration][$date_key] = '0';
					}
					$sales_made[$duration][$date_key] += $net_sales; 

					//check if repeat or not and log accordingly					
					$order_ct = wc_get_customer_order_count( $customer_id );
					if ($order_ct > 1) {
						if(!isset($customers['Returning'][$date_key])) {
							$customers['Returning'][$date_key] = 0;
						}
						++$customers['Returning'][$date_key];
					} else {
						if(!isset($customers['New'][$date_key])) {
							$customers['New'][$date_key] = 0;
						}
						++$customers['New'][$date_key];
					}

					//get total sales for current ('this') duration
					if (strpos($duration, 'this') !== FALSE) {
						
						$sales_total += $net_sales; 
						++$sales_volume;
					}

					$payout_status = get_post_meta($order_id, 'hdgs_payout_status', true);
					if ('paid' == $payout_status) {
						if(!isset($orders_made[$duration][$date_key])) {
							$orders_paid_out[$duration][$date_key] = '0';
						}
						++$orders_paid_out[$duration][$date_key]; 
					}
				}

				
			}
		}
	}

	/***************************************************************************
	 ***************************************************************************
	 ** TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA **
	 ** DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST **
	 ** TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA **
	 ** DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST **
	 ** TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA **
	 ***************************************************************************
	 *************************************************************************
						foreach($durations as $duration) {
							foreach($labels as $date_key) {
								$now = current_time( 'timestamp', 0 );
								if (strtotime($date_key) > $now AND strpos($duration, 'This') !== FALSE) {
									@$orders_made[$duration][$date_key] = '0';
									@$sales_made[$duration][$date_key] = '0';
									@$orders_ready[$duration][$date_key] = '0';
									@$orders_paid_out[$duration][$date_key] = '0';
									@$customers['New'][$date_key] = '0';
									@$customers['Returning'][$date_key] = '0';
								} else {
									@$orders_made[$duration][$date_key] = rand(0,50);	
									@$orders_ready[$duration][$date_key] = rand(0,10);
									@$orders_paid_out[$duration][$date_key] = rand(0,10);

									$sales_amount = rand(100,5000);
									@$sales_made[$duration][$date_key] += $sales_amount;
									@$customers['New'][$date_key] = rand(0,20);
									@$customers['Returning'][$date_key] = rand(0,20);
									$sales_total += $sales_amount;
								}
								
							}

						}
		**************************************************************************
	 ***************************************************************************
	 ** TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA **
	 ** DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST **
	 ** TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA **
	 ** DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST **
	 ** TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA **
	 ***************************************************************************
	 ***************************************************************************/

	$repeat_rate = array_sum($customers['Returning'])*100/max(1,(array_sum($customers['Returning'])+array_sum($customers['New'])));

	//SHOW CHARTS
	?>

		<h2>Total Sales</h2>
		<?php echo wc_price($sales_total); ?>

		<h4>Sales over Time</h4>
		<?php hdgs_show_chart ('sales_made', $labels, $sales_made, array('skyblue','royalblue')); ?>
			
		<h4>Orders over Time</h4>
		<?php hdgs_show_chart ('orders_made', $labels, $orders_made, array('orange','orangered')); ?>

		<h4>Orders Ready-to-Ship</h4>
		<?php hdgs_show_chart ('orders_ready', $labels, $orders_ready, array('mediumaquamarine','teal')); ?>

		<h2>Repeat Customer Rate</h2>
		<?php echo number_format($repeat_rate,2).'%'; ?>
		<h4>Customers</h4>
		<?php hdgs_show_chart ('repeat_customer_chart', $labels, $customers,array('violet','purple')); ?>

		<h4>Orders Paid Out</h4>
		<?php hdgs_show_chart ('orders_paid_out', $labels, $orders_paid_out, array('greenyellow','limegreen')); ?>

	<?php
}

function hdgs_show_conversion_stats($durations) {
	$user_id = get_current_user_id(); 
	$data = array();
	$storeviews = 0;
	$past_storeviews = 0;
	$totals = array('page' 			=> 1, 
									'cart' 			=> 0, 
									'checkout' 	=> 0, 
									'order'			=> 0, 
								);

	//get products
	$products = hdgs_get_users_products($user_id);

	//get date constants
	if (isset($_GET['start_date'])) {

		$start_date = date('M d, Y', strtotime($_GET['start_date']));
		if (isset($_GET['end_date'])) {
			$end_date = date('M d, Y', strtotime($_GET['end_date']));
		}
		if (!$end_date) {
			$end_date = date('M d, Y', current_time( 'timestamp', 0 ));
		}
		$durations = array('From '.$start_date.' to '.$end_date);

	} elseif (!isset($durations)) {
		$durations = array('Last Week','This Week');
	}

	//assign start and end times for each constant
	foreach($durations as $duration) {
		$range = hdgs_date_happened($duration);
		$start_date = $range['start'];
		$end_date = $range['end'];
	}

	//set the labels
	if (isset($_GET['duration'])) {
		$d = 'This '.ucwords($_GET['duration']);
	} else {
		$d = 'This Week';
	}
	$now_range = hdgs_date_happened($d);
	$date_loop = $now_range['start'];
	while ($date_loop < $now_range['end']) {
		$label_key = date('Md',$date_loop);
		$labels[$label_key] = $label_key;	
		$date_loop +=	(3600*24); //add a day 
	}

	//get product stats
	foreach ($products as $product) {
		$stats = get_post_meta( $product->ID, 'hdgs_product_stats', true );
		//stats are arranged in key => array of timestamps
		if (is_array($stats)) {
			foreach($stats as $key => $array) {
				foreach($array as $timestamp) {
					$date_key = date('Md',$timestamp);

					//count number of visits during this duration
					foreach($durations as $duration) {

						$range = hdgs_date_happened($duration);
						$start_date = $range['start'];
						$end_date = $range['end'];

						//skip if outside duration
						if ($timestamp < $start_date) {
							continue;
						}
						if ($timestamp > $end_date) {
							continue;
						}
						
						if(!isset($data[$key][$duration][$date_key])) {
							$data[$key][$duration][$date_key] = '0';
						}
						++$data[$key][$duration][$date_key]; 
						
						//count total views for current ('this') duration
						if (strpos($duration, 'This') !== FALSE) {
							++$storeviews;
						} else {
							++$past_storeviews;
						}

					}
				}
			}
		}
	}

	//get purchases made during this range
	$orders = hdgs_get_orders_of_seller($user_id);
	if (is_array($orders) ) { 
		foreach ( $orders as $order_object ) {
			$order = wc_get_order( $order_object );
			$date_created = $order->get_date_created();
			
			if (!$date_created) {
				continue;
			} else {
				$date_created = $date_created->getTimestamp();
				$date_key = date('Md',$date_created);	
			}
				
			foreach($durations as $duration) {
				//skip if outside duration
				if ($date_created < $start_date) {
					continue;
				}
				if ($date_created > $end_date) {
					continue;
				}
				
				if(!isset($data['order'][$duration][$date_key])) {
					$data['order'][$duration][$date_key] = '0';
				}
				++$data['order'][$duration][$date_key]; 
			}
		}
	}

	//get totals for funnel
	foreach($data as $stage => $logs) {
		$totals[$stage] = count($logs);	
	}

	/***************************************************************************
	 ***************************************************************************
	 ** TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA **
	 ** DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST **
	 ** TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA **
	 ** DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST **
	 ** TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA **
	 ***************************************************************************
	 **************************************************************************
				foreach($durations as $duration) {
					foreach($labels as $date_key) {
						$now = current_time( 'timestamp', 0 );
						if (strtotime($date_key) > $now AND strpos($duration, 'This') !== FALSE) {
							$data['page'][$duration][$date_key] = '0';
						} else {
							$visits = rand(0,100);
							$data['page'][$duration][$date_key] = $visits;

							//get total views for current ('this') duration
							if (strpos($duration, 'This') !== FALSE) {
								$storeviews += $visits;
							} else {
								$past_storeviews += $visits;
							}	
						}
					}

					

					$stages = array('cart','checkout','order');
					$totals['page'] = $storeviews;
					$total = $storeviews;
					foreach($stages as $stage) {
						$total = intval(rand(0,100)*$total/100);
						$totals[$stage] = $total;
					}
				}
		**************************************************************************
	 ***************************************************************************
	 ** TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA **
	 ** DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST **
	 ** TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA **
	 ** DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST **
	 ** TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA TEST DATA **
	 ***************************************************************************
	 ***************************************************************************/
		


	$views_change = ($storeviews - $past_storeviews)*100/max(1,$past_storeviews);
	if (!isset($data['page'])) {
		$data['page'] = array();
	}

	//get rates
	$cart_rate = number_format($totals['cart']*100/max(1,$totals['page']),2);
	$checkout_rate = number_format($totals['checkout']*100/max(1,$totals['page']),2);
	$order_rate = number_format($totals['order']*100/max(1,$totals['page']),2);

	//SHOW STORE VIEWS
	?>

		<h2>Total Store Visits</h2>
		<?php echo $storeviews.' visits'; ?>
		<span style="float:right;"><?php echo number_format($views_change,2); ?>%</span>

		<h4>Visits Over Time</h4>
		<?php hdgs_show_chart ('visits_chart', $labels, $data['page'], array('tan','saddlebrown')); ?>

		<h2>Online Store Conversion Rate</h2>
	
		<h4>Conversion Funnel</h4>	
		<table>
			<tbody>
				<tr>
					<td>Product Views:<br/><?php echo $totals['page'].' views'; ?></td>
					<td></td>
				</tr>
				<tr>
					<td>Added to Cart:<br/><?php echo $totals['cart'].' added to cart'; ?></td>
					<td><?php echo $cart_rate.'%'; ?></td>
				</tr>
				<tr>
					<td>Reached Checkout:<br/><?php echo $totals['checkout'].' checked out'; ?></td>
					<td><?php echo $checkout_rate.'%'; ?></td>
				</tr>
				<tr>
					<td>Purchased:<br/><?php echo $totals['order'].' ordered'; ?></td>
					<td><?php echo $order_rate.'%'; ?></td>
				</tr>
			</tbody>
		</table>

	<?php
}

