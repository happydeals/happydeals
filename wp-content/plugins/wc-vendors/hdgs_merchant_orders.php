<?php
 
/************************
**** MERCHANT ORDERS ***
************************/
     
 
/***** ORDER STATUS EDITS ********/
 
//new order statuses: "Ready to Ship" and "Picked Up"
add_action( 'init', 'hdgs_add_new_order_status' );
function hdgs_add_new_order_status() {
    register_post_status( 'wc-ready-to-ship', 
        array('label'                     => 'Ready to Ship',
                'public'                    => true,
                'exclude_from_search'       => false,
                'show_in_admin_all_list'    => true,
                'show_in_admin_status_list' => true,
                'label_count'               => _n_noop( 'Ready to Ship <span class="count">(%s)</span>', 'Ready to Ship <span class="count">(%s)</span>' )
    ) );
    register_post_status( 'wc-picked-up', 
        array('label'                     => 'Picked Up',
                'public'                    => true,
                'exclude_from_search'       => false,
                'show_in_admin_all_list'    => true,
                'show_in_admin_status_list' => true,
                'label_count'               => _n_noop( 'Picked Up <span class="count">(%s)</span>', 'Picked Up <span class="count">(%s)</span>' )
    ) );
}
 
add_filter( 'wc_order_statuses', 'hdgs_add_order_statuses_to_list' );
function hdgs_add_order_statuses_to_list( $order_statuses ) {
    $new_order_statuses = array();
    // add new order status after processing
    foreach ( $order_statuses as $key => $status ) {
        $new_order_statuses[ $key ] = $status;
        if ( 'wc-processing' === $key ) {
            $new_order_statuses['wc-processing'] = 'Paid';
            $new_order_statuses['wc-ready-to-ship'] = 'Ready to Ship';
            $new_order_statuses['wc-picked-up'] = 'Picked Up';
        }
    }
    return $new_order_statuses;
}
 
 
 
 
 
/************ ORDER DETAILS VIEW *************/
 
//add Order Page in dashboard
add_action( 'admin_menu', 'hdgs_add_merchant_order_to_menu' );
function hdgs_add_merchant_order_to_menu() {
  add_submenu_page(null, 
                  'Order Details', 
                  'Order Details', 
                  'vendor', 
                  'merchant_order', 
                  'hdgs_show_merchant_order'
                  );
  add_submenu_page(null, 
                  'Order Details', 
                  'Order Details', 
                  'administrator', 
                  'merchant_order_admin', 
                  'hdgs_show_merchant_order'
                  );
}
 
//show the order to the merchant
function hdgs_show_merchant_order() {
  wp_register_script('hdgs_jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js');
  wp_enqueue_script('hdgs_jquery'); 

  $current_user = wp_get_current_user(); 
  $items = array(); 

  $order_id = $_GET['order'];
  if ($order_id <= 1) {
    return false;
  }

  //set order status if changed
  global $allowed_merchant_statuses;

  $order_status = '';
  if (isset($_GET['order_status'])) {
    $order_status = $_GET['order_status'];
  }

  $order = wc_get_order( $order_id );
  $line_items = $order->get_items();
  
  $child_order_ids = hdgs_get_child_order_ids($order_id, $current_user->ID);
  $child_id = $child_order_ids[0];
  $child_order = wc_get_order( $child_id );
  
  if ($order_status) {

    //only mark child orders
    $result = hdgs_mark_as($order_status, $child_id);    

    if ($result) {
      $new_status = hdgs_translate_order_status($allowed_merchant_statuses[$order_status], $order_id);
      $blurb = ucwords($new_status);
      $_SESSION['hdgs_admin_notices'][] = array('type' => 'success', 
                                                'message' => 'Orders Marked as '.$blurb, 
                                                );
    }
  }
     
  

 
  //check if the items are merchant's
  foreach ( $line_items as $item_id => $item ) {

    //get product owner
    $merchant_id = get_post_field( 'post_author', $item->get_product_id()); 

    //check if product belongs to this user (or is admin)
    if ($merchant_id == $current_user->ID OR is_super_admin()) {
      $items[$item_id] = $item;
    }
  }

  //if there are no items, exit
  if (!array_filter($items)) {
    wp_die( 'You are not allowed to access this info.' );
  }
  
 
  //add styles and scripts
  hdgs_show_merchant_order_styles_scripts();

  //add order header 
  hdgs_show_merchant_order_header($order);
  hdgs_show_admin_notices();

  //add right side info
  hdgs_show_order_customer_info($order);

  //add main info
  hdgs_show_order_details($child_order);
  hdgs_show_order_delivery_info($child_order);
  hdgs_show_order_voucher($child_order);
  hdgs_show_order_timeline($child_order);

  //add extra stuff that only admins can see
  if (is_super_admin()) {
    $is_metabox = false;
    hdgs_show_financials_shop_order ($child_order, $is_metabox);
  }
  

  //add save
  hdgs_show_save_button();
}

//styles and scripts
function hdgs_show_merchant_order_styles_scripts($is_metabox = false){

?>
  <style type="text/css">
    input[type="number"] {
      max-width: 4em;
    }
    .order_top {
      width: 100%;
    }
    .order_top .left_head {
      float: left;
    }
    .order_top .right_head {
      float:right;
    }
    .order_top .right_head a {
      text-decoration: none;
    }
    .order_number {
      width: 100%;
      display:block;
      clear: both;
      font-size:1.8em;
    }
    .wc-order-totals-items {
      float:right;
    }
    .order_details,
    .fulfillments_delivery,
    .customer_data,
    .voucher_container,
    .order_timeline,
    .contract_details,
    .hdgs_financials_shop_order {
      background: white;
      padding: 0.5em;
      /*border: solid 1px #ddd;*/
      margin: 0.5em 0;
      display: inline-block;
    }
    .order_details,
    .fulfillments_delivery,
    .voucher_container,
    .order_timeline,
    .contract_details,
    .hdgs_financials_shop_order {
      <?php if ($is_metabox): ?>
          width:95%;
      <?php else: ?>
          width:70%;
      <?php endif; ?>
    }
    .customer_data {
      width:25%;
      float:right;
    }
    .customer_data input,
    .customer_data textarea {
      max-width:100%;
    }
    .fulfillments_delivery > div:first-child {
      border-bottom: solid 1px #ccc;
      display: inline-block;
      width: 100%;
    }
    .deliver_option,
    .package_info,
    .delivery_info,
    .delivery_notes {
      width:48%;
      height: inherit;
      float:left;
      padding:1%;
    }
    .statuses {
      display: inline-block;
    }
    .status_tag {
      font-size: 0.7em;
      background: #aaa;
      border-radius: 0.5em;
      padding: 0.4em 0.5em;
      color: white;
    }
    .fulfillments_delivery h3 {
      margin-left: 0.5em;
    }
    .delivery_notes h4 {
      margin-top: 0;
    }
    .package_info {
      border-left: solid 1px #ccc;
    }
    .customer_data > div {
      border-bottom: solid 1px #ccc;
      padding: 1em;
    }
    .customer_data > div:last-child {
      border-bottom: 0;
    }
    .customer_name .avatar {
      float:right;
    }

    /**tooltip css from http://www.lets-develop.com/html5-html-css-css3-php-wordpress-jquery-javascript-photoshop-illustrator-flash-tutorial/css-css3-css-3-style-styling/style-tooltip-css-how-to-style-the-title-attribute-with-css/**/
    a.tooltip span{
      z-index: 10;
      display: none;
      border-radius:4px;
      -moz-border-radius: 4px;
      -webkit-border-radius: 4px;
    }
    a.tooltip:hover span{
      display: inline;
      position: absolute;
      border: 1px solid #8c8c8c;
      background: #f4f4f4;
    }
    a.tooltip > span{
      width: 320px;
      padding: 10px 12px;
      opacity: 0;
      visibility: hidden;
      z-index: 10;
      position: absolute;
      font-size: 0.9em;
      font-style: normal;
      font-weight: normal;
      -webkit-border-radius: 3px;
      -moz-border-radius: 3px; -o-border-radius: 3px;
      border-radius: 3px;
      -webkit-box-shadow: 4px 4px 4px #d9b3c3;
      -moz-box-shadow: 4px 4px 4px #d9b3c3;
      box-shadow: 4px 4px 4px #d9b3c3;
    }
    a.tooltip:hover > span{
      opacity: 1;
      text-decoration:none;
      visibility: visible;
      overflow: visible;
      margin-top: 40px;
      display: inline;
      margin-left: -200px;
    }
    a.tooltip span b{
      width: 15px;
      height: 15px;
      margin-left:  0px;
      margin-top: -19px;
      display: block;
      position: absolute;
      -webkit-transform: rotate(-45deg);
      -moz-transform: rotate(-45deg);
      -o-transform: rotate(-45deg);
      transform: rotate(-45deg);
      -webkit-box-shadow: inset -1px 1px 0 #fff;
      -moz-box-shadow: inset 0 1px 0 #fff; -o-box-shadow: inset 0 1px 0 #fff;
      box-shadow: inset 0 1px 0 #fff;
      display: none\0/; *display: none;
      background: #f4f4f4;
      border-top: 1px solid #8c8c8c;
      border-right: 1px solid #8c8c8c;
    }
    a.tooltip > span{
      color: #000000;
      background: #f4f4f4;
      background: -moz-linear-gradient(top, #FBF5E6 0%, #FFFFFF 100%);
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#FBF5E6), color-stop(100%,#FFFFFF));
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#FBF5E6', endColorstr='#FFFFFF',GradientType=0 );
      border: 1px solid #8c8c8c;
    }
    a.tooltip span img {
      width: 100%;
      margin-top: 1em;
    }
    .package_info .read_me {
      text-decoration: none;
    }
    .package_info .read_me:before {
      font-family: Dashicons;
      speak: none;
      font-weight: 400;
      font-variant: normal;
      text-transform: none;
      line-height: 1;
      -webkit-font-smoothing: antialiased;
      margin: 0;
      text-indent: 0;
      width: 200%;
      height: 200%;
      text-align: center;
      content: "";
      cursor: help;
      font-size: 1.3em;
      color: black !important;
         
    }
    .package_dimension td {
      display: none;
    }
    .more_actions,
    a[href="#hide_actions"] {
      display: none;
    }
    img {
      width:20px;
      height:auto;
    }
    a[href="#cancel_address"],
    textarea.billing_address {
      display: none;
    }
    .deliver_actions {
      float:right;
      width: 100%;
      text-align: right;
    }
    .order_timeline .top_box,
    .order_timeline ul {
      margin-left:2em;
    }

    .order_timeline .top_box {
      margin-bottom: 1em;
    }
    .order_timeline h3 {
      border-bottom: solid 1px #ccc;

    }
    .order_timeline ul {
      padding-left: 2em;
      max-height:800px;
      overflow-y: scroll;
    }
    .order_timeline ul li {
      font-size: 2em;
      list-style-type: disc;
      width: 50%;
      min-width:320px;
      margin-bottom:2em;
    }
    .order_timeline ul li:before {
      margin-top:1em;
    }
    .order_timeline ul li div {
      font-size: 0.5em;
      margin-bottom:1em;
    }
    .order_timeline ul li span {
      float:right;
      display: block;
      font-size: 0.4em;
      line-height: 1em;
      color: #888;
    }
    input[name="save_order"] {
      display: block;
      clear: both;
      width: 25%;
      float: right;
      font-weight: bold;
    }
    @media only screen and (max-width: 640px) {
      .order_details,
      .fulfillments_delivery,
      .voucher_container,
      .order_timeline,
      .customer_data,
      input[name="save_order"]{
        width: 95%;
        float:none;
      }
    }
    .grayed_out {
      color:gray !important;
    }
    .payout_details {
      width:40%;
      float:left;
    }
    .payout_details table {
      width: 100%;
      
    }
    .indented {
      padding-left:1em;
    }
    .bolded {
      font-weight: bold;
    }
    .payout_details table tr td:nth-child(2) {
      text-align:right;
    }
    .fin_remarks {
      width:50%;
      float:left;
      margin-left:5%;
    }
    .fin_remarks textarea {
      width: 100%;
      height: 8em;
    }
    .contract_details table {
      background: white;
      margin-bottom: 2em;
    }
    .contract_details table tr td:first-child {
      width: 15em;
    }
    .add_charges {
      display:none;
    }
    .add_charges div {
      display:block;
      width: 100%;
    }
    .add_charges div input {
      min-width: 25%;
    }
    .final_save {
      width: 100%;
    }
    .final_save input[type="submit"] {
      float:right;
    }
  </style>
  <script type="text/javascript">
    jQuery(document).ready(function(){
      $('select#package_size').change(function(){
        if ($(this).val() == 'own') {
          $('.package_dimension td').show();
        } else {
          $('.package_dimension td').hide();
        }
      });

      //show actions menu
      $('a[href="#more_actions"]').click(function(){
        $('a[href="#hide_actions"]').show();
        $('a[href="#more_actions"]').hide();
        $('.more_actions').show();
      });
      $('a[href="#hide_actions"]').click(function(){
        $('a[href="#more_actions"]').show();
        $('a[href="#hide_actions"]').hide();
        $('.more_actions').hide();
      });

      //show edit address
      $('a[href="#edit_address"]').click(function(){
        $('textarea.billing_address').show();
        $('a[href="#cancel_address"]').show();
        $('.shipping_address').hide();
      });
      $('a[href="#cancel_address"]').click(function(){
        $('textarea.billing_address').hide();
        $('a[href="#cancel_address"]').hide();
        $('.shipping_address').show();
      });

      //show charges menu
      $('a[href="#add_charges"]').click(function(){
        $('.add_charges').show();
        $('a[href="#cancel_add_charges"]').show();
        $('a[href="#add_charges"]').hide();
      });
      $('a[href="#cancel_add_charges"]').click(function(){
        $('.add_charges').hide();
        $('a[href="#cancel_add_charges"]').hide();
        $('a[href="#add_charges"]').show();
      });
    });
  </script>
<?php
}
 
//form header and other top page stuff
function hdgs_show_merchant_order_header($order) {
    
  //setup data
  $order_id = $order->get_id();
  $order_data = $order->get_data();
  $date_created = $order_data['date_created']->getTimestamp();
  $orders_url = admin_url('admin.php?page=wcv-vendor-orders');
  $prev_oid = '';
  $next_oid = '';
  $page = '';

  //get previous and next orders
  $current_user = wp_get_current_user();
  $orders = hdgs_get_orders_of_seller($current_user->ID);
  foreach ($orders as $o) {
    $all_ids[$o->ID] = $o->ID; 
  }
  asort($all_ids);

  //get the previous and next order_ids by walking through the array
  reset($all_ids);
  while (!in_array(key($all_ids), [$order_id, null])) {
    $start = current($all_ids);
    next($all_ids);
  }
  if (current($all_ids) !== false) {
    $prev_oid = prev($all_ids);
    next($all_ids);
    $next_oid = next($all_ids);
  }
  //fix if at first order
  if (isset($start) AND $start == current($all_ids)) {
    reset($all_ids);
    $next_oid = next($all_ids);
  }

  //set the links for the arrows
  if ($prev_oid) {
    $prev_order_url = '<a href="'.admin_url('admin.php?page=merchant_order&order='.$prev_oid).'"><-</a>';
  } else {
    $prev_order_url = '<span class="grayed_out"><-</span>';
  }
  if($next_oid) {
    $next_order_url = '<a href="'.admin_url('admin.php?page=merchant_order&order='.$next_oid).'">-></a>';
  } else {
    $next_order_url = '<span class="grayed_out">-></span>';
  }
  
  global $allowed_merchant_statuses;
  $order_change = $allowed_merchant_statuses;
  unset($order_change[$order->get_status()]); //remove existing status from options

  if (isset($_GET['page']) 
      AND ('order_financials' == $_GET['page'] 
          OR 'merchant_order' == $_GET['page']
          OR 'merchant_order_admin' == $_GET['page']) ){
    $page = $_GET['page'];
  } 

  $child_order_ids = hdgs_get_child_order_ids($order_id, $current_user->ID);
  $child_id = $child_order_ids[0];

  ?>
    
    <form method="post" action="admin-post.php">
      <input type="hidden" name="action" value="hdgs_update_order" />
      <input type="hidden" name="oid" value="<?php echo $child_id; ?>" />
      <input type="hidden" name="page" value="<?php echo $page; ?>" />
      <div class="order_header">
        <div class="order_top">
          <div class="left_head">
              <a href="<?php echo $orders_url; ?>">Orders</a>
          </div>
            <div class="right_head" style="display:none;">
              <?php echo $prev_order_url; ?>&nbsp;<?php echo $next_order_url; ?>
          </div>
        </div>
        <div class="order_number">
          #<?php echo $order_id; ?> <span><?php echo date('F d, Y g:i A',$date_created); ?></span>
        </div>
        <div class="order_actions">
          <a href="#print" onclick="window.print();">🖨 Print Order</a>
          <a href="#more_actions">More Actions</a>
          <a href="#hide_actions">Hide Actions</a>
          <div class="more_actions">
            <a href="<?php echo $track_shipment_url; ?>">Track Shipment</a>
            <?php foreach ($order_change as $status => $blurb): ?>
              <a href="<?php echo admin_url('admin.php?page=merchant_order&order_status='.$status.'&order='.$order_id); ?>">Mark as <?php echo  $blurb; ?></a>
            <?php endforeach; ?>
          </div>
        </div>
    </div>
  <?php
}
 
//show the order details in the merchant's order page
function hdgs_show_order_details($order) {
  global $post, $wpdb;
  $order_id = $order->get_id();
  $current_user = wp_get_current_user();
  $order_discount = 0;
  $order_subtotal = 0;
  $order_total = 0;
  $order_refunded = 0;
  $status_tags = '';

  // Get line items
  $line_items = $order->get_items();

  //check if the items are merchant's (except if super admin)
  if (current_user_can('vendor')) {

    foreach ( $line_items as $item_id => $item ) {
      $product_id = $item->get_product_id();
      $merchant_id = get_post_field( 'post_author', $product_id); 
      if ($merchant_id != $current_user->ID) {
          unset($line_items[$item_id]);
      }
    }

    //if there are no items, exit
    if (!$line_items) {
      return false;
    }
  }

  if ( wc_tax_enabled() ) {
    $order_taxes      = $order->get_taxes();
    $tax_classes      = WC_Tax::get_tax_classes();
    $classes_options  = wc_get_product_tax_class_options();
    $show_tax_columns = count( $order_taxes ) === 1;
  }

  //set order status
  $statuses['order'] = hdgs_translate_order_status($order->get_status(), $order_id);

  //for deals
  $show_payment_accept = false;
  $show_redeemed = false;

  $product_cats = hdgs_get_product_cats($order_id);
  if ($product_cats['hasdeals']) {
    if ($order->get_status() == 'processing') {
      $show_payment_accept = true;
    }
    if ($order->get_status() == 'completed') {
      $show_payment_accept = true;
      $show_redeemed = true;
    }
  }

  //set shipping total if blank
  if (!$order->get_shipping_total()) {
    $shipping_cost = hdgs_get_shipping_cost($order_id);
    $order->set_shipping_total($shipping_cost);
  }

  ?>
    <div class="order_details">
      <h3>
        Order Details 
        <div class="statuses">
        <?php foreach ($statuses as $status): ?>
          <span class="status_tag"><?php echo ucwords($status); ?></span>
        <?php endforeach; ?>
        </div>
      </h3> 
        <div class="woocommerce_order_items_wrapper wc-order-items-editable">
            <table cellpadding="0" cellspacing="0" class="woocommerce_order_items">
                <thead>
                    <tr>
                        <th class="item sortable" colspan="2" data-sort="string-ins"><?php _e( 'Item', 'woocommerce' ); ?></th>
                        <th class="item_cost sortable" data-sort="float"><?php _e( 'Cost', 'woocommerce' ); ?></th>
                        <th class="quantity sortable" data-sort="int"><?php _e( 'Qty', 'woocommerce' ); ?></th>
                        <th class="line_cost sortable" data-sort="float"><?php _e( 'Total', 'woocommerce' ); ?></th>
                        <?php
                            if ( ! empty( $order_taxes ) ) :
                                foreach ( $order_taxes as $tax_id => $tax_item ) :
                                    $tax_class      = wc_get_tax_class_by_tax_id( $tax_item['rate_id'] );
                                    $tax_class_name = isset( $classes_options[ $tax_class ] ) ? $classes_options[ $tax_class ] : __( 'Tax', 'woocommerce' );
                                    $column_label   = ! empty( $tax_item['label'] ) ? $tax_item['label'] : __( 'Tax', 'woocommerce' );
                                    $column_tip     = sprintf( esc_html__( '%1$s (%2$s)', 'woocommerce' ), $tax_item['name'], $tax_class_name );
                                    ?>
                                    <th class="line_tax tips" data-tip="<?php echo esc_attr( $column_tip ); ?>">
                                        <?php echo esc_attr( $column_label ); ?>
                                    </th>
                                    <?php
                                endforeach;
                            endif;
                        ?>
                        <th class="wc-order-edit-line-item" width="1%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody id="order_line_items">
                <?php
                    foreach ( $line_items as $item_id => $item ) {
                        $item_discount = 0;
                        $line_discount = 0;
                         
                        $product      = $item->get_product();
                        $product_link = admin_url( 'post.php?post=' . $item->get_product_id() . '&action=edit' );
                        $thumbnail    = $product->get_image( 'thumbnail', array( 'title' => '' ), false );
 
                    ?>
                        <tr class="item">
                            <td class="thumb">
                                <?php echo '<div class="wc-order-item-thumbnail">' . wp_kses_post( $thumbnail ) . '</div>'; ?>
                            </td>
                            <td class="name" data-sort-value="<?php echo esc_attr( $item->get_name() ); ?>">
                                <?php
                                    echo $product_link ? '<a href="' . esc_url( $product_link ) . '" class="wc-order-item-name">' . esc_html( $item->get_name() ) . '</a>' : '<div class="wc-order-item-name">' . esc_html( $item->get_name() ) . '</div>';
 
                                    if ( $product && $product->get_sku() ) {
                                        echo '<div class="wc-order-item-sku"><strong>' . __( 'SKU:', 'woocommerce' ) . '</strong> ' . esc_html( $product->get_sku() ) . '</div>';
                                    }
 
                                    if ( $item->get_variation_id() ) {
                                        echo '<div class="wc-order-item-variation"><strong>' . __( 'Variation ID:', 'woocommerce' ) . '</strong> ';
                                        if ( 'product_variation' === get_post_type( $item->get_variation_id() ) ) {
                                            echo esc_html( $item->get_variation_id() );
                                        } else {
                                            /* translators: %s: variation id */
                                            printf( esc_html__( '%s (No longer exists)', 'woocommerce' ), $item->get_variation_id() );
                                        }
                                        echo '</div>';
                                    }
 
                          
                                ?>
                            </td>
                            <td class="item_cost" width="1%" data-sort-value="<?php echo esc_attr( $order->get_item_subtotal( $item, false, true ) ); ?>">
                                <div class="view">
                                    <?php
                                        echo wc_price( $order->get_item_total( $item, false, true ), array( 'currency' => $order->get_currency() ) );
 
                                        if ( $item->get_subtotal() !== $item->get_total() ) {
                                            $item_discount = $order->get_item_subtotal( $item, false, false ) - $order->get_item_total( $item, false, false );
                                            echo '<span class="wc-order-item-discount">-' . wc_price( wc_format_decimal( $item_discount, '' ), array( 'currency' => $order->get_currency() ) ) . '</span>';
                                        }
                                    ?>
                                </div>
                            </td>
                            <td class="quantity" width="1%">
                                <div class="view">
                                    <?php
                                        echo '<small class="times">&times;</small> ' . esc_html( $item->get_quantity() );
 
                                        if ( $refunded_qty = $order->get_qty_refunded_for_item( $item_id ) ) {
                                            echo '<small class="refunded">' . ( $refunded_qty * -1 ) . '</small>';
                                        }
                                    ?>
                                </div>
                                <div class="edit" style="display: none;">
                                    <input type="number" step="<?php echo apply_filters( 'woocommerce_quantity_input_step', '1', $product ); ?>" min="0" autocomplete="off" name="order_item_qty[<?php echo absint( $item_id ); ?>]" placeholder="0" value="<?php echo esc_attr( $item->get_quantity() ); ?>" data-qty="<?php echo esc_attr( $item->get_quantity() ); ?>" size="4" class="quantity" />
                                </div>
                                <div class="refund" style="display: none;">
                                    <input type="number" step="<?php echo apply_filters( 'woocommerce_quantity_input_step', '1', $product ); ?>" min="0" max="<?php echo $item->get_quantity(); ?>" autocomplete="off" name="refund_order_item_qty[<?php echo absint( $item_id ); ?>]" placeholder="0" size="4" class="refund_order_item_qty" />
                                </div>
                            </td>
                            <td class="line_cost" width="1%" data-sort-value="<?php echo esc_attr( $item->get_total() ); ?>">
                                <div class="view">
                                    <?php
                                        $order_total += $item->get_total();
                                        echo wc_price( $item->get_total(), array( 'currency' => $order->get_currency() ) );
 
                                        if ( $item->get_subtotal() !== $item->get_total() ) {
                                            $line_discount = $item->get_subtotal() - $item->get_total();
                                            echo '<span class="wc-order-item-discount">-' . wc_price( wc_format_decimal( $line_discount, '' ), array( 'currency' => $order->get_currency() ) ) . '</span>';
                                            $order_discount += $line_discount;
                                        }
 
                                        if ( $refunded = $order->get_total_refunded_for_item( $item_id ) ) {
                                            echo '<small class="refunded">' . wc_price( $refunded, array( 'currency' => $order->get_currency() ) ) . '</small>';
                                            $order_refunded += $refunded;
                                        }
                                    ?>
                                </div>
                                <div class="refund" style="display: none;">
                                    <input type="text" name="refund_line_total[<?php echo absint( $item_id ); ?>]" placeholder="<?php echo wc_format_localized_price( 0 ); ?>" class="refund_line_total wc_input_price" />
                                </div>
                            </td>
 
                            <?php
                                if ( ( $tax_data = $item->get_taxes() ) && wc_tax_enabled() ) {
                                    foreach ( $order_taxes as $tax_item ) {
                                        $tax_item_id       = $tax_item->get_rate_id();
                                        $tax_item_total    = isset( $tax_data['total'][ $tax_item_id ] ) ? $tax_data['total'][ $tax_item_id ] : '';
                                        $tax_item_subtotal = isset( $tax_data['subtotal'][ $tax_item_id ] ) ? $tax_data['subtotal'][ $tax_item_id ] : '';
                                        ?>
                                        <td class="line_tax" width="1%">
                                            <div class="view">
                                                <?php
                                                    if ( '' != $tax_item_total ) {
                                                        echo wc_price( wc_round_tax_total( $tax_item_total ), array( 'currency' => $order->get_currency() ) );
                                                    } else {
                                                        echo '&ndash;';
                                                    }
 
                                                    if ( $item->get_subtotal() !== $item->get_total() ) {
                                                        if ( '' === $tax_item_total ) {
                                                            echo '<span class="wc-order-item-discount">&ndash;</span>';
                                                        } else {
                                                            echo '<span class="wc-order-item-discount">-' . wc_price( wc_round_tax_total( $tax_item_subtotal - $tax_item_total ), array( 'currency' => $order->get_currency() ) ) . '</span>';
                                                        }
                                                    }
 
                                                    if ( $refunded = $order->get_tax_refunded_for_item( $item_id, $tax_item_id ) ) {
                                                        echo '<small class="refunded">' . wc_price( $refunded, array( 'currency' => $order->get_currency() ) ) . '</small>';
                                                    }
                                                ?>
                                            </div>
                                        </td>
                                        <?php
                                    }
                                }
                            ?>
                        </tr>
                        <?php
                    }
                ?>
                </tbody>
                <tbody id="order_shipping_line_items">
                <?php
                    $shipping_methods = WC()->shipping() ? WC()->shipping->load_shipping_methods() : array();
                    foreach ( $line_items as $item_id => $item ) {
                        $merchant_id = get_post_field( 'post_author', $item->get_product_id()); 
                        if ($merchant_id != $current_user->ID) {
                            continue;
                        }
                    }
                ?>
                </tbody>
            </table>
        </div>
        <div class="wc-order-data-row wc-order-totals-items wc-order-items-editable">
            <?php
                $coupons = $order->get_items( 'coupon' );
                if ( $coupons ) {
                    ?>
                    <div class="wc-used-coupons">
                        <ul class="wc_coupon_list"><?php
                            echo '<li><strong>' . __( 'Coupon(s)', 'woocommerce' ) . '</strong></li>';
                            foreach ( $coupons as $item_id => $item ) {
                                $merchant_id = get_post_field( 'post_author', $item->get_product_id()); 
                                if ($merchant_id != $current_user->ID) {
                                    continue;
                                }
                                $post_id = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM {$wpdb->posts} WHERE post_title = %s AND post_type = 'shop_coupon' AND post_status = 'publish' LIMIT 1;", $item->get_code() ) );
                            }
                        ?></ul>
                    </div>
                    <?php
                }
            ?>
            <?php if ($order->get_customer_note()): ?>
                <div class="customer_note">
                    <h3>Note:</h3>
                    <p><?php echo $order->get_customer_note(); ?></p>
                </div>
            <?php endif; ?>
            <table class="wc-order-totals">
              <?php if ( $order_discount ) : ?>
                <tr>
                  <td class="label"><?php _e( 'Discount:', 'woocommerce' ); ?></td>
                  <td width="1%"></td>
                  <td class="total">
                      <?php echo wc_price( $order_discount, array( 'currency' => $order->get_currency() ) ); ?>
                  </td>
                </tr>
              <?php endif; ?>

              <?php if ( $order->get_shipping_methods() AND $order->get_shipping_total()) : ?>
                <tr>
                  <td class="label"><?php _e( 'Shipping:', 'woocommerce' ); ?></td>
                  <td width="1%"></td>
                  <td class="total"><?php
                    if ( ( $refunded = $order->get_total_shipping_refunded() ) > 0 ) {
                      echo '<del>' . strip_tags( wc_price( $order->get_shipping_total(), array( 'currency' => $order->get_currency() ) ) ) . '</del> <ins>' . wc_price( $order->get_shipping_total() - $refunded, array( 'currency' => $order->get_currency() ) ) . '</ins>';
                    } else {
                      echo wc_price(  $order->get_shipping_total(), array( 'currency' => $order->get_currency() ) );
                    }
                  ?></td>
                </tr>
              <?php endif; ?>

              <?php if ( $shipping_cost ) : ?>
                <tr>
                  <td class="label"><?php _e( 'Shipping:', 'woocommerce' ); ?></td>
                  <td width="1%"></td>
                  <td class="total"><?php echo wc_price( $shipping_cost, array( 'currency' => $order->get_currency() ) ); ?></td>
                </tr>
              <?php endif; ?>


              <?php if ( wc_tax_enabled() ) : ?>
                <?php foreach ( $order->get_tax_totals() as $code => $tax ) : ?>
                  <tr>
                    <td class="label"><?php echo $tax->label; ?>:</td>
                    <td width="1%"></td>
                    <td class="total"><?php
                      if ( ( $refunded = $order->get_total_tax_refunded_by_rate_id( $tax->rate_id ) ) > 0 ) {
                        echo '<del>' . strip_tags( $tax->formatted_amount ) . '</del> <ins>' . wc_price( WC_Tax::round( $tax->amount, wc_get_price_decimals() ) - WC_Tax::round( $refunded, wc_get_price_decimals() ), array( 'currency' => $order->get_currency() ) ) . '</ins>';
                      } else {
                        echo $tax->formatted_amount;
                      }
                    ?></td>
                  </tr>
                <?php endforeach; ?>
              <?php endif; ?>
              <tr>
                <td class="label"><?php _e( 'Total', 'woocommerce' ); ?>:</td>
                <td width="1%"></td>
                <td class="total">
                  <?php echo wc_price($order_total + $shipping_cost, array( 'currency' => $order->get_currency() ) ); ?>
                </td>
              </tr>
              <?php if ( $order_refunded ) : ?>
                <tr>
                  <td class="label refunded-total"><?php _e( 'Refunded', 'woocommerce' ); ?>:</td>
                  <td width="1%"></td>
                  <td class="total refunded-total">-<?php echo wc_price( $order_refunded, array( 'currency' => $order->get_currency() ) ); ?></td>
                </tr>
              <?php endif; ?>
            </table>
            <div class="clear"></div>
        </div>
    <?php if ($show_payment_accept): ?>
      <div class="deal_paid">Payment of <?php echo wc_price($order->get_total()); ?> was accepted</div>
    <?php endif; ?>

    <?php if ($show_redeemed): ?>
      <div class="show_redeemed">Redeemed</div>
    <?php endif; ?>
  </div> 

<?php
}
 
// show the delivery info for the order. includes pickup info, packaging info and courier info
function hdgs_show_order_delivery_info($order, $is_metabox = false) {
  $order_id = $order->get_id();
  $parent_id = wp_get_post_parent_id($order_id);
  if (!$order_id) {
    return false;
  }
  $line_items = $order->get_items();
  $current_user = wp_get_current_user();
  
  $package_style = '';
  $denominator = 3500;

  $act_delivery_date = '';
  $est_delivery_date = '';
  $pickup_reason = '';
  $delivery_status = '';

  if (!$is_metabox) {
    global $allowed_merchant_statuses;
    $order_change = $allowed_merchant_statuses;
    unset($order_change[$order->get_status()]);  
  }
  
  //get packages, minimum 1
  $package_count = max(1,get_post_meta($order_id,'package_count',true));

  //get weights.
  $calc_ship_weight = hdgs_get_calc_ship_weight($order_id);
  $total_weight = hdgs_get_total_weight($order_id);

  if (!$total_weight) {
    return false; //if deals, skip since no shipping involved
  }

  //get branch details from  order meta, then from branch / user meta
  $pickup_branch_id = get_post_meta($order_id, 'hdgs_pickup_branch', true);
  $hdgs_account_info = get_user_meta( $pickup_branch_id, 'hdgs_account_info', true );
  $pickup_branch = $hdgs_account_info['company_name'];
  $pickup_address = $hdgs_account_info['legal_address'];

  $deliver_notes = get_post_meta($order_id, '_wcst_custom_text', true);

  $mark_picked_up_url = admin_url('admin.php?page=merchant_order&order_status=picked-up&order='.$order_id);
  $mark_delivered_url = admin_url('admin.php?page=merchant_order&order_status=completed&order='.$order_id);

  $package_size = get_post_meta($order_id,'package_size',true);
  if (!$package_size) {
    $package_size = 0;
  }
  if ($package_size == 'own') {
    $package_style = 'display:table-cell;';
  }
  $package_sizes = array ('0' =>  'Choose Packaging', 
                          'small' => 'Small Pouch', 
                          'medium' => 'Medium Pouch', 
                          'large' => 'Large Pouch', 
                          'own' => 'Own Packaging',
                        );

  //allow printing to label
  $shipping_label_url = hdgs_get_current_url().'&print_label=true';
  if (isset($_GET['print_label']) AND 'true' == $_GET['print_label']) {
    hdgs_print_shipping_label($order_id);
  }

  //edit order url for admin
  $edit_order_url = admin_url('admin.php?page=merchant_order_admin&order='.$order_id);

  //get delivery info from API 
  $tracking_number = get_post_meta($order_id,'_wcst_order_trackno',true);
  $delivery_data = hdgs_get_delivery_data($order_id);

  //If within Metro Manila 1 to 2 days delivery, outside MM 3 to 7 days delivery. All couriers do not deliver on Sundays and Holidays. https://trello.com/c/ZBWqdCV2/29-merchant-tool-dashboard-page#comment-5a8a4a9459de1cccacb83548
  $picked_up_date = get_post_meta($order_id,'pickup_date',true);
  $address = strtolower($order->get_formatted_shipping_address());
  if ($picked_up_date) {
    if (strpos($address, 'manila') !== FALSE) {
      $est_delivery_date = $picked_up_date + 48*3600; //2 days if within metro manila
    } else {
      $est_delivery_date = $picked_up_date + 7*24*3600;
    }
    $pickup_status = 'Picked Up';
  } else {
    $pickup_status = 'Not Picked Up';
  }

   //get $pickup_reason from API
  if (isset($delivery_data['remarks'])) {
    $pickup_remarks = $delivery_data['remarks'];  
  }

  if(isset($delivery_data['pickup_status'])) {
    $pickup_status = ucwords($delivery_data['pickup_status']);  
  }  

  //set statuses
  if (isset($delivery_data['delivery_status'])) {
    $delivery_status = ucwords($delivery_data['delivery_status']);
    if (isset($delivery_data['date']) AND 'delivered' == $delivery_data['delivery_status']) {
      $act_delivery_date = $delivery_data['date'];
    }
  } 

  $track_shipment_url = hdgs_get_current_url();

  ?>

  <?php if(!$is_metabox): ?>
    <h2 class="fulfillments_delivery_h2">Fulfillments and Delivery</h2>
  <?php endif; ?>
  <div class="fulfillments_delivery">
    <div>
      <h3>#<?php echo $parent_id; ?></h3>
      <table class="deliver_option"><tbody>
        <tr>
          <td><h4>Delivery Option:</h4></td>
          <td><h4><?php echo $order->get_shipping_method(); ?></h4></td>
        </tr>
        <tr>
          <td>Customer Pick Up Branch:</td>
          <td><?php echo $pickup_branch; ?></td>
        </tr>
        <tr>
          <td>Customer Pick Up Address:</td>
          <td><?php echo $pickup_address; ?></td>
        </tr>
        <tr>
          <td>Pick Up Status:</td>
          <td><?php echo $pickup_status; ?></td>
        </tr>
        <tr>
          <td>Pick Up Remarks:</td>
          <td><?php echo $pickup_remarks; ?></td>
        </tr>
      </tbody></table>

      <table class="package_info"><tbody>
        <tr>
          <td colspan=2>
            <h3>PACKAGE INFO: 
              <a class="tooltip read_me" href="#">
                <span>
                  <b></b><!--the small arrow on top-->
                  Please double check the package info if correct, to help the courier deliver the item properly.<br/><br/>
                  Here's a handy guide to help you with choosing the right package size:
                  <img src="<?php echo plugins_url('shipping_tooltip.png', __FILE__); ?>" />
                </span>
              </a>
            </h3>
          </td> 
        </tr>
        <tr>
          <td>Package Count:</td>
          <td>
            <?php 
              if ($is_metabox):
                echo $package_count;
              else:
            ?>
              <input name="package_count" type="number" value="<?php echo $package_count; ?>" step="1" min="1" />
            <?php endif; ?>
          </td>
        </tr>
        <tr>
          <td>Package Size:</td>
          <td>
            <?php 
              if ($is_metabox):
                echo ucwords($package_size);
              else:
            ?>
              <select id="package_size" name="package_size">
                <?php foreach ($package_sizes as $val => $blurb): ?>
                  <option value="<?php echo $val; ?>" <?php selected( $val, $package_size); ?> ><?php echo $blurb; ?></option>
                <?php endforeach; ?>
              </select>
            <?php endif; ?>
          </td>
        </tr>
        <tr class="package_dimension">
          <td style="<?php echo $package_style; ?>">Package Dimension:<br/>
            <span>L x W x H</span></td>
          <td style="<?php echo $package_style; ?>">
            <?php 
              if ($is_metabox):
                echo $length.'cm X '.$width.'cm X '.$height.'cm';
              else:
            ?>
              <input name="length" type="number" value="<?php echo $length; ?>">cm X <input name="width" type="number" value="<?php echo $width; ?>">cm X <input name="height" type="number" value="<?php echo $height; ?>">cm
            <?php endif; ?>
          </td>
        </tr>
        <tr>
          <td>Actual Weight:</td>
          <td>
            <?php 
              if ($is_metabox):
                echo ceil($total_weight).'kg';
              else:
            ?>
              <input name="total_weight" type="number" value="<?php echo ceil($total_weight); ?>" />kg
            <?php endif; ?>
          </td>
        </tr>
        <tr>
          <td>Calc Ship Weight:</td>
          <td><?php echo ceil($calc_ship_weight); ?>kg</td>
        </tr>
      </tbody></table>
    </div>
   
    <h3>DELIVERY INFO:</h3>
    <div>
      <table class="delivery_info"><tbody>
        <tr>
          <td>Estimated Courier Pick Up Date:</td>
          <td><?php if ($picked_up_date) echo date('F d, Y, g:i A',$picked_up_date); ?></td>
        </tr>
        <tr>
          <td>Estimated Delivery Date:</td>
          <td><?php if ($est_delivery_date) echo date('F d, Y, g:i A',$est_delivery_date); ?></td>
        </tr>
        <tr>
          <td>Tracking No.:</td>
          <td><?php echo $tracking_number; ?></td>
        </tr>
        <tr>
          <td>Delivery Status:</td>
          <td><?php echo $delivery_status; ?></td>
        </tr>
        <tr>
          <td>Actual Delivery Date:</td>
          <td><?php if ($act_delivery_date) echo date('F d, Y, g:i A',$act_delivery_date); ?></td>
        </tr>
      </tbody></table>

      <div class="delivery_notes">
        <h4>Delivery Notes:</h4>
        <p><?php echo $deliver_notes; ?></p>
      </div>
    </div>

    <div class="deliver_actions">
      <?php if ($is_metabox): //show edit details link if admin ?>
        <a href="<?php echo $edit_order_url; ?>#order_timeline" target="_blank">Post to Timeline</a> 
        <a href="<?php echo $edit_order_url; ?>" target="_blank">Edit Order Details</a>
      <?php else: ?>
        <a href="<?php echo $shipping_label_url; ?>" target="_blank">Print Shipping Label</a>
      <?php endif; ?>
      <a href="#more_actions">More Actions</a>
      <a href="#hide_actions">Hide Actions</a>
      <div class="more_actions">
        <a href="<?php echo $track_shipment_url; ?>">Track Shipment</a>
        <?php foreach ($order_change as $status => $blurb): ?>
          <a href="<?php echo admin_url('admin.php?page=merchant_order&order_status='.$status.'&order='.$order_id); ?>">Mark as <?php echo  $blurb; ?></a>
        <?php endforeach; ?>
      </div>
    </div>
  </div>

  <?php
}


//also add package info to order pages in admin area
add_action( 'add_meta_boxes_shop_order', 'hdgs_add_package_info_to_shop_order' );
function hdgs_add_package_info_to_shop_order($post) {
  $product_cats = hdgs_get_product_cats($post->ID);
  if (!$product_cats['hasgoods']) {
    return false;
  }
  add_meta_box( 'hdgs_add_package_info_to_shop_order', 
              __('Fulfillments and Delivery','hdgs_text_domain'), 
              'hdgs_show_package_info_admin', 
              'shop_order'
              );
}
function hdgs_show_package_info_admin ($post) {
  
  $child_order_ids = hdgs_get_child_order_ids($post->ID, get_current_user_id());
  $is_metabox = true;
  hdgs_show_merchant_order_styles_scripts($is_metabox);
  foreach ($child_order_ids as $child_id) {
    $product_cats = hdgs_get_product_cats($child_id);
    if (!$product_cats['hasgoods']) {
      continue;
    }

    $order = wc_get_order($child_id);
    hdgs_show_order_delivery_info($order, $is_metabox);
    hdgs_show_admin_delivery_info($order);
    echo '<hr/>';
  }
}

//extra data only for admin
function hdgs_show_admin_delivery_info($order){
  $order_id = $order->get_id();

  //get declared value
  $declared_value = 0;
  $merchant_names = '';
  $line_items = $order->get_items();
  foreach ( $line_items as $item_id => $item ) {
    $product_id = $item->get_product_id();
    $product = wc_get_product($product_id);
    $qty = $item->get_quantity();
    $price = $product->get_price();
    $declared_value += $price*$qty;
  }

  $collect = $order->get_total();

  $courier_data = hdgs_get_courier_data($order_id);

  $courier_name = $courier_data['courier']; 

  $merchant_ids = hdgs_get_merchants_from_order($order_id);

  foreach ($merchant_ids as $merchant_id) {
    if ($merchant_names) {
      $merchant_names .= ' ,';
    }
    $merchant_names .= '<a href="'.get_edit_user_link($merchant_id).'">'.get_the_author_meta('user_login',$merchant_id).'</a>';
  }

  ?>
    <div class="fulfillments_delivery">
      <h3>Extra Delivery Info</h3>
      
      <div>
        <table class="delivery_info"><tbody>
          <tr>
            <td>Merchant:</td>
            <td><?php echo $merchant_names; ?></td>
          </tr>
          <tr>
            <td>Ship Declared Value:</td>
            <td><?php echo wc_price($declared_value); ?></td>
          </tr>
          <tr>
            <td>Amount to collect:</td>
            <td> <?php echo wc_price($collect); ?></td>
          </tr>
          <tr>
            <td>Courier Name:</td>
            <td><?php echo $courier_name; ?></td>
          </tr>
        </tbody></table>
      </div>
    </div>
  <?php
}
     
function hdgs_show_order_customer_info($order) {
  $order_id = $order->get_id();
  $order_data = $order->get_data(); 
  $user_id = $order->get_user_id();
  $first_name = $order_data['billing']['first_name'];
  $last_name = $order_data['billing']['last_name'];
  $full_name = $first_name.' '.$last_name;
  $order_ct = wc_get_customer_order_count( $user_id );
  $order_blurb = 'order';
  if (1 != $order_ct) {
    $order_blurb .= 's';
  }

  //data is saved in child order so we need the child id
  $child_order_ids = hdgs_get_child_order_ids($order_id, get_current_user_id());
  $child_id = $child_order_ids[0];

  $email = get_post_meta($child_id,'new_email', true);
  if (!$email) {
    $email = $order_data['billing']['email'];    
  }
  
  $phone = get_post_meta($child_id, 'new_phone', true);
  if (!$phone) {
    $phone = $order_data['billing']['phone'];    
  }
  

  //use new address if it exists
  $new_shipping_address = get_post_meta($child_id,'new_shipping_address', true);
  if ($new_shipping_address) {
    $address = $new_shipping_address;
  } else {
    $address = $order->get_formatted_shipping_address(); 
  }
  $textarea_address = str_replace('<br/>', PHP_EOL, $address); //br2nl

  ?>
  <div class="customer_data">

    <h3>Customer</h3>
    <div class="customer_name">
      <div class="avatar">
        <?php echo get_avatar( $email, 20); ?>
      </div>
      <h4><?php echo $full_name; ?></h4>
      <?php echo $order_ct.' '.$order_blurb; ?>
    </div>

    <div class="order_contact">
      <h4>Order Contact Email</h4>
      <input type="text" name="billing_email" value="<?php echo $email; ?>" />
    </div>

    <div class="shipping">
      <h4>Shipping Address</h4>
      <div class="shipping_address">
        <?php echo $address; ?>
        <br/>
        <a href="#edit_address">Change Shipping Address</a>
        <br/>
      </div>
      <a href="#cancel_address">Cancel Change Address</a>
      <br/>
      <textarea class="billing_address" name="billing_address" placeholder="Change Address" rows="8"><?php echo $textarea_address; ?></textarea>
    </div>

    <div class="billing_phone">
      <h4>Phone</h4>
      <input type="text" name="billing_phone" value="<?php echo $phone; ?>" />
    </div>

    <?php hdgs_show_payment_info($order); ?>

  </div>
  <?php
}

function hdgs_show_payment_info($order, $is_metabox = false) {
  $order_id = $order->get_id();
  $order_data = $order->get_data();
  $payment_method = $order_data['payment_method'];
  if (!$payment_method) {
    return false;
  }
  $payment_date = hdgs_get_status_change_timestamp($order_id, 'processing');
  if (!$payment_date) {
    $payment_date = 'None';
  }
  ?>
    <div class="payment_method">
      <h4>Payment Method</h4>
      <?php echo $payment_method; ?>
      <h4>Payment Date</h4>
      <?php echo $payment_date; ?>
    </div>
  <?php
}
//also add payment info to order pages in admin area
add_action( 'add_meta_boxes_shop_order', 'hdgs_add_payment_info_to_shop_order' );
function hdgs_add_payment_info_to_shop_order() {
  add_meta_box( 'hdgs_add_payment_info_to_shop_order', 
              __('Payment Info','hdgs_text_domain'), 
              'hdgs_show_payment_info_admin', 
              'shop_order',
              'side'
              );
}
function hdgs_show_payment_info_admin ($post) {
  $order = wc_get_order($post->ID);
  $is_metabox = true;
  hdgs_show_payment_info($order, $is_metabox);
}
 
//show the voucher info for each item
function hdgs_show_order_voucher($order, $is_metabox = false) { 
  //get basic data
  $days = 24*60*60;
  $order_id = $order->get_id();
  $order_data = $order->get_data();
  //find vouchers based on order_id
  $args = array('post_type'       => 'voucher',
              'posts_per_page'    => -1,
              'post_status'       => 'any',
              'meta_key'          => 'order_id', 
              'meta_value'        => $order_id,
              );
  $vouchers = get_posts($args);

  //get impt dates
  $voucher_redemption_dates = get_post_meta($order_id,'voucher_redemption_dates',true);
  if ($vouchers) {    
    foreach($vouchers as $voucher) {
      $vid = $voucher->ID;
      $pid = get_post_meta($vid, 'deal_id', true);
      $product = wc_get_product($pid);

      $v[$vid]['code'] = get_post_meta($vid, 'voucher_code', true);

      //compute expiration date
      $duration = get_post_meta($pid, 'hdgs_voucher_duration', true); //in days
      if ($order->get_date_paid()) {
        $date_paid = $order->get_date_paid()->getTimestamp();
      }
      if ($date_paid) {
        $v[$vid]['expiration_date'] =  strtotime($date_paid) + $duration*$days;    
      }
      
      $v[$vid]['notes'] = get_post_meta($pid, 'hdgs_voucher_notes', true);
      if (isset($voucher_redemption_dates[$vid])) {
        $v[$vid]['redemption_date'] = $voucher_redemption_dates[$vid];
      }
      $v[$vid]['end_date'] = get_post_meta($pid, 'hdgs_voucher_end_date', true);
      $v[$vid]['start_date'] = $date_paid;
    }

  } else {
    return false;
  }
 
  ?>
    <div class="voucher_container">
      <?php if (!$is_metabox): ?>
          <h3>Voucher Validity</h3>
      <?php endif; ?>
      <?php foreach($v as $id => $voucher): ?>
        <h2><?php echo $product->get_name(); ?></h2>
        <table><tbody>
          <tr>
            <td><?php _e('Voucher ID#:','hdgs_text_domain'); ?></td>
            <td><?php _e($id,'hdgs_text_domain'); ?></td>
          </tr>
          <tr>
            <td><?php _e('Deal Start Date:', 'hdgs_text_domain'); ?></td>
            <td><?php _e(date('F d, Y, g:i A',strtotime($voucher['start_date']))); ?></td>
          </tr>
          <tr>
            <td><?php _e('Deal End Date:', 'hdgs_text_domain'); ?></td>
            <td><?php _e(date('F d, Y, g:i A',strtotime($voucher['end_date']))); ?></td>
          </tr>
          <tr>
            <td><?php _e('Voucher Code:', 'hdgs_text_domain'); ?></td>
            <td><?php _e($voucher['code'], 'hdgs_text_domain'); ?></td>
          </tr>
          <tr>
            <td><?php _e('Expiration Date:', 'hdgs_text_domain'); ?></td>
            <td><?php _e(date('F d, Y g:i A',$voucher['expiration_date']), 'hdgs_text_domain'); ?></td>
          </tr>
          <tr>
            <td><?php _e('Voucher Notes:', 'hdgs_text_domain'); ?></td>
            <td><div class="voucher_notes"><?php _e($voucher['notes'], 'hdgs_text_domain'); ?></div></td>
          </tr>
          <tr>
            <td><label for="<?php _e('voucher_redemption_dates['.$vid.']', 'hdgs_text_domain'); ?>"><?php _e('Redeemed On:','hdgs_text_domain'); ?></label></td>
            <td><input name="<?php _e('voucher_redemption_dates['.$vid.']', 'hdgs_text_domain'); ?>" type="date" id="<?php _e('voucher_redemption_dates['.$vid.']', 'hdgs_text_domain'); ?>" value="<?php _e($voucher['redemption_date'], 'hdgs_text_domain'); ?>" /></td>
          </tr>
        </tbody></table>      
      <?php endforeach; ?>
    </div>
  <?php
}

//also add voucher info to order pages in admin area
add_action( 'add_meta_boxes_shop_order', 'hdgs_add_voucher_metabox_to_shop_order' );
function hdgs_add_voucher_metabox_to_shop_order($post) {
  $product_cats = hdgs_get_product_cats($post->ID);
  if (!$product_cats['hasdeals']) {
    return false;
  }
  add_meta_box( 'hdgs_add_voucher_metabox_to_shop_order', 
              __('Voucher Validity','hdgs_text_domain'), 
              'hdgs_show_order_voucher_admin', 
              'shop_order' 
              );
}
function hdgs_show_order_voucher_admin ($post) {
  $order = wc_get_order($post->ID);
  $is_metabox = true;
  
  $child_order_ids = hdgs_get_child_order_ids($post->ID, get_current_user_id());
  foreach ($child_order_ids as $child_id) {
    $product_cats = hdgs_get_product_cats($child_id);
    if (!$product_cats['hasdeals']) {
      continue;
    }
    
    //show merchant names for this voucher
    $merchant_ids = hdgs_get_merchants_from_order($child_id);
    foreach ($merchant_ids as $merchant_id) {
      if ($merchant_names) {
        $merchant_names .= ' ,';
      }
      $merchant_names .= '<a href="'.get_edit_user_link($merchant_id).'">'.get_the_author_meta('user_login',$merchant_id).'</a>';
    }
    echo $merchant_names.'</br>';

    $order = wc_get_order($child_id);
    hdgs_show_order_voucher($order, $is_metabox);

    echo '<hr/>';
  }
}


//add fees info to order pages in admin area
add_action( 'add_meta_boxes_shop_order', 'hdgs_add_admin_fees_to_shop_order' );
function hdgs_add_admin_fees_to_shop_order() {
  add_meta_box( 'hdgs_add_admin_fees_to_shop_order', 
              __('Admin Fees','hdgs_text_domain'), 
              'hdgs_show_admin_fees', 
              'shop_order',
              'side'
              );
}
function hdgs_show_admin_fees ($post) {

  $fees = array();
  $merchant_names = '';
  
  $child_order_ids = hdgs_get_child_order_ids($post->ID, get_current_user_id());
  foreach ($child_order_ids as $child_id) {
    $order = wc_get_order($child_id);
    $order_id = $order->get_id();

    $gross_sales = $order->get_subtotal() - $order->get_total_tax();
    $net_sales = $gross_sales - $order->get_total_discount();

    //get the charge info
    $charge_fees = get_option('hdgs_get_basic_charge_fees');
    $charge_fields = hdgs_get_basic_charge_fields();

    //get the users in this order and assign fees to each of them

    $merchant_ids = hdgs_get_merchants_from_order($child_id);

    foreach ($merchant_ids as $user_id) {

      if (!$user_id) continue;

      $charge_status = get_user_meta( $user_id, 'hdgs_charge_status', true );
      //remove blanks
      $charge_status = array_filter($charge_status);

      //we'll get the hd commission using another method
      unset($charge_status['hd_commission']);
      if (!isset($fees[$user_id]['hd_commission'])) {
        $fees[$user_id]['hd_commission'] = 0;
      }
      $fees[$user_id]['hd_commission'] += hdgs_get_hd_commission($order_id);

      foreach ($charge_status as $name => $val) {
        if (!isset($charge_fees[$name])) {
          $charge_fees[$name] = 0;
        }
        $amount = $net_sales*$charge_fees[$name]/100;
        if ($amount) {
          if(!isset($fees[$user_id][$name])) {
            $fees[$user_id][$name] = 0;
          }
          $fees[$user_id][$name] += $amount;  
        }
      }

      if ($merchant_names) {
        $merchant_names .= ' ,';
      }
      $merchant_names .= '<a href="'.get_edit_user_link($user_id).'">'.get_the_author_meta('user_login',$user_id).'</a>';

    }

    $fees = array_filter($fees);


    //show the fees
    foreach ($fees as $user_id => $fee): 
      $user = get_user_by('id',$user_id);
    ?>

      <table class="hdgs_order_fees"><tbody>
        <tr>
          <td>Merchant:</td>
          <td><?php echo $merchant_names; ?></td>
        </tr>
        <?php foreach ($fee as $name => $value): ?>
          <tr>
            <td><?php _e($charge_fields[$name],'hdgs_text_domain'); ?></td>
            <td><?php _e(wc_price($value),'hdgs_text_domain'); ?></td>
          </tr>
        <?php endforeach; ?>
      </tbody></table>
      <hr/>
    
    <?php 

    endforeach; 

  }
  
}

//also add financials in order
add_action( 'add_meta_boxes_shop_order', 'hdgs_add_financials_to_shop_order' );
function hdgs_add_financials_to_shop_order($post) {
  add_meta_box( 'hdgs_add_financials_to_shop_order', 
              __('Financials','hdgs_text_domain'), 
              'hdgs_show_financials_shop_order', 
              'shop_order' 
              );
}
function hdgs_show_financials_shop_order ($post, $is_metabox = true) {
  global $allowed_payout_statuses;
  $merchant_names = '';

  $child_order_ids = hdgs_get_child_order_ids($post->ID, get_current_user_id());
  foreach ($child_order_ids as $child_id) {

    $order = wc_get_order($child_id);
    $order_id = $order->get_id();

    $payout_statuses = $allowed_payout_statuses;
    $payout_status = get_post_meta($order_id, 'hdgs_payout_status', true);
    $payout_date = get_post_meta($order_id, 'hdgs_payout_date', true);
    $or_number = get_post_meta($order_id, 'hdgs_or_number', true);
    $or_date = get_post_meta($order_id, 'hdgs_or_date', true);

    $vat_ex = $order->get_subtotal();
    $vat_rate = get_option('hdgs_vat_rate');
    $vat_inc = $vat_ex + ($vat_ex*$vat_rate);

    $cr_number = get_post_meta($order_id, 'hdgs_cr_number', true);
    $cr_date = get_post_meta($order_id, 'hdgs_cr_date', true);

    $financial_remarks = get_post_meta($order_id, 'hdgs_fin_remarks', true);

    $date_completed = $order->get_date_completed();
    if($date_completed) {
      $date_completed = $date_completed->getTimestamp();
      $completed_blurb = date('F d, Y, g:i A', $date_completed);
    } else {
      $completed_blurb = 'Not yet completed';
    }

    //show merchant names for this voucher
    $merchant_ids = hdgs_get_merchants_from_order($child_id);
    foreach ($merchant_ids as $merchant_id) {
      if ($merchant_names) {
        $merchant_names .= ' ,';
      }
      $merchant_names .= '<a href="'.get_edit_user_link($merchant_id).'">'.get_the_author_meta('user_login',$merchant_id).'</a>';
    }

    $edit_order_url = admin_url('admin.php?page=merchant_order_admin&order='.$child_id);

    ?>

    <table class="hdgs_financials_shop_order">
      <tbody>
      <?php if (!$is_metabox): ?>
        <tr>
          <td><h2>Financials</h2></td>
        </tr>
      <?php endif; ?>
      <tr>
        <td>Merchant:</td>
        <td><?php echo $merchant_names; ?></td>
      </tr>
      <tr>
        <td>Payout Status:</td>
        <td>
          <?php 
            if ($is_metabox): 
              echo $payout_status;
            else: 
          ?>
            <select name="payout_status">
              <?php foreach ($payout_statuses as $slug => $status): ?>
                <option value="<?php echo $slug; ?>" <?php selected($payout_status, $status); ?> >
                  <?php echo $status; ?>
                </option>
              <?php endforeach; ?>
            </select>
          <?php endif; ?>
        </td>
      </tr>
      <tr>
        <td>Payout Schedule:</td>
        <td><?php echo hdgs_show_recur_blurb('payout'); ?></td>
      </tr>
      <tr>
        <td>Payout Date:</td>
        <td>
          <?php 
            if ($is_metabox): 
              echo $payout_date;
            else: 
          ?>
            <input type="date" name="payout_date" value="<?php echo $payout_date; ?>" />
          <?php endif; ?>
        </td>
      </tr>
      <tr>
        <td>Official Receipt Number:</td>
        <td>
          <?php 
            if ($is_metabox): 
              echo $or_number;
            else: 
          ?>
            <input type="text" name="or_number" value="<?php echo $or_number; ?>" />
          <?php endif; ?>
        </td>
      </tr>
      <tr>
        <td>Official Receipt Date:</td>
        <td>
          <?php 
            if ($is_metabox): 
              echo $or_date;
            else: 
          ?>
            <input type="date" name="or_date" value="<?php echo $or_date; ?>" />
          <?php endif; ?>
        </td>
      </tr>
      <tr>
        <td>Total Value (Vat Excl.):</td>
        <td><?php echo wc_price($vat_ex); ?></td>
      </tr>
      <tr>
        <td>VAT:</td>
        <td><?php echo wc_price($vat_ex*$vat_rate); ?></td>
      </tr>
      <tr>
        <td>Total Value (Vat Incl.):</td>
        <td><?php echo wc_price($vat_inc); ?></td>
      </tr>
      <tr>
        <td>Collection Receipt Number:</td>
        <td>
          <?php 
            if ($is_metabox): 
              echo $cr_number;
            else: 
          ?>
            <input type="text" name="cr_number" value="<?php echo $cr_number; ?>" />
          <?php endif; ?>
        </td>
      </tr>
      <tr>
        <td>Collection Receipt Date:</td>
        <td>
          <?php 
            if ($is_metabox): 
              echo $cr_date;
            else: 
          ?>
            <input type="date" name="cr_date" value="<?php echo $cr_date; ?>" />
          <?php endif; ?>
        </td>
      </tr>
      <tr>
        <td>Financial Remarks</td>
        <td>
          <?php 
            if ($is_metabox): 
              echo $financial_remarks;
            else: 
          ?>
            <textarea name="financial_remarks"><?php echo $financial_remarks; ?></textarea>
          <?php endif; ?>
        </td>
      </tr>
      <tr>
        <td>Order Completion Date</td>
        <td><?php echo $completed_blurb; ?></td>
      </tr>
      </tbody>
    </table>
    <?php if ($is_metabox): ?>
      <a href="<?php echo $edit_order_url; ?>" target="_blank" style="display:block;text-align:right;">Edit Order Details</a>
    <?php endif; ?>
    <hr/>
    <?php
  }
}


function hdgs_show_order_timeline($order, $is_metabox = false) {
  $order_id = $order->get_id();
  $current_user = wp_get_current_user();

  $edit_order_url = admin_url('admin.php?page=merchant_order_admin&order='.$order_id);
  
  $timeline_items = hdgs_get_order_notes( $order_id );
  $timeline_meta = get_post_meta($order_id,'hdgs_timeline_meta',true);

  ?>
    <div class="order_timeline" id="order_timeline">
      <?php if ($is_metabox): ?>
         <a href="<?php echo $edit_order_url; ?>#order_timeline" target="_blank">Post on Timeline</a>
      <?php else: ?>
        <h3>Timeline</h3>
        <div class="top_box">
          <?php echo get_avatar($current_user->user_email, 50); ?>
          <input name="timeline_status" type="text" id="timeline_status" />
          <input name="hdgs_send_status" value="Post New Status" type="submit" />
        </div>
      <?php endif; ?>
      <ul>
        <?php 

        foreach ($timeline_items as $item): 
          $posted_on = hdgs_date_to_words(strtotime($item->comment_date));
          //get extra meta from order like original poster / action taker
          if (isset($timeline_meta[$item->comment_ID])) {
            $user_id = $timeline_meta[$item->comment_ID]['comment_author'];
            $user = get_user_by('id',$user_id);
            $author = $user->display_name;
          } else {
            $author = 'Happy Deals';
          }
          ?>
          <li>
            <div><?php _e(nl2br($item->comment_content), 'hdgs_text_domain');?></div>
            <span><?php _e($posted_on.' by '.$author, 'hdgs_text_domain'); ?></span>
          </li>
        <?php endforeach ?>
      </ul>
    </div>
  <?php
}
//adding the timeline to the admin order area as well
/**deactivate for now, too confusing. doesn't sync feeds well
add_action( 'add_meta_boxes_shop_order', 'hdgs_add_timeline_to_shop_order' );
function hdgs_add_timeline_to_shop_order($post) {
  add_meta_box( 'hdgs_add_timeline_to_shop_order', 
              __('Timeline','hdgs_text_domain'), 
              'hdgs_show_order_timeline_admin', 
              'shop_order' 
              );
}
function hdgs_show_order_timeline_admin($post) {
  $order = wc_get_order($post->ID);
  $is_metabox = true;
  hdgs_show_order_timeline($order, $is_metabox);
}
**/

 
//submit button
function hdgs_show_save_button() {
  ?>
    <div class="final_save">
      <input name="hdgs_save_order" value="Save" type="submit" class="" />
    </div>
  </form>
  <?php
} 

//process the edit of the order. only saves to vendor orders
//Fixed an issue where save_post update interferes with Woocommerce ordering
//https://icanwp.com/developer-blog/syntaxerror-json-parse-unexpected-character-line-1-column-1-json-data/
add_action('admin_post_hdgs_update_order', 'hdgs_update_order' );
add_action('save_post', 'hdgs_update_order', 20);
function hdgs_update_order($post_id){
  $timeline_status = '';
  $send_status = '';
  $saved = array();

  if (!current_user_can( 'vendor' ) AND !current_user_can( 'administrator' )) {
    return false;
  }

  //always needs an order id
  $order_id = $post_id;
  if (!$order_id AND isset($_POST['oid'])) {
    $order_id = $_POST['oid'];
  }

  if (!$order_id) {
    return false;
  }

  //if vendor, can only modify own orders
  if (current_user_can( 'vendor' )) {
    $merchant_ids = hdgs_get_merchants_from_order($order_id);
    $user_id = get_current_user_id(); 
    if (!in_array($user_id, $merchant_ids) ) {
      return false;
    }
  }
   
  $order = wc_get_order( $order_id );
  $order_data = $order->get_data();

  if(isset($_POST['page'])) {
    $page = $_POST['page'];
  } else {
    $page = '';
  }

  //get parent
  $parent_id = wp_get_post_parent_id($order_id);

  if ($page) {
    if (is_super_admin()) {
      $redirect = admin_url('admin.php?page='.$page.'&order='.$order_id);
    } else {
      $redirect = admin_url('admin.php?page='.$page.'&order='.$parent_id);
    }
  } elseif (is_super_admin()){
    $redirect = get_edit_post_link($parent_id,'');
  } else {
    $redirect = hdgs_get_current_url();
  }

  //if sending just the status, skip everything else
  if (isset($_POST['timeline_status'])) {
    $timeline_status = sanitize_text_field($_POST['timeline_status']);
  }
  if (isset($_POST['hdgs_send_status'])) {
    $send_status = sanitize_text_field($_POST['hdgs_send_status']);
  }
  if ($order AND $timeline_status AND $send_status) {
    $saved['timeline'] = hdgs_log_user_actions($order, $timeline_status);
    if ($saved['timeline']) {
      $_SESSION['hdgs_admin_notices'][] = array('type' => 'success', 
                                        'message' => 'New Status Posted', 
                                        );  
    }

    wp_redirect($redirect);
    exit;
  }  

  //add a new billing address if there was a change
  if (isset($_POST['billing_address'])) {
    $billing_address = sanitize_textarea_field($_POST['billing_address']);
    $address = $order->get_formatted_shipping_address(); 
    $old_address = str_replace('<br/>', PHP_EOL, $address);
    if ($billing_address != $old_address) {
      if(!$billing_address) {
        $billing_address = $old_address;
      }
      $saved['address'] = update_post_meta($order_id,'new_shipping_address', $billing_address);
      if ($saved['address']) {
        $note = 'Changed shipping address to: '.PHP_EOL.$billing_address;
        hdgs_log_user_actions($order, $note);
      }
    }
  }
  

  //change email
  if (isset($_POST['billing_email'])) {
    $billing_email = sanitize_email($_POST['billing_email']);
    $old_email = $order_data['billing']['email'];
    if ($billing_email != $old_email) {
      if (!$billing_email) {
        $billing_email = $old_email;
      }
      $saved['email'] = update_post_meta($order_id,'new_email',$billing_email);
      if ($saved['email']) {
        $note = 'Changed email to: '.$billing_email;
        hdgs_log_user_actions($order, $note);  
      }
    }
  }
  

  //change phone
  if(isset($_POST['billing_phone'])) {
    $billing_phone = sanitize_text_field($_POST['billing_phone']);
    $old_phone = $order_data['billing']['phone'];
    if ($billing_phone != $old_phone) {
      if (!$billing_phone) {
        $billing_phone = $old_phone;
      }
      $saved['phone'] = update_post_meta($order_id,'new_phone', $billing_phone);
      if ($saved['phone']) {
        $note = 'Changed phone to: '.$billing_phone;
        hdgs_log_user_actions($order, $note);
      }
    }
  }
  

  //change package count
  if (isset($_POST['package_count'])) {
    $package_count = sanitize_text_field($_POST['package_count']);
    $old_count = get_post_meta($order_id,'package_count',true);
    if ($package_count != $old_count) {
      if (!$package_count) {
        $package_count = 1;
      }
      $saved['package_count'] = update_post_meta($order_id,'package_count',$package_count);
      if ($saved['package_count']) {
        $note = 'Changed package count to: '.$package_count;
        hdgs_log_user_actions($order, $note);
      }
    }
  }
 

  //save packaging choice
  if (isset($_POST['package_size'])) {
    $package_size = sanitize_text_field($_POST['package_size']);
    $old_size = get_post_meta($order_id,'package_size',true);
    if ($package_size != $old_size) {
      $saved['package_size'] = update_post_meta($order_id,'package_size',$package_size);
      if ($saved['package_size']) {
        $note = 'Changed packaging size to: '.ucwords($package_size);
        hdgs_log_user_actions($order, $note);
      }
      if ('own' == $package_size) {
        $dimensions['length'] = intval($_POST['length']);
        $dimensions['width'] = intval($_POST['width']);
        $dimensions['height'] = intval($_POST['height']);
        $old_dimensions = get_post_meta($order_id,'dimensions', true);
        if ($dimensions != $old_dimensions) {
          if (!$dimensions OR !array_filter($dimensions)) {
            $dimensions = $old_dimensions;
          }
          $saved['dimensions'] = update_post_meta($order_id,'dimensions', $dimensions);
          //only add note if overwriting old dimensions
          if ($saved['dimensions']) {
            $note = 'Changed dimensions to: '.PHP_EOL;
            $note .= $dimensions['length'].'cm X';
            $note .= $dimensions['width'].'cm X';
            $note .= $dimensions['height'].'cm';
            hdgs_log_user_actions($order, $note);
          }
        }
      }   
    }
  }
  

  //change weight
  if(isset($_POST['total_weight'])) {
    $total_weight = intval($_POST['total_weight']);
    if($total_weight <= 0) {
      $total_weight = min(hdgs_get_total_weight($order_id),1);
    }
    $old_weight = get_post_meta($order_id,'total_weight',true);
    if ($total_weight != $old_weight) {
      $saved['total_weight'] = update_post_meta($order_id,'total_weight',$total_weight);
      //only add note if overwriting old count
      if ($saved['total_weight']) {
        $note = 'Changed package weight to: '.$total_weight;
        hdgs_log_user_actions($order, $note);
      }
    }
  }
  

  //change redemption dates
  if (isset($_POST['voucher_redemption_dates'])) {
    $voucher_redemption_dates = sanitize_text_field($_POST['voucher_redemption_dates']);
    $old_dates = get_post_meta($order_id,'voucher_redemption_dates',true);
    if ($voucher_redemption_dates != $old_dates) {
      $saved['redemption'] = update_post_meta($order_id,'voucher_redemption_dates',$voucher_redemption_dates);
      //only add note if overwriting old count
      if ($saved['redemption']){
        foreach ($voucher_redemption_dates as $vid => $date) {
          if($date != $old_dates[$vid]) {
            $note = 'Changed redemption date of Voucher ID#'.$vid.' to: '.PHP_EOL.$date;
            hdgs_log_user_actions($order, $note);
          }
        }
      }
    }
  }

  //save financial stuff
  if(isset($_POST['payout_status'])) {
    $payout_status = sanitize_text_field($_POST['payout_status']);
    $old_status = get_post_meta($order_id, 'hdgs_payout_status', true);
    if ($payout_status != $old_status) {
      $saved['payout_status'] = update_post_meta($order_id,'hdgs_payout_status',$payout_status);
      //only add note if overwriting old count
      if ($saved['payout_status']) {
        $note = 'Changed payout status to: '.$payout_status;
        hdgs_log_user_actions($order, $note);
      }
    }
  }
  
  if (isset($_POST['payout_date'])) {
    $payout_date = sanitize_text_field($_POST['payout_date']);
    $old_date = get_post_meta($order_id, 'hdgs_payout_date', true);
    if ($payout_date != $old_date) {
      $saved['payout_date'] = update_post_meta($order_id,'hdgs_payout_date',$payout_date);
      //only add note if overwriting old count
      if ($saved['payout_date']) {
        $note = 'Changed payout date to: '.esc_attr(date('M d, Y',strtotime($payout_date)));
        hdgs_log_user_actions($order, $note);
      }
    }
  }
  
  if (isset($_POST['or_number'])) {
    $or_number = sanitize_text_field($_POST['or_number']);
    $old_or_number = get_post_meta($order_id, 'hdgs_or_number', true);
    if ($or_number != $old_or_number) {
      $saved['or_number'] = update_post_meta($order_id,'hdgs_or_number',$or_number);
      //only add note if overwriting old count
      if ($saved['or_number']) {
        $note = 'Changed OR number to: '.$or_number;
        hdgs_log_user_actions($order, $note);
      }
    }
  }
  
  if (isset($_POST['or_date'])) {
    $or_date = sanitize_text_field($_POST['or_date']);
    $old_or_date = get_post_meta($order_id, 'hdgs_or_date', true);
    if ($or_date != $old_or_date) {
      $saved['or_date'] = update_post_meta($order_id,'hdgs_or_date',$or_date);
      //only add note if overwriting old count
      if ($saved['or_date']) {
        $note = 'Changed OR date to: '.esc_attr(date('M d, Y',strtotime($or_date)));
        hdgs_log_user_actions($order, $note);
      }
    }
  }
  
  if (isset($_POST['cr_number'])) {
    $cr_number = sanitize_text_field($_POST['cr_number']);
    $old_cr_number = get_post_meta($order_id, 'hdgs_cr_number', true);
    if ($cr_number != $old_cr_number) {
      $saved['cr_number'] = update_post_meta($order_id,'hdgs_cr_number',$cr_number);
      //only add note if overwriting old count
      if ($saved['cr_number']) {
        $note = 'Changed commission receipt number to: '.$cr_number;
        hdgs_log_user_actions($order, $note);
      }
    }
  }

  if (isset($_POST['cr_date'])) {
    $cr_date = sanitize_text_field($_POST['cr_date']);
    $old_cr_date = get_post_meta($order_id, 'hdgs_cr_date', true);
    if ($cr_date != $old_cr_date) {
      $saved['cr_date'] = update_post_meta($order_id,'hdgs_cr_date',$cr_date);
      //only add note if overwriting old count
      if ($saved['cr_date']) {
        $note = 'Changed commission receipt date to: '.esc_attr(date('M d, Y',strtotime($cr_date)));
        hdgs_log_user_actions($order, $note);
      }
    }
  }
  
  //save financial remarks
  if (isset($_POST['financial_remarks']) ) {
    $financial_remarks = sanitize_textarea_field($_POST['financial_remarks']);
    $old_remarks = get_post_meta($order_id, 'hdgs_fin_remarks', true);
    if ($financial_remarks != $old_remarks) {
      $saved['financial_remarks'] = update_post_meta($order_id,'hdgs_fin_remarks',$financial_remarks);
      //only add note if overwriting old count
      if ($saved['financial_remarks']) {
        $note = 'Changed financial remarks to: '.$financial_remarks;
        hdgs_log_user_actions($order, $note);
      }
    }
  }

  //extra input for financials

  //add extra charges
  if(isset($_POST['charge_name']) AND isset($_POST['charge_amount']) AND $_POST['charge_amount']) {
    $charge_name = sanitize_text_field($_POST['charge_name']);
    $charge_amount = floatval($_POST['charge_amount']);
    $new_charges = get_post_meta($order_id,'hdgs_new_charges', true);
    if (isset($new_charges[$charge_name])) {
      $pre_note = 'Changed value of';
    } else {
      $pre_note = 'Added new';
    }
    $new_charges[$charge_name] = $charge_amount;
    $saved['new_charges'] = update_post_meta($order_id,'hdgs_new_charges',$new_charges);
    //only add note if overwriting old count
    $charge_blurb = 'P'.abs($charge_amount);
    if ($charge_amount < 0) {
      $charge_blurb = '-'.$charge_blurb;
    }
    if ($saved['new_charges']) {
      $note = $pre_note.' Order Charge: '.$charge_name.' = '.$charge_blurb;
      hdgs_log_user_actions($order, $note);
    }
  }

  //show success
  if (array_filter($saved)) {
    $_SESSION['hdgs_admin_notices'][] = array('type' => 'success', 
                                      'message' => 'Order Updated', 
                                  );  
  }
  
  if ($order) {
    wp_redirect($redirect);
    exit;  
  }
  
}

//remove order notes metabox
/*
add_action( 'do_meta_boxes', 'hdgs_remove_order_notes' );
function hdgs_remove_order_notes($post) {
  if (current_user_can( 'administrator' )) {
    remove_meta_box( 'woocommerce-order-notes', 'shop_order', 'side' );  
  } 
}
*/

 