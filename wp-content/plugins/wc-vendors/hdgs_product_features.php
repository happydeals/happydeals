<?php

/**************************
/*** PRODUCT FEATURES *****
***************************/

//add links to deals and goods in submenu at left
add_action('admin_menu', 'hdgs_add_product_cat_to_products_menu');
function hdgs_add_product_cat_to_products_menu() {

	add_submenu_page('edit.php?post_type=product',
						      'Products > Deals',
						      ' - Deals',
						      'vendor',
						      'edit.php?post_type=product&product_cat=deals'
						  		);
	add_submenu_page('edit.php?post_type=product',
						      'Products > Goods',
						      ' - Goods',
						      'vendor',
						      'edit.php?post_type=product&product_cat=goods'
						  		);
}

//instead of publishing, updated posts are automatically pending instead
add_action('save_post', 'hdgs_submit_for_review_on_update', 25 );
function hdgs_submit_for_review_on_update($post_id) {
	$current_user = wp_get_current_user();
	$post = get_post($post_id);
	if (!in_array('administrator', $current_user->roles) 
			AND 'product' == $post->post_type 
			AND 'publish' == $post->post_status ) {
		$my_post = array();
		$my_post['ID'] = $post_id;
		$my_post['post_status'] = 'pending';

		remove_action('save_post', 'submit_for_review_update', 25);
		wp_update_post($my_post);
		add_action('save_post', 'submit_for_review_update', 25);

		return true;
	}

	return false;
}

//change texts for product update page
add_filter( 'gettext', 'hdgs_change_texts', 10, 2 );
function hdgs_change_texts( $translation ) {
	$text = array();
	$post = get_post();
	if (!$post) {
		return $translation;
	}
	$post_type = $post->post_type; 
	$post_author = $post->post_author;

	//translate these if in product edit page
	if ('product' == $post_type 
		AND get_current_user_id() == $post_author ) {
		$text['Update'] = 'Submit for Review';
		$text['Copy to a new draft'] = 'Duplicate Product';
	}

	if ($text) {
		$translation = str_ireplace(  array_keys($text),  $text,  $translation );
	}

	return $translation;
}

//allow export of products just for the current vendor's products
add_action( 'admin_init', 'hdgs_do_vendor_product_export');
function hdgs_do_vendor_product_export() {
	$post_type = @$_GET['post_type'];
	$page = @$_GET['page'];
	if ( 'product' == $post_type AND 'product_exporter' == $page) {
		include_once( 'class-hdgs-product-csv-exporter.php' );
		$exporter = new hdgs_Product_CSV_Exporter();
		$exporter->hdgs_export();
	}
}

//hack to redirect importer page to 'done' step because the JS fails to refresh the page for some reason
add_action( 'admin_init', 'hdgs_do_vendor_product_import');
function hdgs_do_vendor_product_import() {
	$post_type = @$_GET['post_type'];
	$page = @$_GET['page'];
	$step = @$_GET['step'];
	if ($post_type == 'product' AND $page == 'product_importer' AND $step == 'import') {
		$redirect_url = admin_url('edit.php?post_type=product&page=product_importer&step=done');
		?>
			<meta http-equiv="refresh" content="10; url=<?php echo $redirect_url; ?>" />
		<?php

		do_action('hdgs_do_vendor_product_import');
	}
}

//adds a preview of Google search results as a metabox
add_action( 'add_meta_boxes_product', 'hdgs_add_google_snippet_metabox' );
function hdgs_add_google_snippet_metabox( $post ) {
  add_meta_box(
      'hdgs_google_snippet',
      __( 'Google Results Preview' ),
      'hdgs_show_google_snippet',
      'product',
      'normal',
      'low'
  );
}

function hdgs_show_google_snippet() {
	$post = get_post();
	$link = get_permalink( $post->ID );
	$title = $post->post_title;
	$description = $post->post_content;
	if (strlen($title) > 70) {
		$title = substr($title,0,70).'...';
	} elseif (!$title) {
		$title = 'My Sample Product';
	}
	if (strlen($description) > 173) {
		$description = substr($description,0,173).'...';
	} elseif (!$description) {
		$description = 'Create a new you with our excellent treatment. Always do your best. What you plant now, you will harvest later. Setting goals is the first step in turning the invisible into the visible. There is always room at the top. If you’re offered a seat on a rocket ship, don’t ask what seat! Just get on.';
	}
	if (strlen($link) > 80) {
		$link = substr($link,0,80).'...';
	}
?>
	<style>
	div.hdgs_snippet {
		width: 510px;
		padding-top: 6px;
		padding: 0;
		padding-top: 0px;
		outline: 0;
		color: #222;
		margin-top: 1em;
	}

	div.hdgs_snippet * {
			font-family: arial,sans-serif;
			margin: 0;
	}
	div.hdgs_snippet h3.snippet_title {
			color: #12c;
			cursor: pointer;
			font-size: medium;
			text-decoration: none;
			font-weight: normal;
	}
	div.hdgs_snippet a.snippet_link {
			color: #093;
			font-size: small;
			text-decoration: none;
	}
	div.hdgs_snippet p.snippet_desc {
			padding: 2px 0;
			color: #222;
			font-size: small;
	}
	</style>
	<div class="hdgs_snippet">
			<h3 class="snippet_title"><?php echo $title; ?></h3>
			<a class="snippet_link" href="javascript://"><?php echo $link; ?></a>
			<p class="snippet_desc"><?php echo $description; ?></p>
	</div>
<?php
}

//adds a voucher text but only readonly
add_action( 'add_meta_boxes_product', 'hdgs_add_voucher_metabox' );
function hdgs_add_voucher_metabox( $post ) {
		if(is_super_admin()) return;
    add_meta_box(
        'hdgs_voucher_meta',
        __( 'Voucher Validity' ),
        'hdgs_show_voucher',
        'product',
        'side',
        'low'
    );

}

function hdgs_show_voucher($post) {
	$post = get_post();

	//get notes and duration
	$duration = get_post_meta($post->ID, 'hdgs_voucher_duration', true); //in days
	if (!$duration) return false;

	$voucher_notes = get_post_meta($post->ID, 'hdgs_voucher_notes', true);
?>
	<div>
		<p><?php echo $voucher_notes; ?></p>
		<p>Duration: <?php echo $duration; ?> Days from Payment</p>
	</div>
<?php
}

//remove some product columns for vendors
add_action( 'manage_edit-product_columns','hdgs_manage_vendor_product_list', 10, 1 );
function hdgs_manage_vendor_product_list( $columns, $post_id) {
	$post_type = $_GET['post_type'];
	if (current_user_can('vendor') AND 'product' == $post_type) {
		 unset($columns['sku']);
		 unset($columns['product_tag']);
		 unset($columns['featured']);
		 unset($columns['product_type']);
		 unset($columns['taxonomy-deal_company']);
 }
  return $columns;
}