<?php 


/******** EDIT USER LIST *************/


//modify the columns
add_filter('manage_users_columns', 'hdgs_manage_merchant_list', 10, 1);
function hdgs_manage_merchant_list( $columns ) {
	if ($_GET['role'] != 'vendor') return $columns; //only for merchant view of user list
	
	$new_columns['cb'] = $columns['cb'];	
	$new_columns['id'] = 'ID';
	$new_columns['date_registered'] = 'Date Registered';
	$new_columns['account_status'] = 'Status';
	$new_columns['username'] = $columns['username'];	
	$new_columns['contact_person'] = 'Contact Person';
	$new_columns['active_package'] = 'Active Package';
	$new_columns['effectivity_date'] = 'Effectivity Date';
	$new_columns['total_value'] = 'Total Value';
	$new_columns['completion'] = '% Completion';

	return $new_columns;
}

add_action( 'admin_menu', 'hdgs_add_link_to_merchant_list' );
function hdgs_add_link_to_merchant_list() {
	if (!is_super_admin()) return; //only for admins
	add_menu_page('Merchants', 
								'Merchants', 
								'administrator', 
								'users.php?role=vendor', 
								'',
								'dashicons-store' );
}

//add our own columns
add_action('manage_users_custom_column', 'hdgs_add_merchant_column_values', 15, 3);
function hdgs_add_merchant_column_values($value, $column_name, $id) {
	if (isset($_GET['role']) AND $_GET['role'] != 'vendor') {
		return $value; //only for merchant view
	}
	$hdgs_account_info = get_user_meta( $id, 'hdgs_account_info', true );

	switch ($column_name):
		
		case 'id':
			return $id;
		break;

		case 'date_registered':
			return $hdgs_account_info['date_registered'];	
		break;

		case 'account_status':
			return get_user_meta($id, 'hdgs_account_status', true);
		break;

		case 'contact_person':
			return $hdgs_account_info['contact_person'];
		break;

		case 'active_package':
			return get_user_meta($id, 'hdgs_active_package', true);
		break;

		case 'effectivity_date':
			$hdgs_effectivity_date = get_user_meta($id, 'hdgs_effectivity_date', true);
			if ($hdgs_effectivity_date) {
				return date('F j, Y',strtotime($hdgs_effectivity_date));
			}
		break;

		case 'total_value':
			return 'P'.number_format(get_user_meta($id, 'hdgs_led_options_hdgs_led_total_value', true),2,'.',',');

		break;

		case 'completion':
			$sales_to_date = hdgs_get_sales_to_date($id);

			$total_value = get_user_meta($id, 'hdgs_led_options_hdgs_led_total_value', true);

			$balance = $total_value - $sales_to_date;

			$completion = $sales_to_date/$total_value;

			return number_format($completion*100,0).'%';
		break;

	endswitch;

	return false;
}

//get sortable columns
add_filter( 'manage_users_sortable_columns', 'hdgs_manage_sortable_merchant_columns' );
function hdgs_manage_sortable_merchant_columns( $columns ) {
	if (isset($_GET['role']) AND $_GET['role'] != 'vendor') {
		return $columns;
	} //only for merchant view
	
	$new_columns['id'] = 'ID';
	$new_columns['username'] = $columns['username'];	

	return $new_columns;
}
