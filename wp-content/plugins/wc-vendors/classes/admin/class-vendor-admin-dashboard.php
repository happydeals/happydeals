<?php 
/**
 *  WC Vendor Admin Dashboard - Vendor WP-Admin Dashboard Pages
 * 
 * @author Jamie Madden <http://wcvendors.com / https://github.com/digitalchild>
 * @package WCVendors
 */

//This file contains order lists, fulfillment lists, and financial lists

Class WCV_Vendor_Admin_Dashboard { 

	public $dashboard_error_msg; 

	function __construct(){ 

		//show the right menu for the right user

		if ( current_user_can('administrator')) {
			add_action( 'admin_menu', array( $this, 'admin_dashboard_pages') ); 	
		}


		if ( current_user_can('vendor')) {
			add_action( 'admin_menu', array( $this, 'vendor_dashboard_pages') ); 	
		}

		if ( current_user_can('branch_account') ) {
			add_action( 'admin_menu', array( $this, 'branch_dashboard_pages') ); 
		}

		// Hook into init for form processing 
		add_action( 'admin_init', array( $this, 'save_shop_settings' ) );
		add_action( 'admin_head', array( $this, 'admin_enqueue_order_style') ); 

	}

	function admin_dashboard_pages() {
		$this->show_financials_menu('administrator');
	}

	function vendor_dashboard_pages(){
		//no shops!!
    //add_menu_page( __('Shop Settings', 'wcvendors'), __('Shop Settings', 'wcvendors'), 'manage_product', 'wcv-vendor-shopsettings', array( $this, 'settings_page' ) );

    $order_page = 'wcv-vendor-orders';
		$hook = add_menu_page( __( 'Orders', 'wcvendors' ), __( 'Orders', 'wcvendors' ), 'manage_product', $order_page, array( $this, 'orders_page' ), 'dashicons-tickets-alt' );
		add_action( "load-".$hook, array( $this, 'add_options' ) );
		add_submenu_page($order_page,
							      'Orders > Deals',
							      ' - Deals',
							      'vendor',
							      'admin.php?page='.$order_page.'&product_cat=deals'
									  );
		add_submenu_page($order_page,
							      'Orders > Goods',
							      ' - Goods',
							      'vendor',
							      'admin.php?page='.$order_page.'&product_cat=goods'
							  		);

		//also show fulfillment pages
		$this->show_fulfillment_menu('vendor');
		$this->show_financials_menu('vendor');
	}

	function branch_dashboard_pages() {
		$this->show_fulfillment_menu('branch_account');
	}

	function show_fulfillment_menu($role){
		
		//fulfillment pages
		$fulfillment_page = 'hdgs_fulfillment';
		$hook = add_menu_page( __( 'Fulfillment', 'wcvendors' ), __( 'Fulfillment', 'wcvendors' ), $role, $fulfillment_page, array( $this, 'orders_page' ), 'dashicons-location-alt' );
		add_action( "load-".$hook, array( $this, 'add_options' ) );
		add_submenu_page($fulfillment_page,
							      'Fulfillment - Deal Redemption',
							      ' - Deal Redemption',
							      $role,
							      'admin.php?page='.$fulfillment_page.'&product_cat=deals'
										);
		add_submenu_page($fulfillment_page,
							      'Fulfillment - Customer Pick-up',
							      ' - Customer Pick-up',
							      $role,
							      'admin.php?page='.$fulfillment_page.'&product_cat=goods&delivery=pickup'
							  		);
		add_submenu_page($fulfillment_page,
							      'Fulfillment - Shipping',
							      ' - Shipping',
							      $role,
							      'admin.php?page='.$fulfillment_page.'&product_cat=goods&delivery=shipping'
							  		);
	}

	function show_financials_menu($role) {
		//fulfillment pages
		$financials_page = 'hdgs_financials';
		$hook = add_menu_page( __( 'Financials', 'wcvendors' ), __( 'Financials', 'wcvendors' ), $role, $financials_page, array( $this, 'orders_page' ), 'dashicons-analytics' );
		add_action( "load-".$hook, array( $this, 'add_options' ) );
		add_submenu_page($financials_page,
							      'Financials - Deals',
							      ' - Deals',
							      $role,
							      'admin.php?page='.$financials_page.'&product_cat=deals'
									  );
		add_submenu_page($financials_page,
							      'Financials - Goods',
							      ' - Goods',
							      $role,
							      'admin.php?page='.$financials_page.'&product_cat=goods'
							  		);
	}
 
	function settings_page() {  
		$user_id = get_current_user_id(); 
		$paypal_address   = true; 
		$shop_description = true; 
		$description = get_user_meta( $user_id, 'pv_shop_description', true );
		$seller_info = get_user_meta( $user_id, 'pv_seller_info', true );
		$has_html    = get_user_meta( $user_id, 'pv_shop_html_enabled', true );
		$shop_page   = WCV_Vendors::get_vendor_shop_page( wp_get_current_user()->user_login );
		$global_html = WC_Vendors::$pv_options->get_option( 'shop_html_enabled' );
		include('views/html-vendor-settings-page.php'); 
	}

	function admin_enqueue_order_style() { 

		$screen 	= get_current_screen(); 
		$screen_id 	= $screen->id; 

		if ( 'wcv-vendor-orders' === $screen_id OR 'hdgs_fulfillment'  === $screen_id OR 'hdgs_financials' == $screen_id){ 

			add_thickbox();
			wp_enqueue_style( 'admin_order_styles', wcv_assets_url . 'css/admin-orders.css' );
		}	
	}

	/** 
	*	Save shop settings 
	*/
	public function save_shop_settings()
	{
		$user_id = get_current_user_id();
		$error = false; 
		$error_msg = '';

		if (isset ( $_POST[ 'wc-vendors-nonce' ] ) ) { 

			if ( !wp_verify_nonce( $_POST[ 'wc-vendors-nonce' ], 'save-shop-settings-admin' ) ) {
				return false;
			}

			if ( !is_email( $_POST[ 'pv_paypal' ] ) ) {
				$error_msg .=  __( 'Your PayPal address is not a valid email address.', 'wcvendors' );
				$error = true; 
			} else {
				update_user_meta( $user_id, 'pv_paypal', $_POST[ 'pv_paypal' ] );
			}
		
			if ( !empty( $_POST[ 'pv_shop_name' ] ) ) {
				$users = get_users( array( 'meta_key' => 'pv_shop_slug', 'meta_value' => sanitize_title( $_POST[ 'pv_shop_name' ] ) ) );
				if ( !empty( $users ) && $users[ 0 ]->ID != $user_id ) {
					$error_msg .= __( 'That shop name is already taken. Your shop name must be unique.', 'wcvendors' ); 
					$error = true; 
				} else {
					update_user_meta( $user_id, 'pv_shop_name', $_POST[ 'pv_shop_name' ] );
					update_user_meta( $user_id, 'pv_shop_slug', sanitize_title( $_POST[ 'pv_shop_name' ] ) );
				}
			}

			if ( isset( $_POST[ 'pv_shop_description' ] ) ) {
				update_user_meta( $user_id, 'pv_shop_description', $_POST[ 'pv_shop_description' ] );
			}

			if ( isset( $_POST[ 'pv_seller_info' ] ) ) {
				update_user_meta( $user_id, 'pv_seller_info', $_POST[ 'pv_seller_info' ] );
			}

			do_action( 'wcvendors_shop_settings_admin_saved', $user_id );

			if ( ! $error ) {
				add_action( 'admin_notices', array( $this, 'add_admin_notice_success' ) ); 
			} else { 
				$this->dashboard_error_msg = $error_msg; 
				add_action( 'admin_notices', array( $this, 'add_admin_notice_error' ) );  	
			}
		}
	}

	/**
	 * Output a sucessful message after saving the shop settings
	 * 
	 * @since 1.9.9 
	 * @access public 
	 */
	public function add_admin_notice_success( ){ 

		echo '<div class="updated"><p>';
		echo __( 'Settings saved.', 'wcvendors' );
		echo '</p></div>';

	} // add_admin_notice_success() 

	/**
	 * Output an error message 
	 * 
	 * @since 1.9.9 
	 * @access public 
	 */
	public function add_admin_notice_error( ){ 

		echo '<div class="error"><p>';
		echo $this->dashboard_error_msg; 		
		echo '</p></div>';

	} // add_admin_notice_error()

	/**
	 *
	 *
	 * @param unknown $status
	 * @param unknown $option
	 * @param unknown $value
	 *
	 * @return unknown
	 */
	public static function set_table_option( $status, $option, $value )
	{
		if ( $option == 'orders_per_page' ) {
			return $value;
		}
	}


	/**
	 *
	 */
	public static function add_options()
	{
		global $WCV_Vendor_Order_Page;

		$page = $_GET['page'];

		$args = array(
			'label'   => 'Rows',
			'default' => 10,
			'option'  => 'orders_per_page_'.$page,
		);
		add_screen_option( 'per_page', $args );

		$WCV_Vendor_Order_Page = new WCV_Vendor_Order_Page();

	}


	/**
	 * HTML setup for the Orders Page 
	 */
	public static function orders_page() {

		if (isset($_GET['rate_maintainer'])) {
			hdgs_rate_maintainer();
			return;
		}

		global $WCV_Vendor_Order_Page;

		if (!$WCV_Vendor_Order_Page) {
			$WCV_Vendor_Order_Page = new WCV_Vendor_Order_Page();
		}

		$WCV_Vendor_Order_Page->prepare_items();
		

		$title = 'Orders';
		$product_cat = '';
		if (isset($_GET['product_cat'])) {
			$product_cat = $_GET['product_cat'];
			$title .= ' - '.ucwords($product_cat);
 		}

 		$page = '';
 		if (isset($_GET['page'])) {
			$page = $_GET['page'];
 		}

		if ('hdgs_fulfillment' == $page) {
			$title = 'Fulfillment';
			if (isset($_GET['product_cat'])) {
				if ('deals' == $product_cat) {
					$title .= ' - Deal Redemption';
				} elseif ('goods' == $product_cat) {
					$delivery = '';
			 		if (isset($_GET['delivery'])) {
			 			$delivery = $_GET['delivery'];
			 		}
					if ('pickup' == $delivery) {
						$title .= ' - Customer Pick-up';
					} else {
						$title .= ' - Shipping';
					}
				}
			}
		} elseif ('hdgs_financials' == $page) {
			$title = 'Financials';
			if ('deals' == $product_cat) {
				$title .= ' - Deal Redemption';
			} elseif ('goods' == $product_cat) {
				$title .= ' - Goods';
			}
		}

 		$orders_export_url = hdgs_get_current_url().'&export=order_list';

		?>
		<div class="wrap">

			<div id="icon-woocommerce" class="icon32 icon32-woocommerce-reports"><br/></div>
			<h2><?php _e( $title, 'wcvendors' ); ?></h2>
			<a href="<?php echo $orders_export_url; ?>">Export Orders</a>

			<form id="posts-filter" method="get">

				<input type="hidden" name="page" value="<?php echo $page; ?>"/>

				<?php if ('hdgs_financials' != $page) {
								$WCV_Vendor_Order_Page->show_statuses(); //status filter 
							}
				?>

				<?php $WCV_Vendor_Order_Page->search_box( 'Search Orders', 'shop_order' ); //search box ?>

				<?php $WCV_Vendor_Order_Page->display(); ?>

			</form>
			<div id="ajax-response"></div>
			<br class="clear"/>
		</div>

	<?php 

	}

} // End WCV_Vendor_Admin_Dashboard

if ( !class_exists( 'WP_List_Table' ) ) require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';

/**
 * WCV Vendor Order Page 
 * 
 * @author Jamie Madden <http://wcvendors.com / https://github.com/digitalchild>
 * @package WCVendors 
 * @extends WP_List_Table
 */
class WCV_Vendor_Order_Page extends WP_List_Table
{

	public $index;

	/**
	 * can_view_comments
	 *  
	 * @since    1.0.0
	 * @access   public
	 * @var      string    $can_view_comments    permission check for view comments
	 */
	public $can_view_comments;


	/**
	 * can_add_comments
	 *  
	 * @since    1.0.0
	 * @access   public
	 * @var      string    $can_add_comments    permission check for add comments
	 */
	public $can_add_comments;


	/**
	 * __construct function.
	 *
	 * @access public
	 */
	function __construct()
	{
		global $status, $page;

		$this->index = 0;

		//Set parent defaults
		parent::__construct( array(
								  'singular' => __( 'order', 'wcvendors' ),
								  'plural'   => __( 'orders', 'wcvendors' ), 
								  'ajax'     => false,
							 ) );

		$this->can_view_comments = WC_Vendors::$pv_options->get_option( 'can_view_order_comments' );
		$this->can_add_comments = WC_Vendors::$pv_options->get_option( 'can_submit_order_comments' );
	}

	//show a list of links with names = available order statuses. clicking will filter the list to just that status
	public function show_statuses() {

		$user_id = get_current_user_id();
		$post_status = '';
		if (isset($_GET['post_status'])){
			$post_status = $_GET['post_status'];
		} else {
			$post_status = 'all';
		}

		if (isset($_GET['page'])) {
			$current_url = admin_url('admin.php?page='.$_GET['page']);
		} elseif (isset($_GET['product_cat'])) {
			$current_url .= '&product_cat='.$_GET['product_cat'];
		} else {
			return;
		}		

		//get this user's products, filtered by category
		$product_cat = '';
		if (isset($_GET['product_cat'])) {
			$product_cat = $_GET['product_cat'];
		}
		$orders = hdgs_get_orders_of_seller($user_id, $product_cat);

		if (!$orders) {
			return false;
		}

		$views['all'] = count($orders);

		foreach ($orders as $order_id) {
			$order = wc_get_order( $order_id );
			if (isset($views[$order->get_status()])) {
				++$views[$order->get_status()];
			} else {
				$views[$order->get_status()] = 1;
			}
			
		}

	?>
		<ul class="subsubsub">
			<?php foreach ($views as $class => $count): ?>
			<li class="<?php echo $class; ?>"><a href="<?php echo $current_url.'&post_status='.$class; ?>" class="<?php if ($class == $post_status) echo 'current aria-current="page"'; ?>" ><?php echo ucwords(hdgs_translate_order_status($class)); ?> <span class="count">(<?php echo $count; ?>)</span></a> |</li>
		<?php endforeach; ?>
		</ul>
	<?php
	}

	//create a search box 
	public function search_box( $text, $input_id ) {
		
		$search_array = array('product' => 'Product', 
													'order_id' => 'Order ID', 
													'customer' => 'Customer',
												);

		if (isset($_GET['product_cat']) AND 'deals' == $_GET['product_cat']) {
			$search_array['branch'] = 'Branch';
		}

		$filterby = '';
		if (isset($_GET['filterby'])) {
			$filterby = sanitize_text_field( $_GET['filterby'] );
		}

		?>

			<p class="search-box">
				<select name="filterby" id="filterby">
					<?php foreach ($search_array as $value => $blurb): ?>
						<option value="<?php echo $value; ?>" <?php selected($value, $filterby); ?>><?php echo $blurb; ?></option>
					<?php endforeach; ?>
				</select>
				<label class="screen-reader-text" for="<?php echo esc_attr( $input_id ); ?>"><?php echo $text; ?>:</label>
				<input type="search" id="<?php echo esc_attr( $input_id ); ?>" name="s" value="<?php _admin_search_query(); ?>" />
				<?php submit_button( $text, '', '', false, array( 'id' => 'search-submit' ) ); ?>
			</p>

		<?php		
	}


	/**
	 * column_default function.
	 *
	 * @access public
	 *
	 * @param unknown $item
	 * @param mixed   $column_name
	 *
	 * @return unknown
	 */
	function column_default( $item, $column_name ) {
		global $wpdb;

		$page = '';
		if (isset($_GET['page'])) {
			$page = $_GET['page'];	
		} 

		switch ( $column_name ) {
			case 'order_id' :
				if ('hdgs_financials' == $page) {
					$order_url = admin_url('admin.php?page=order_financials&order='.$item->order_id);
					return  '<a href="'.$order_url.'">#'.$item->order_id.'</a>';
				} elseif (current_user_can('vendor') OR is_super_admin()) {
					$order_url = admin_url('admin.php?page=merchant_order&order='.$item->order_id);
					return  '<a href="'.$order_url.'">#'.$item->order_id.'</a>';	
				} elseif (current_user_can( 'branch_account' )) {
					return '#'.$item->order_id;
				}				
				
			case 'date' : 
				if ($item->date) {
					return date_i18n( 'm/d/y g:i A', $item->date); 	
				} else {
					return '';
				}

			case 'status' : 
				return $item->status;

			case 'customer' : 
				return $item->customer; 

			case 'products' :
				return $item->products; 

			case 'delivery_option':
				return $item->delivery_option; 

			case 'total' : 
				return wc_price($item->total);

			case 'branch' :
				return $item->branch;

			case 'voucher_code' :
				return $item->voucher_code;

			case 'courier_name' :
				return $item->courier_name;

			case 'package_count' :
				return $item->package_count;

			case 'tracking_number' :
				return $item->tracking_number;

			//financial field
			case 'date_completed':
				if ($item->date_completed) {
					return date_i18n( 'm/d/y g:i A', $item->date_completed); 
				} else {
					return '';
				}

			case 'sales_package':
				return $item->sales_package;

			case 'total_payout':
				return wc_price($item->total_payout);

			case 'payout_status':
				global $allowed_payout_statuses;
				return $allowed_payout_statuses[$item->payout_status];

			case 'payout_schedule':
				if ($item->payout_schedule) {
					return date_i18n( 'm/d/y g:i A', $item->payout_schedule); 
				}	else {
					return '';
				}			

			case 'fin_remarks':
				return $item->fin_remarks;

			// case 'comments' : 
			// 	return $item->comments; 
			
			default: 
				return apply_filters( 'wcvendors_vendor_order_page_column_default', '', $item, $column_name );
		}

		return '';
	}


	/**
	 * column_cb function.
	 *
	 * @access public
	 *
	 * @param mixed $item
	 *
	 * @return unknown
	 */
	function column_cb( $item )
	{
		return sprintf(
			'<input type="checkbox" name="%1$s[]" value="%2$s" />',
			/*$1%s*/
			'order_id',
			/*$2%s*/
			$item->order_id
		);
	}


	/**
	 * get_columns function.
	 *
	 * @access public
	 * @return unknown
	 */
	function get_columns()
	{
		$columns = array('cb'        			=> '<input type="checkbox" />',
										'order_id'  			=> __( 'Order ID', 'hdgs_text_domain' ),
										'date'      			=> __( 'Date', 'hdgs_text_domain' ),
										'status'    			=> __( 'Order Status', 'hdgs_text_domain' ),
										'customer'  			=> __( 'Customer', 'hdgs_text_domain' ),
										'products'  			=> __( 'Order', 'hdgs_text_domain' ), 
										'branch'					=> __( 'Branch', 'hdgs_text_domain'), 
										'voucher_code' 		=> __( 'Voucher Code', 'hdgs_text_domain'), 
										'delivery_option' => __( 'Delivery Option', 'hdgs_text_domain'), 
										'total'						=> __( 'Net Sales', 'hdgs_text_domain'),
										'courier_name'		=> __( 'Courier Name', 'hdgs_text_domain'),
										'package_count'		=> __( 'Package Count', 'hdgs_text_domain'),
										'tracking_number'	=> __( 'Tracking Number', 'hdgs_text_domain'), 
									);						

		//unset unneeded columns depending on the view
		$product_cat = '';
		if (isset($_GET['product_cat'])) {
			$product_cat = $_GET['product_cat'];	
		}
		

		if (!is_super_admin()) {
			unset($columns['courier_name']);	
		}
		if ('deals' == $product_cat) {
			unset($columns['delivery_option']);
			unset($columns['package_count']);
			unset($columns['tracking_number']);
			unset($columns['courier_name']);
		} elseif ('goods' == $product_cat) {
			unset($columns['voucher_code']);
		}

		if (isset($_GET['page']) AND 'hdgs_fulfillment' == $_GET['page']) {			
			unset($columns['total']);
			if (isset($_GET['delivery'])) {
				unset($columns['delivery_option']);
				if (isset($_GET['delivery']) AND 'pickup' == $_GET['delivery']) {
					unset($columns['courier_name']);
					unset($columns['tracking_number']);
					unset($columns['package_count']);
				}
			}
		}

		if (isset($_GET['page']) AND 'hdgs_financials' == $_GET['page']) {	
			$columns = array('cb'        			=> '<input type="checkbox" />',
										'order_id'  			=> __( 'Order ID', 'hdgs_text_domain' ), 
										'date_completed'	=> __( 'Order Completion', 'hdgs_text_domain'), 
										'sales_package'		=> __( 'Sales Package', 'hdgs_text_domain'), 
										'total_payout'		=> __( 'Total Payout', 'hdgs_text_domain'), 
										'payout_status'		=> __( 'Payout Status', 'hdgs_text_domain'), 
										'payout_schedule'	=> __( 'Payout Schedule', 'hdgs_text_domain'), 
										'fin_remarks'			=> __( 'Remarks', 'hdgs_text_domain'), 
									);

		}

		return $columns;

	}


	/**
	 * get_sortable_columns function.
	 *
	 * @access public
	 * @return unknown
	 */
	function get_sortable_columns()
	{
		$sortable_columns = array(
			'order_id'  	=> array( 'order_id', false ),
			//'total'  		=> array( 'total', false ),
			//'status'     	=> array( 'status', false ),
			'date'      => array('date', false ),
		);

		return $sortable_columns;
	}


	/**
	 * Get bulk actions
	 *
	 * @return unknown
	 */
	function get_bulk_actions() {

		global $allowed_merchant_statuses;

		$actions = $allowed_merchant_statuses;
		$post_status = '';
		if (isset($_GET['post_status'])){
			$post_status = sanitize_text_field( $_GET['post_status'] );
		}
		
		$product_cat = '';
		if (isset($_GET['product_cat'])) {
			$product_cat = $_GET['product_cat'];
		}		
		if ('deals' == $product_cat) {
			unset($actions['ready-to-ship']);
			unset($actions['picked-up']);
		} elseif ('goods' == $product_cat) {
			$actions['print-shipping-labels'] = 'Print Shipping Labels';
		}
		//more action restrictions for branch accounts
		if (current_user_can( 'branch_account' )) {
			if ('pickup' == $_GET['delivery']) {
				unset($actions['ready-to-ship']);
				unset($actions['completed']);
				unset($actions['print-shipping-labels']);
			} elseif ('shipping' == $_GET['delivery']) {
				unset($actions['picked-up']);
			} 
		}

		foreach ($actions as $key => $val) {
			if ($key == $post_status) {
				unset($actions[$key]);
			} elseif ($key == 'print-shipping-labels') {
				continue; //no need to change
			} else {
				$actions[$key] = 'Mark as '.ucwords(hdgs_translate_order_status($key));	
			}
			
		}

		return $actions;
	}


	/**
	 * Process bulk actions
	 *
	 * @return unknown
	 */
	function process_bulk_action()
	{
		if ( !isset( $_GET[ 'order_id' ] ) ) return;

		if (is_array(  $_GET[ 'order_id' ] ) ) { 

			$items = array_map( 'intval', $_GET[ 'order_id' ] );
			
			$result = $this->mark_these( $items, $this->current_action() );
			if ( $result ) {
				hdgs_show_admin_notices();
			}

		} else { 

			if ( !isset( $_GET[ 'action' ] ) ) return;
		}


	}

	//NOTE: it's the child order that is updated, not the parent
	public function mark_these($ids = array(), $action) {

		global $woocommerce, $allowed_merchant_statuses;
		$user_id = get_current_user_id();

		if ( !empty( $ids ) ) {
			
			if ($action == 'print-shipping-labels') {
				
				$result = hdgs_print_shipping_label($ids);

			//change the status
			} else {

				foreach ($ids as $order_id ) {

					$child_order_ids = hdgs_get_child_order_ids($order_id, $merchant_id);
					$child_id = $child_order_ids[0];

					$result = hdgs_mark_as($action, $child_id);	
					
					//additional stuff when marked as picked up / shipped
					if ('picked-up' == $action) {
						$shippers = (array) get_post_meta( $child_id, 'wc_pv_shipped', true );
						if( !in_array($user_id, $shippers)) {
							$shippers[] = $user_id;
							$mails = $woocommerce->mailer()->get_emails();
							if ( !empty( $mails ) ) {
								$mails[ 'WC_Email_Notify_Shipped' ]->trigger( $child_id, $user_id );
							}
							do_action('wcvendors_vendor_ship', $child_id, $user_id);
						}
						update_post_meta( $child_id, 'wc_pv_shipped', $shippers );
					}
				}
			}

			if ($result) {
				$blurb = ucwords(hdgs_translate_order_status($allowed_merchant_statuses[$action]));
				$_SESSION['hdgs_admin_notices'][] = array('type' => 'success', 
				                                        'message' => 'Orders Marked as '.$blurb, 
				                                    		);
			}

			return $result;
		}

		return false;
	}


	/**
	 *  Mark orders as shipped 
	 *
	 * @param unknown $ids (optional)
	 *
	 * @return unknown
	 */
	public function mark_shipped( $ids = array() )
	{
		return;
		global $woocommerce;

		$user_id = get_current_user_id();

		if ( !empty( $ids ) ) {
			foreach ($ids as $order_id ) {
				$order = wc_get_order( $order_id );
				$vendors = WCV_Vendors::get_vendors_from_order( $order );
				$vendor_ids = array_keys( $vendors );

				//for branch accounts, only allow if they are the only branch for that order
				if (current_user_can('branch_account')) {
					$pickup_branch_id = get_post_meta($order->ID, 'hdgs_pickup_branch', true);
					if ($pickup_branch_id == $user_id) {
						$vendor_ids[] = $user_id;
					}
				}

				if ( !in_array( $user_id, $vendor_ids ) ) {
					wp_die( __( 'You are not allowed to modify this order.', 'wcvendors' ) );
				}
				$shippers = (array) get_post_meta( $order_id, 'wc_pv_shipped', true );
				if( !in_array($user_id, $shippers)) {
					$shippers[] = $user_id;
					$mails = $woocommerce->mailer()->get_emails();
					if ( !empty( $mails ) ) {
						$mails[ 'WC_Email_Notify_Shipped' ]->trigger( $order_id, $user_id );
					}
					do_action('wcvendors_vendor_ship', $order_id, $user_id);
				}
				update_post_meta( $order_id, 'wc_pv_shipped', $shippers );
			}
			return true; 
		}
		return false; 
	}



	/**
	 *  Get Orders to display in admin 
	 *
	 * @return $orders
	 */
	function get_orders() { 

		$user_id = get_current_user_id(); 

		if (current_user_can('vendor')) {
			$merchant_id = $user_id;
		} elseif (current_user_can('branch_account')) {
			$merchant_id = get_user_meta($user_id, 'parent_merchant', true);
		}

		$product_cat = '';
		if (isset($_GET['product_cat'])) {
			$product_cat = $_GET['product_cat'];	
		}
		$orders = array(); 

		//setup search variables
		$filterby = '';
		if (isset($_GET['filterby'])) {
			$filterby = $_GET['filterby'];
		}
		$product_search = '';
		$order_search = '';
		$customer_ids = array();
		$branch_ids = array();
		$order_list = array();

		switch ($filterby) {
			
			case 'product':
				$product_search = sanitize_text_field($_GET['s']);
			break;
			
			case 'order_id':
				if (isset($_GET['s'])) {
					$order_search = $_GET['s'] + 0;
				}				
			break;
			
			case 'customer':
				$customer_search = sanitize_text_field($_GET['s']);
				$users = get_users( array( 'search' => '*'.$customer_search.'*' ) );
				// Array of WP_User objects.
				foreach ( $users as $user ) {
					$customer_ids[$user->ID] = $user->ID;
				}
				$customer_ids = array_filter($customer_ids);
			break;
			
			case 'branch':
				$branch_search = sanitize_text_field($_GET['s']);
				$users = get_users( array( 'search' => '*'.$branch_search.'*' ) );
				// Array of WP_User objects.
				foreach ( $users as $user ) {
					$branch_ids[$user->ID] = $user->ID;
				}
				$branch_ids = array_filter($branch_ids);
			break;
			
		}

		//get the pagination data from query
		if (isset($_GET['paged'])) {
			$paged = $_GET['paged'];
		} else {
			$paged = 1;
		}

		if (isset($_GET['page'])) {
			$page = $_GET['page'];	
		} else {
			$page = '';
		}

		$child_orders = hdgs_get_orders_of_seller($user_id, $product_cat, $product_search, $page);	

		if (!empty( $child_orders ) AND is_array($child_orders) ) { 		

			foreach ( $child_orders as $order_object ) {

				$valid 	= array();
				$products = '';
				$product_list = array(); 
				$net_sales = 0;
				$branch = '';
				$order = '';

				//order here is the parent order by default
				//child orders are called when needed
				$parent_id = wp_get_post_parent_id($order_object->ID);
				if ($parent_id) {
					$order = wc_get_order( $parent_id );	
				}

				if (!$order) {
					continue;
				}

				//only show orders once
				$order_id = $order->get_id(); 
				if (!in_array($order_id, $order_list)) {
					$order_list[] = $order_id;
				} else {
					continue;
				}
				
				//only show paid orders in financials page
				$date_completed = $order->get_date_completed();
				if ('hdgs_financials' == $page AND !$date_completed) {
					continue;
				}

				if ($date_completed) {
					$date_completed = $date_completed->getTimestamp();
				}

				//set up child order data				
				$child_order = wc_get_order( $order_object );
				$child_id = $child_order->get_id();
				$order_status = $child_order->get_status();	

				//skip orders that aren't a match to the needed type
				$post_status = '';
				if (isset($_GET['post_status'])) {
					$post_status = $_GET['post_status'];
				}
				if ($post_status 
						AND $post_status != 'all' 
						AND $order_status != $post_status) {
					continue;
				}

				//if searching for a particular order, skip the other order ids
				if ($order_search > 1 AND $order_id != $order_search) {
					continue;
				}

				//if searching for a customer, exclude other users
				$cust_id = $order->get_customer_id();
				if (array_filter($customer_ids) 
						AND !in_array($cust_id, $customer_ids)) {
					continue;
				}

				//skip branches that aren't a match
				$pickup_branch_id = get_post_meta($child_id, 'hdgs_pickup_branch', true);
				if (array_filter($branch_ids) 
						AND !in_array($branch_ids, $pickup_branch_id)) {
					continue;
				}

				//get the line items
				$items = $order->get_items();

				//check if the order items are owned by this user. only used for non-admins
				if (!current_user_can('administrator')) {
					foreach ( $items as $order_item_id => $item) {
						$seller_id = get_post_field( 'post_author', $item->get_product_id()); 
						if ($seller_id == $merchant_id) {
							$valid[ $order_item_id ] = $item;
						}
					}
					//skip order if has no items
					if (!array_filter($valid)) continue;
				}
				

				//filters for delivery under fulfullment
				$delivery_filter = '';
				if (isset($_GET['delivery'])) {
					$delivery_filter = $_GET['delivery'];
				}
				if($delivery_filter) {
					if ('pickup' == $delivery_filter 
							AND $order->get_shipping_method() != 'Local Pickup') {
						continue;
					} elseif ('shipping' == $delivery_filter 
										AND $order->get_shipping_method() == 'Local Pickup') {
						continue;
					}
				}

				
				$order_date = ( version_compare( WC_VERSION, '2.7', '<' ) ) ? $order->order_date : $order->get_date_created(); 

				$voucher_code = '';
				foreach ( $valid as $item_id => $item ) {
					//link the order number to the order page
					$order_items = array(); 

					if ($products) {
						$products .= '<br />';
					}
					$products .= '<strong>'. $item['qty'] . ' x <a href="'.get_edit_post_link( $item->get_product_id() ). '">' . $item['name'] . '</a></strong>'; 
					
					$product_list[] = $item['qty'].' x '.$item['name'];

					if ($products) {
						$voucher_code .= '<br/>';
					}
					$voucher_code .= get_post_meta($item->get_product_id(), 'voucher_code', true);
				}

				//reordered this and added a few items
				
				$order_status = hdgs_translate_order_status($order_status, $child_id);
				
				//for deals, get branch and voucher
				$product_cat = '';
				$branch = '';
				if (isset($_GET['product_cat'])) {
					$product_cat = $_GET['product_cat'];
				}
				
				if ('deals' == $product_cat AND $pickup_branch_id) {
					$branch_account = get_user_by( 'id', $pickup_branch_id );
					$branch = $branch_account->user_login;
				}

				//get net sales
				$gross_sales = $child_order->get_subtotal() - $child_order->get_total_tax();
				$net_sales = $gross_sales - $child_order->get_total_discount();	

				//delivery info uses child order and saves it there so let's get it there
				$package_count = get_post_meta($child_id,'package_count',true);
				$tracking_number = get_post_meta($child_id,'_wcst_order_trackno',true);
				$courier_data = hdgs_get_courier_data($child_id);
				$courier_name = $courier_data['courier'];

				$sales_package = get_user_meta($user_id, 'hdgs_active_package', true);

				$total_payout = hdgs_calculate_total_payout($child_id);
				$payout_status = get_post_meta($child_id,'hdgs_payout_status',true);
				if(!$payout_status) {
					$payout_status = 'pending';
				}
				$payout_schedule = hdgs_get_next_payout($date_completed);
				$fin_remarks = get_post_meta($child_id,'hdgs_fin_remarks',true);

				$order_items[ 'order_id' ] 				= $order_id;
				$order_items[ 'date' ] 						= strtotime( $order_date ); 
				$order_items[ 'status' ] 					= ucwords($order_status); //was $shipped;
				$order_items[ 'customer' ]				= $order->get_formatted_shipping_address();
				$order_items[ 'products' ] 				= $products; 
				$order_items[ 'delivery_option' ] = $child_order->get_shipping_method();
				$order_items[ 'branch' ] 					= $branch;
				$order_items[ 'voucher_code' ] 		= $voucher_code;	
				$order_items[ 'total' ] 					= $net_sales;
				$order_items[ 'courier_name' ]		= $courier_name;
				$order_items[ 'package_count' ] 	= $package_count;
				$order_items[ 'tracking_number' ]	= $tracking_number;

				$order_items[ 'product_list' ] 		= $product_list;

				//financial items
				$order_items[ 'date_completed' ]	= $date_completed;
				$order_items[	'sales_package' ]		= $sales_package;
				$order_items[	'total_payout' ]		= $total_payout;
				$order_items[	'payout_status' ]		= $payout_status;
				$order_items[	'payout_schedule' ]	= $payout_schedule;
				$order_items[	'fin_remarks' ]			= $fin_remarks;

				$orders[] = (object) $order_items; 

			}
		}

		return $orders; 

	}

	/**
	 *  Get the vendor products sold 
	 *
	 * @param $user_id  - the user_id to get the products of 
	 *
	 * @return unknown
	 */
	public function get_vendor_products( $user_id )
	{
		global $wpdb;

		$vendor_products = array();
		$sql = '';

		$sql .= "SELECT product_id FROM {$wpdb->prefix}pv_commission WHERE vendor_id = {$user_id} AND status != 'reversed' GROUP BY product_id"; 

		$results = $wpdb->get_results( $sql );

		foreach ( $results as $value ) {
			$ids[ ] = $value->product_id;
		}

		if ( !empty( $ids ) ) {
			$vendor_products = get_posts( 
				array(
				   'numberposts' => -1,
				   'orderby'     => 'post_date',
				   'post_type'   => array( 'product', 'product_variation' ),
				   'order'       => 'DESC',
				   'include'     => $ids
			  	)
			);
		}

		return $vendor_products;
	}


	/**
	 * All orders for a specific product
	 *
	 * @param array $product_ids
	 * @param array $args (optional)
	 *
	 * @return object
	 */
	public function get_orders_for_vendor_products( array $product_ids, array $args = array() )
	{
		global $wpdb;

		if ( empty( $product_ids ) ) return false;

		$defaults = array(
			'status' => apply_filters( 'wcvendors_completed_statuses', array( 'completed', 'processing' ) ),
		);

		$args = wp_parse_args( $args, $defaults );


		$sql = "
			SELECT order_id
			FROM {$wpdb->prefix}pv_commission as order_items
			WHERE   product_id IN ('" . implode( "','", $product_ids ) . "')
			AND     status != 'reversed'
		";

		if ( !empty( $args[ 'vendor_id' ] ) ) {
			$sql .= "
				AND vendor_id = {$args['vendor_id']}
			";
		}

		$sql .= "
			GROUP BY order_id
			ORDER BY time DESC
		";

		$orders = $wpdb->get_results( $sql );

		return $orders;
	}



	/**
	 * prepare_items function.
	 *
	 * @access public
	 */
	function prepare_items() {

		
		/**
		 * Init column headers
		 */
		$this->_column_headers = $this->get_column_info();

		/**
		 * Process bulk actions
		 */
		$this->process_bulk_action();

		/**
		 * Get items
		 */
		
		$this->items = $this->get_orders();

		/**
		 * Pagination
		 */
	
		if (isset($_GET['page'])) {
			$items_per_page = get_option('orders_per_page_'.$_GET['page']);
		} else {
			$items_per_page = 10;
		}

		$total_items = count($this->items); 
		
		$per_page = $this->get_items_per_page($items_per_page, 10);

		$this->set_pagination_args( array(
		  'total_items' => $total_items,
		  'per_page'    => $per_page,  
		) );

		//get current page
		if (isset($_GET['paged'])) {
			$paged = $_GET['paged'];
		} else {
			$paged = 0;
		}
		
		//split order ids by page
		$offset = max(0,($paged-1)*$per_page);
		$this->items = array_slice($this->items, $offset, $per_page);


	}


}

//run export orders
add_action('admin_init', 'hdgs_export_order_list');
function hdgs_export_order_list(){

	//only for branch accounts and merchants
	if (!current_user_can('vendor') AND !current_user_can('branch_account')) {
		return false;
	}

	if (isset($_GET['export'])) {
		$get_export = $_GET['export'];
	} else {
		$get_export = '';
	}
	if ('order_list' != $get_export) {
		return false;
	}

	$current_user = wp_get_current_user();
	$now = current_time('timestamp',0);

	$filename = $current_user->user_login.'_orderlist_'.date_i18n('mdy',$now);

	$WCV_Vendor_Order_Page = new WCV_Vendor_Order_Page();

	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: private", false);
	header("Content-Type: application/octet-stream");
	header('Content-Disposition: attachment; filename='.$filename.'.csv' );
	header("Content-Transfer-Encoding: binary");

  // output the CSV data
  $output = @fopen('php://output', 'w');
  //fprintf( $output, chr(0xEF) . chr(0xBB) . chr(0xBF) );
  $columns = $WCV_Vendor_Order_Page->get_columns();
  unset($columns['cb']);
  fputcsv($output, $columns);
	
	//add data
	$items = $WCV_Vendor_Order_Page->get_orders();
  foreach ($items as $item) {
  	$row = array();
  	foreach ($columns as $slug => $name) {

  		switch ( $slug ) {
				
				case 'order_id' :
					$str = $item->order_id;
				break;
				
				case 'date' : 
					if ($item->date) {
						$str = date('m/d/y H:i', $item->date); 	
					} else {
						$str = '';
					}
					
				break;

				case 'status' : 
					$str = $item->status;
				break;

				case 'customer' : 
					$str = str_replace('<br/>', "\n\r", $item->customer); 
				break;

				case 'products' :
					$str = implode("\n\r", $item->product_list);
				break;
				
				case 'delivery_option':
					$str = $item->delivery_option; 
				break;

				case 'total' : 
					$str = 'P'.$item->total;
				break;

				case 'branch' :
					$str = $item->branch;
				break;

				case 'voucher_code' :
					$str = $item->voucher_code;
				break;

				case 'courier_name' :
					$str = $item->courier_name;
				break;

				case 'package_count' :
					$str = $item->package_count;
				break;

				case 'tracking_number' :
					$str = $item->tracking_number;
				break;

				//financial field
				case 'date_completed':
					if ($item->date_completed) {
						$str = date('m/d/y H:i', $item->date_completed); 
					} else {
						$str = '';
					}
				break;
					
				case 'sales_package':
					$str = $item->sales_package;
				break;

				case 'total_payout':
					$str = 'P'.$item->total_payout;
				break;

				case 'payout_status':
					global $allowed_payout_statuses;
					$str = $allowed_payout_statuses[$item->payout_status];
				break;

				case 'payout_schedule':
					if ($item->payout_schedule) {
						$str = date('m/d/y H:i', $item->payout_schedule); 
					} else {
						$str = '';
					}
				break;

				case 'fin_remarks':
					$str = $item->fin_remarks;
				break;

				
				default: 
					$str = apply_filters( 'wcvendors_vendor_order_page_column_default', 
																'', $item, $slug );	
				break;

			}

			$str = strip_tags( $str );  		

  		$row[] = $str;
  	}

  	fputcsv($output, $row);
  
  }

  fclose( $output );

  die();
}


//NOT WORKING! This is the are you sure bubble
function hdgs_confirm_bulk_actions_js() {
	?>
		<script>
			jQuery(document).ready(function(){

				var areyousure = '<div class="areyousure">Are you sure?<a href="#yes">Yes</a><a href="#no">No</a></div>';
				jQuery('input[value="Apply"]').append(areyousure);
				jQuery('input[value="Apply"]').click(function(e){
					jQuery('areyousure').toggle();
					e.preventDefault();
				});
			});
		</script>
		<style>
			.areyousure {
				position:absolute;
				top:0;
				right:0;
			}
		</style>
	<?php

}