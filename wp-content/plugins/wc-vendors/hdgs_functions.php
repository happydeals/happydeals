<?php

/**
 * Some shared functions that all
 * pages might need. 
 */

//only these status changes are allowed for merchants
global $allowed_merchant_statuses;
$allowed_merchant_statuses = array('ready-to-ship' => 'Ready to Ship', 
                            	'picked-up' => 'Picked Up', 
                            	'completed' => 'Delivered', 
                        			);
global $allowed_payout_statuses;
$allowed_payout_statuses = array('paid'       => 'Paid', 
				                        'collected'   => 'Collected', 
				                        'pending'			=> 'Pending', 
				                        'disputed'    => 'Under Dispute', 
				                        'refunded'    => 'Refunded',  
				                        'cancelled'		=> 'Cancelled', 
                      					);

//display admin notices after stuff is done
add_action( 'admin_notices', 'hdgs_show_admin_notices' );
function hdgs_show_admin_notices() {
	$notices = '';
	if (isset($_SESSION['hdgs_admin_notices'])) {
		$notices = $_SESSION['hdgs_admin_notices'];
	}
	
	if ($notices):
		foreach ($notices as $notice):
	   	?>
	    	<div class="notice notice-<?php echo $notice['type']; ?> is-dismissible">
	        <p><?php _e( $notice['message'], 'hdgs_text_domain' ); ?></p>
	    	</div>
	    <?php
	  endforeach;
	endif;

  unset($_SESSION['hdgs_admin_notices']);
}

/**
 * get this merchants or branch's orders
 *
 * @param int $user_id: The user's ID.
 * @param string $product_cat: the category filter, either deals or goods
 * @return array $orders: order_ids 
 */
function hdgs_get_orders_of_seller($user_id, $product_cat = '', $product_search = '', $page = '') {
	
	//check user role to get the right id
	$user = get_user_by('id', $user_id);
	$merchant_id = $user_id;

	if (in_array('branch_account', $user->roles)) {
		$merchant_id = get_user_meta($user_id, 'parent_merchant', true);
	} 	

	//get the products of the merchant.
	$products = hdgs_get_users_products($merchant_id, $product_cat, $product_search);

	foreach ($products as $product) {
		$all_products[] = $product->ID;
	}

	$orders = hdgs_get_orders_from_products($all_products, $page);		
	
	//3. if branch, remove orders not of this branch
	//deactivated for now
	if (in_array('branch_account', $user->roles)) {

		$branch_ids = get_user_meta($merchant_id, 'hdgs_branch_accounts', true );
		foreach ($orders as $key => $order) {
			$pickup_branch_id = get_post_meta($order->ID, 'hdgs_pickup_branch', true);
			if ($pickup_branch_id != $user_id) {
				unset($orders[$key]);
			}
		}		
	}
	
	return $orders;
}


/**
 * Get this user's products
 *
 * @param int $user_id: The user's ID.
 * @param string $product_cat: the category filter, either deals or goods
 * @param string $s: a search string filter
 * @return array of posts objects (WP Post) based on $args
 */
function hdgs_get_users_products($user_id, $product_cat = '', $product_search = '') {
	
	//get all status types except auto-draft
	$statuses = get_post_stati( '', 'names' );
	foreach ($statuses as $key => $status) {
		if ($status == 'auto-draft') {
			unset($statuses[$key]);
		}
	}

	//setup the basic argument for getting the products
	$args = array ('author' => $user_id,
								'post_type' => 'product',
								'posts_per_page' => -1,
								'post_status' => $statuses,
								); //get all status kinds
	//get all products if admin
	if (is_super_admin($user_id)) {
		unset($args['author']);
	}
	//add search capability
	if ($product_search) {
		$args['s'] = $product_search;
	}

	//just get relevant products for that post_type
	if ($product_cat == 'deals') {
		$args['tax_query'] = array(array('taxonomy' => 'product_cat',
																	'field' => 'slug',
																	'terms' => 'deals',
																	));
	}
	if ($product_cat == 'goods') {
		$args['tax_query'] = array(array('taxonomy' => 'product_cat',
																		'field' => 'slug',
																		'terms' => 'goods',
																		));
	}

	return get_posts($args);
}


/**
 * Get the merchants participating in an order. This usually returns only 1 merchant
 * works both for parent and child orders
 *
 * @param int $order_id: The id of the order
 * @return array $merchant_ids: the user_ids of the merchants involved
 */
function hdgs_get_merchants_from_order($order_id) {
	//convert single order_id into $order
	$order = wc_get_order($order_id);

	if (!$order) {
		return false;
	}
	
	$items = $order->get_items();
	foreach ( $items as $item) {
		 $product_id = $item->get_product_id();
     $merchant_id = get_post_field( 'post_author', $product_id); 
     $merchant_ids[] = $merchant_id;
	}

	//filter out duplicates
	if (isset($merchant_ids)) {
		$merchant_ids = array_unique($merchant_ids);
		return $merchant_ids;
	}
	
	return false;
	
}



/**
 * Get the shop_order_vendor orders from wp_posts of an array of product_ids 
 *
 * @param int|array $product_ids: a set of product_ids
 * @return array $orders: WP Post objects of the orders
 */
function hdgs_get_orders_from_products( $product_ids = array(), $page = ''){
	global $wpdb;	

	if (!$product_ids){
		return false;
	}

	if (is_numeric($product_ids)) {
		$product_ids = array($product_ids);
	}

	$order_ids = hdgs_get_line_items_from_products($product_ids);

	$args = array('posts_per_page'   => -1,
								'include'          => $order_ids,
								'post_type'        => 'shop_order_vendor',
								'post_status'      => 'any',
								'orderby'          => 'ID',
								'order'            => 'DESC',
								);

	return get_posts($args);
}



/**
 * get the order id of line items from a list of product IDs
 *
 * @param array $product_ids
 * @return array of parent order ids of the line items returned
 */
function hdgs_get_line_items_from_products( $product_ids = array()){
	global $wpdb;	

	if (is_numeric($product_ids)) {
		$product_ids = array($product_ids);
	} else {
		$product_ids = array_filter($product_ids);
	}

	if (!$product_ids) {
		return array();
	}

	//get the order ids. Convoluted? Super. But it's the only way
	//1. get the order item id from itemmeta
	$q = "SELECT order_item_id FROM {$wpdb->prefix}woocommerce_order_itemmeta " .
            "WHERE meta_key = '_product_id' AND meta_value IN (".implode(', ', $product_ids ).")";
	$order_item_ids = $wpdb->get_col($q);

	$order_item_sql = implode(', ', $order_item_ids);

	if (!$order_item_sql) {
		return array();
	}

	//2. next, get the order_ids of the line items
	$q = "SELECT order_id FROM {$wpdb->prefix}woocommerce_order_items " .
            "WHERE order_item_id IN (".$order_item_sql.") ".
            "ORDER BY order_id DESC";
	$line_items = $wpdb->get_col($q);


	return $line_items;
}



/**
 * get child orders of this order that are for this merchant
 *
 * @param array $order_id
 * @param array $merchant_id
 * @return array of order_ids for the child orders for this merchant
 */
function hdgs_get_child_order_ids($order_id, $merchant_id) {
	$child_orders = array();

	//1. get all the child orders of this order
	$args = array('post_parent' => $order_id, 
								'post_type'   => 'shop_order_vendor',  
								'numberposts' => -1, 
								'post_status' => 'any', 
							);
	$children = get_children( $args );

	//if has no children, return $order_id since it's a child order
	if (!$children) {
		return array($order_id);
	}



	//2. from this list, remove child orders not owned by this merchant
	if(is_array($children) AND $children) {
		foreach($children as $child_order) {
			$merchant_ids = hdgs_get_merchants_from_order($child_order->ID);
			if (in_array($merchant_id, $merchant_ids) 
					OR is_super_admin($merchant_id)) {
				$child_orders[] = $child_order->ID;
			}
		}
	}

	return $child_orders;
}



/**
 * get sales to date for user
 *
 * @param int $user_id
 * @param string $gross_or_net: type of sales, not or gross. Gross does not deduct discounts
 * @return int $sales_to_date: total sales amount in PHP
 */
function hdgs_get_sales_to_date($user_id, $gross_or_net = 'gross') {
	$sales_to_date = 0;
	if (get_user_meta($user_id, 'hdgs_account_status', true) == 'Inactive') return false; //no value if inactive account

	$hdgs_effectivity_date = strtotime(get_user_meta($user_id, 'hdgs_effectivity_date', true));	
	$hdgs_checked_products = get_user_meta($user_id,'hdgs_checked_products','true');

	$orders = hdgs_get_orders_from_products($hdgs_checked_products);

	if(is_array($orders)) {
		foreach ($orders as $o) {
			$order = wc_get_order( $o->ID );
			$order_data = $order->get_data();
			$order_created = $order_data['date_created']->getTimestamp();

			//only get data from completed (paid and delivered) orders and those made after the current LED1 package
			if ($order_data['status']  == 'completed' AND $order_created > $hdgs_effectivity_date) {
				$gross_sales = $order->get_subtotal() - $order->get_total_tax();
				$cust_shipping = $order->get_shipping_total();
				$net_sales = $gross_sales - $order->get_total_discount();
				if ($gross_or_net == 'net') {
					$sales_to_date += $net_sales;
				} else {
					$sales_to_date += $gross_sales;
				}
			}
		}
	}
	

	return $sales_to_date;
}



/**
 * get types of products included in an order
 *
 * @param int $order_id
 * @return array with boolean value for hasdeals and hasgoods
 */
function hdgs_get_product_cats($order_id) {

	$hasdeals = false;
	$hasgoods = false;

	if ($order_id) {
		$order = wc_get_order( $order_id );
		$items = $order->get_items();
		foreach ( $items as $item) {
			$product_id = $item->get_product_id();
			if (has_term('deals', 'product_cat', $product_id)) {
				$hasdeals = true;
			}
			if (has_term('goods', 'product_cat', $product_id)) {
				$hasgoods = true;
			}
		}
	}

	//also check the product_cat of the page
	$product_cat = '';
	if (isset($_GET['product_cat'])) {
		$product_cat = $_GET['product_cat'];
	}
	
	if ('deals' == $product_cat) {
		$hasdeals = true;
	}
	if ('goods' == $product_cat) {
		$hasgoods = true;
	}

	return array ('hasdeals' => $hasdeals, 
								'hasgoods' => $hasgoods, 
								);
}



/**
 * get LED balance and send alert if it goes over due to a purchase. 
 * triggers on completion and payment
 *
 * @param int $order_id
 * @return boolean, returns true on successful email alert sent
 */
add_action( 'woocommerce_order_status_completed', 'hdgs_check_balance', 10, 1);
add_action( 'woocommerce_order_status_processing', 'hdgs_check_balance', 10, 1);
function hdgs_check_balance($order_id){

	$order = wc_get_order( $order_id );
	$status = $order->get_status();

	$product_cats = hdgs_get_product_cats($order_id);

	//only continue if status is processing for deals 
	if ( ($product_cats['hasdeals'] AND 'processing' == $status) 
				OR ($product_cats['hasgoods'] AND 'completed' == $status) 
			) {

		$merchant_ids = hdgs_get_merchants_from_order($order_id);
  	$merchant_id = $merchant_ids[0];
		
		$sales_to_date = hdgs_get_sales_to_date($merchant_id);

		$total_value = get_user_meta($merchant_id, 'hdgs_led_options_hdgs_led_total_value', true);
		$safety_rate = get_option('hdgs_safety_rate');
		if (!$safety_rate) {
			$safety_rate = 80;
		}
		$safe_balance = $total_value*$safety_rate/100;

		if ($sales_to_date >= $safe_balance) {
			//send alert
			$merchant = get_user_by('id',$merchant_id);
			$profile_url = get_edit_user_link($merchant_id);
			$safety_now = ceil($sales_to_date*100/$total_value);
			$message = $merchant->user_login.' above Safety Level ('.$safety_now.'%)!';
			$_SESSION['hdgs_admin_notices'][] = array ('type' => 'warning',
																				'message' => $message,
																				);
			//send alert to admin
			$subject = 'Alert!! '.$message;
			$message .= PHP_EOL.PHP_EOL.'You can view their profile here: '.PHP_EOL;
			$message .= '<a href="'.$profile_url.'">'.$profile_url.'</a>';
			return hdgs_send_admin_alert($subject, $message);
		}
	}

	return false;
}



/**
 * send an email to admin
 *
 * @param string $subject
 * @param string $message
 * @return boolean, returns true on successful email alert sent
 */
function hdgs_send_admin_alert($subject, $message) {
	$admin_email = get_bloginfo('admin_email');

	$headers = 'From: Happy Deals Alertbot <alertbot@happydeals.ph>' . PHP_EOL.
							'Return-Path: alertbot@happydeals.ph'.PHP_EOL.
							'Reply-To: alertbot@happydeals.ph'.PHP_EOL;

	$headers .= "MIME-Version: 1.0\n";
	$headers .= "Content-Type: " . get_bloginfo('html_type') . "; charset=\"". get_bloginfo('charset') . "\"\n";
	$headers.= "X-Priority: 1\r\n";
	$mailtext = "<html><head><title>".$subject."</title></head><body>".$message."</body></html>";
	
	return wp_mail($admin_email, $subject, $mailtext, $headers);
}


/**
 * a list of the basic account info fields. add here if you want more
 *
 * @param none
 * @return array, list of account fields taken from options or adding if doesn't exist
 */
function hdgs_get_basic_account_fields(){
	$basic_account_fields = get_option('hdgs_basic_account_fields');
	if (!$basic_account_fields) {
		$basic_account_fields = array('company_name' 		=> 'Company Name', 
																	'legal_address' 	=> 'Company Legal Address', 
																	'contact_person' 	=> 'Contact Person', 
																	'contact_number' 	=> 'Contact No.', 
																	'contact_email' 	=> 'Email Address', 
																	'date_registered' => 'Date Registered', 
																	'merchant_tin' 		=> 'Merchant TIN No.',
																	);
		update_option( 'hdgs_basic_account_fields', $basic_account_fields );
	}
	return $basic_account_fields;
}

/**
 * a list of the basic charge fields. add here if you want more
 *
 * @param none
 * @return array, list of charge fields taken from options or adding if doesn't exist
 */
function hdgs_get_basic_charge_fields() {
	$basic_charge_fields = get_option('hdgs_basic_charge_fields');
	if (!$basic_charge_fields) {
		$basic_charge_fields = array('hd_commission' 		=> 'HD Commission',
																'transaction_fees' 	=> 'Transaction Fees',
																'handling_fee' 			=> 'Handling Fee',
																'shipping_fee' 			=> 'Shipping Fee',
																'penalties' 				=> 'Penalties',
																'credits' 					=> 'Credits',
																'led_package' 			=> 'Package 1 - LED',
															);
		update_option('hdgs_basic_charge_fields', $basic_charge_fields);
	}
	return $basic_charge_fields;
}

/**
 * get the HD commissions table, or create if it doesn't exist
 *
 * @param none
 * @return array, commissions table with values
 */
function hdgs_get_commissions_table() {
	$commissions_table = get_option('hdgs_get_commissions_table');

	//build the category list if it doesn't exist
	if (!$commissions_table OR isset($_GET['update_commissions_table'])) {
		$taxonomy     = 'product_cat';
	  $hierarchical = 0;      // 1 for yes, 0 for no  
	  $empty        = 0;

	  $args = array('taxonomy'    => $taxonomy,
				         'hierarchical' => $hierarchical,
				         'hide_empty'   => $empty,
				  			);
		$categories = get_categories( $args );		
		foreach ($categories as $cat) {
			if($cat->category_parent == 0) {
			  $args2 = array('taxonomy'    => $taxonomy,
						          'child_of'     => $cat->term_id,
						          'hierarchical' => $hierarchical,
						          'hide_empty'   => $empty,
										  );
			  $subcats = get_categories( $args2 );
			  
		  	foreach ($subcats as $subcat) {
					if($subcat->category_parent == $cat->term_id) {
				  	$args3 = array('taxonomy'    => $taxonomy,
								          'child_of'     => $subcat->term_id,
								          'hierarchical' => $hierarchical,
								          'hide_empty'   => $empty,
												  );
				  	$subsubcats = get_categories( $args3 );
					  
				    foreach($subsubcats as $subsubcat) {
				    	if($subsubcat->category_parent == $subcat->term_id AND !isset($commissions_table[$cat->term_id][$subcat->term_id][$subsubcat->term_id])) {
				    			$commissions_table[$cat->term_id][$subcat->term_id][$subsubcat->term_id] = 0;
				      }
				    }   
					}
			  }
			}       
		}

		update_option('hdgs_get_commissions_table',$commissions_table);
	}

	return $commissions_table;
}

/**
 * log time when order changed status
 *
 * @param int $order_id
 * @param string $from_status: original order status
 * @param string $to_status: new order status
 * @return array of statuses saved and timestamp
 */
add_action('woocommerce_order_status_changed', 'hdgs_log_order_status_change');
function hdgs_log_order_status_change($order_id, $from_status, $to_status) {
	if (!$to_status) return false;
  $order_status_change_log = get_post_meta($order_id, 'order_status_change_log', true);
  $now = current_time( 'timestamp', 0 );
  $order_status_change_log[] = array('from_status' => $from_status, 
  																		'to_status'		=> $to_status, 
  																		'timestamp' 	=> $now,
   																	);
   return update_post_meta($order_id, 'order_status_change_log',$order_status_change_log);
}

/**
 * get the last timestamp when order_id changed $from_status to $to_status
 *
 * @param int $order_id
 * @param string $to_status: new order status
 * @param string $from_status: original order status
 * @return int: timestamp of change from order_status_change_log meta
 */
function hdgs_get_status_change_timestamp($order_id, $to_status, $from_status = '') {
	$order_status_change_log = get_post_meta($order_id, 'order_status_change_log', true);

	//reverse sort to get the last status change
	if (is_array($order_status_change_log)) {
		krsort($order_status_change_log); 	
	} else {
		return 0;
	}
	

	//find first status change that matches by looping through
	foreach ($order_status_change_log as $row) {

		//if to_status matches...
		if ($row['to_status'] == $to_status) {

			//get the timestamp if the from status matches as well
			if ($from_status) {
				if ($from_status == $row['from_status']) {
					return $row['timestamp']; 
				}

			//or just return the timestamp of the first occurrence
			} else {
				return $row['timestamp'];
			}	

		}
	}

	return 0;
}

//if staff switched order to processing (paid), deactivate products with price that are over balance
/**
TODOs
1. must only trigger if manual processing change
2. must trigger for all users who own a product in that order
**/
add_action('woocommerce_order_status_changed', 'hdgs_check_if_overbalance');
function hdgs_check_if_overbalance($order_id, $from_status, $to_status) {
	if ($to_status == 'processing') {
		//add code for deactivating products here
	}
}

/**
 * translate order status from Woo version to HD version
 *
 * @param string $order_status
 * @param int $order_id
 * @return string: translated status
 */
function hdgs_translate_order_status($order_status, $order_id = '') {
	//can also be used with product_cat
	$product_cats = hdgs_get_product_cats($order_id);

	switch ($order_status) {
		
		case 'pending':
			return 'unpaid';
		break;
		
		case 'processing':
			if ($product_cats['hasdeals']) {
				return 'unredeemed';
			} else {
				return 'unfulfilled';
			}
		break;

		case 'completed':
			if ($product_cats['hasdeals']) {
				return 'redeemed';
			} else {
				return 'delivered';
			}
		break;
	}
	

	return $order_status;
}

/**
 * change order status to named status. only works for child orders
 *
 * @param string $order_status
 * @param int $order_id
 * @return boolean: true on successful update
 */
function hdgs_mark_as($order_status, $order_id) {
	global $allowed_merchant_statuses;
	$user_id = get_current_user_id();

	if (!isset($allowed_merchant_statuses[$order_status])) {
		return false;
	}

	$merchant_ids = hdgs_get_merchants_from_order($order_id);

	//if branch and order is for branch, add in allowable merchants list
	if (current_user_can('branch_account')) {
		$pickup_branch_id = get_post_meta($order_id, 'hdgs_pickup_branch', true);
		if ($pickup_branch_id == $user_id) {
			$merchant_ids[] = $user_id;
		}
	}
	
	if (!in_array($user_id, $merchant_ids)) {
		return false;
	}

	//change the status
	$order = new WC_Order( $order_id );
	return $order->update_status($order_status);
}

/**
 * get the hd commission based on commission table
 *
 * @param int $order_id
 * @return float $commission: commission amount for the order
 */
function hdgs_get_hd_commission($order_id) {
	$commission = 0;

	if ($order_id) {
		$order = wc_get_order( $order_id );
		$items = $order->get_items();

		foreach ( $items as $item) {
			$product_id = $item->get_product_id();
			$price = $item->get_subtotal();
			
			$commission_rate = 0;
			$commissions_table = hdgs_get_commissions_table();
			foreach ($commissions_table as $cat => $row) {
				foreach ($row as $subcat => $row2) {
					foreach ($row2 as $subsubcat => $rate) {
						//checks all categories.
						if (has_term($subsubcat, 'product_cat', $product_id)) {
							//we only get the highest rate among all matching categories
							$commission_rate = max($rate, $commission_rate);
						}	
					}
				}
			}
			
			$commission += $commission_rate*$price/100;
		}
	}

	return $commission;
}

/**alt way to get commission
function hdgs_get_hd_commission2($order_id) {
	
	$terms = array();
	$order = wc_get_order( $order_id );
	$line_items = $order->get_items();

	$commissions_table = hdgs_get_commissions_table();

	//get default commission
	$charge_fees = get_option('hdgs_get_basic_charge_fees');
	$commissions[] = $charge_fees['hd_commission'];

	foreach ( $line_items as $item_id => $item ) {
		$product_id = $item->get_product_id();
		$terms = get_the_terms( $product_id, 'product_cat' );
		$price = $item->get_subtotal();

		//get the parent

		foreach ($terms as $key => $term) {
			if (!$term->parent) {
				$parent = $term;
				unset($terms[$key]);
				break;
			} 
		}
		
		//get the child
		foreach ($terms as $key => $term) {
			if ($term->parent == $parent->term_id) {
				$child = $term;
				unset($terms[$key]);
				break;
			}
		}

		//get the grandchild
		$grandchild = array_shift($terms);

		$commissions_rates[] = $commissions_table[$parent->term_id][$child->term_id][$grandchild->term_id];



	}

	return max($commissions);
	
}

**/

/**
 * Get all approved WooCommerce order notes.
 *
 * @param  int|string $order_id The order ID.
 * @return array      $notes    The order notes, or an empty array if none.
 * @source https://kellenmace.com/get-woocommerce-order-notes/
 */
function hdgs_get_order_notes( $order_id ) {
	remove_filter( 'comments_clauses', array( 'WC_Comments', 'exclude_order_comments' ) );
	$comments = get_comments( array('post_id' => $order_id,
																	'orderby' => 'comment_ID',
																	'order'   => 'DESC',
																	'approve' => 'approve',
																	'type'    => 'order_note',
																) );
	add_filter( 'comments_clauses', array( 'WC_Comments', 'exclude_order_comments' ) );
	return $comments;
}

/**
 * translate timestamp to words
 *
 * @param float $date
 * @return string: the formatted version of the date
 */
function hdgs_date_to_words($date) {
	$now = current_time( 'timestamp', 0 );

	if ($date > strtotime('today', $now)) {
		return 'Today at '.date_i18n('g:i a', $date);
	} elseif ($date > strtotime('yesterday', $now)) {
		return 'Yesterday at '.date_i18n('g:i a', $date);
	} else {
		return date_i18n('M j, g:i a', $date);
	}
}

/**
 * translate duration into time measurement
 *
 * @param string $duration: the kind of time recurrence 
 * @return string: the time measurement for that recurrence
 */
function hdgs_duration_to_time($duration) {
	switch ($duration) {
    case 'Daily':
    	return 'day';
    break;
    case 'Weekly':
    	return 'week';
    break;
    case 'Monthly':
    	return 'month';
    break;
    case 'Yearly':
    	return 'year';
    break;
  }
}

/**
 * log changes made by this user to the corresponding order as comments
 * child order notes should also be logged in parent order
 * parent order notes should also be logged in all child orders
 * sibling orders cannot see each others notes though
 *
 * @param object $order: the WC Order object for the order
 * @param string $note: the note to save
 * @return none
 */
function hdgs_log_user_actions($order, $note) {
	$order_id = $order->get_id();
	$user_id = get_current_user_id();

	$comment_ID = hdgs_add_comment_to_order($order_id, $user_id, $note);
  //also add to personal timeline thread
  if ($comment_ID) {
    $timeline_meta = get_post_meta($order_id,'hdgs_timeline_meta',true);
    $timeline_meta[$comment_ID] = array('comment_author' => $user_id);
    update_post_meta($order_id,'hdgs_timeline_meta', $timeline_meta);
  } 
}

/**
 * add comment to order. a dupe of http://woocommerce.wp-a2z.org/oik_api/wc_orderadd_order_note/
 *
 * @param int $order_id
 * @return array: shipping info from the shipping table, false if there's none
 */
function hdgs_add_comment_to_order($order_id, $author_id, $note) {
	$user                 = get_user_by( 'id', $author_id );
  $comment_author       = $user->display_name;
  $comment_author_email = $user->user_email;

  $commentdata = apply_filters( 'woocommerce_new_order_note_data', 
			  												array('comment_post_ID'      => $order_id,
																		  'comment_author'       => $comment_author,
																		  'comment_author_email' => $comment_author_email,
																		  'comment_author_url'   => '',
																		  'comment_content'      => $note,
																		  'comment_agent'        => 'Happy Deals',
																		  'comment_type'         => 'order_note',
																		  'comment_parent'       => 0,
																		  'comment_approved'     => 1,
																		), 
			  												array('order_id'		  		=> $order_id, 
			  															'is_customer_note' 	=> false ) 
  														);
	$comment_id = wp_insert_comment( $commentdata );

	return $comment_id;
}


/**
 * get the right courier data for an order based on matching preferences vs shipping table
 *
 * @param int $order_id
 * @return array: shipping info from the shipping table, false if there's none
 */
function hdgs_get_courier_data($order_id) {

	//get order details we need
	$order = wc_get_order($order_id);
	$package_size = get_post_meta($order_id,'package_size',true);
	$total_weight = ceil(get_post_meta($order_id,'total_weight',true));

	//need parent order for some info
	$parent_id = wp_get_post_parent_id($order_id);
	if ($parent_id) {
		$parent_order = wc_get_order($parent_id);
		$address = strtolower($parent_order->get_formatted_shipping_address());
	} else {
		$address = strtolower($order->get_formatted_shipping_address());
	}

	//get the shipping table
	$shipping_table = get_option('hdgs_shipping_table');
		
	foreach ($shipping_table as $id => $row) {
		
		//is the row a match for the destination?
		if (strpos($address, strtolower($row['destination'])) === FALSE) {
			continue;
		}

		//is the package size a match?
		if (isset($row['package_size']) AND @strpos(strtolower($row['package_size']), $package_size) === FALSE) {
			continue;
		}
		
		//is there still an allocation?
		if ($row['allocation'] < 1) {
			continue;
		} 

		//is the weight within the limit (for packaging that doesn't have overweight charges)
		$total_weight = hdgs_get_total_weight($order_id);
		if (!$row['extra_charge'] AND $total_weight > $row['max_weight']) {
			continue;
		}

		return $row;
		
	}
	
		
	return false;

}

/**
 * get the shipping costs for a certain order
 *
 * @param int $order_id
 * @return float: shipping cost value in PHP
 */
function hdgs_get_shipping_cost($order_id) {

	$data = hdgs_get_courier_data($order_id);

	//this calculation is specific for own packaging package size. Formula: Base rate + (calc Ship weight minus max weight)*per KG Addition
	if ($data['extra_charge']) {
		
		$shipping_weight = hdgs_get_shipping_weight($order_id, $data['denominator']);
		$excess_weight = max($shipping_weight - $row['max_weight'],0);
		return $data['base_rate'] + ($excess_weight * $data['extra_charge']);

	//for packaging without overweight charges, it's a fixed fee
	} else {
		
		return $data['base_rate'];
	}	
}

/**
 * get the right shipping weight aka higher of total vs calculated volumetric
 *
 * @param int $order_id
 * @param int $denominator: a fixed courier-provided value
 * @return float: shipping weight for the order
 */
function hdgs_get_shipping_weight($order_id, $denominator = 3500) {
	$calc_ship_weight = hdgs_get_calc_ship_weight($order_id, $denominator);
	$total_weight = hdgs_get_total_weight($order_id);

	return max($calc_ship_weight, $total_weight);
}

/**
 * get calculated ship weight
 *
 * @param int $order_id
 * @param int $denominator: a fixed courier-provided value
 * @return float: the calculated shipping weight for the order based on volume
 */
function hdgs_get_calc_ship_weight($order_id, $denominator = 3500) {
	$order = wc_get_order($order_id);
	$line_items = $order->get_items();
  $calc_ship_weight = 0;
	$length = 0;
	$width = 0;
	$height = 0;
	$dimensions = array();

  //get dimensions for order (L x W x H)
  $package_size = get_post_meta($order_id,'package_size',true);
  if ('own' == $package_size) {
  	$dimensions = get_post_meta($order_id,'dimensions',true);
  	if ($dimensions) {
	    $length = $dimensions['length'];
	    $width = $dimensions['width'];
	    $height = $dimensions['height'];
	  }    
  } 

  foreach ( $line_items as $item_id => $item ) {
    $product = $item->get_product();
    
    //1. get qty. minimum quantity is 1 piece
    $qty = max(1,$item->get_quantity());

    //2. add dimensions
    //don't overwrite existing dimensions!
    if (!$dimensions) {
      $length += $product->get_length()*$qty;
      $width += $product->get_width()*$qty;
      $height += $product->get_height()*$qty;  
    }
  }

  //3. calculate vol weight
  $calc_ship_weight = ceil($length*$width*$height/$denominator);

  return $calc_ship_weight;
}

/**
 * get total weight of an order
 *
 * @param int $order_id
 * @return float: the total shipping weight for the order
 */
function hdgs_get_total_weight($order_id) {

	//return the total weight if saved already
	$total_weight = get_post_meta($order_id,'total_weight',true);
  if ($total_weight) {
    return ceil($total_weight);
  }

	$order = wc_get_order($order_id);
	$line_items = $order->get_items();
  $total_weight = 0; 

  foreach ( $line_items as $item_id => $item ) {
    
    
    //1. get qty. minimum quantity is 1 piece
    $qty = max(1,$item->get_quantity());

    //2. get actual total weight of product
    $product = $item->get_product();
    $total_weight += $product->get_weight()*$qty;  
  }

  return ceil($total_weight);
}

/**
 * reduce the courier allocation once the order is ready to ship
 * triggers on ready-to-ship status
 *
 * @param int $order_id
 * @return boolean: true on successful update
 */
add_action( 'woocommerce_order_status_ready-to-ship', 'hdgs_reduce_allocation', 10, 1);
function hdgs_reduce_allocation($order_id) {
	$data = hdgs_get_courier_data($order_id);
	$shipping_table = get_option('hdgs_shipping_table');
	$priority = $data['priority'];

	$shipping_table[$priority]['allocation'] = $data['allocation'] - 1;

	return update_option('hdgs_shipping_table');
}

/**
 * get the courier allocation 
 *
 * @param string $courier: internal name of courier
 * @return int: the allocation count for that courier
 */
function hdgs_get_allocation($courier) {

	global $courier_list;
	$allocations = get_option( 'hdgs_courier_allocation');
	$last_access = $allocations['last_access'];
	$now = current_time( 'timestamp', 0 );
	$order_ok = false;
	$allowed_per_day = 100; //allowed transactions per day

	//if last access was yesterday (-1 sec from today), reset allocations
	if ($last_access < strtotime('today',$now)) {
		foreach ($courier_list as $courier) {
			$allocations['alloc'][$courier] = $allowed_per_day;
		}
	}
	$allocations['last_access'] = $now;

	$current_alloc = $allocations['alloc'][$courier];
	if ($current_alloc > 0) {
		--$allocations['alloc'][$courier];
		$order_ok = true;
	}

	//update last_access and allocs
	update_option( 'hdgs_courier_allocation', $allocations);

	return $order_ok;
}

/**
 * generates the shipping label PDF via fpdf. Can be printed or downloaded from that screen
 * works with arrays of ids as well as individual ids 
 *
 * @param array $order_ids
 * @return none: displays the PDF label on exit
 */
function hdgs_print_shipping_label($order_ids) {
	$user_id = get_current_user_id();

  if (!is_array($order_ids)) {
  	$order_ids = array($order_ids);
  }

  if (!array_filter($order_ids)) {
  	wp_die( __( 'You are not allowed to use this feature.', 'hdgs_text_domain' ) );
  }

  //initialize pdf. uses http://www.fpdf.org/
	require_once plugin_dir_path( __FILE__ ) .'fpdf/fpdf.php';
	$pdf = new FPDF('P','mm','A4');
	$pdf->SetFont('Arial','',12);
	$pdf->AddPage();
	ob_end_clean(); //remove any HTML lying around

  foreach ($order_ids as $order_id) {

	  $order = wc_get_order( $order_id );

	  $merchant_ids = hdgs_get_merchants_from_order($order_id);
		$merchant_id = $merchant_ids[0];
		$branch_id = get_post_meta($order_id, 'hdgs_pickup_branch', true);
		if ($user_id != $merchant_id AND $user_id != $branch_id AND !is_super_admin()) {
			wp_die( __( 'You are not allowed to use this feature.', 'hdgs_text_domain' ) );
		} elseif ($merchant_id == $user_id) {
			$branch_id = $merchant_id;
		} 

		//get company name and address
	  $hdgs_account_info = get_user_meta( $branch_id, 'hdgs_account_info', true );
	  if ($hdgs_account_info) {
	    $shipper_name = $hdgs_account_info['company_name'];
			$shipper_address = $hdgs_account_info['legal_address'];
	  }

	  //get customer name and address
	  $customer_name = $order->get_shipping_first_name().' '.$order->get_shipping_last_name();
	  $customer_address = get_post_meta($order_id,'new_shipping_address', true);
	  if (!$customer_address) {
	  	$customer_address = $order->get_formatted_shipping_address(); 	
	  }
	  $customer_address = str_replace('<br/>', PHP_EOL, $customer_address);//formatting for PDF

	  $customer_phone = get_post_meta($order_id,'new_phone', true);
	  if (!$customer_phone) {
	  	$customer_phone = $order->get_billing_phone();
	  }

	  $customer_address .= PHP_EOL.$customer_phone;

	  //get other details
	  $tracking_number = get_post_meta($order_id,'_wcst_order_trackno',true);
	  $weight = number_format(hdgs_get_shipping_weight($order_id),1); //in kg

	  $parent_id = wp_get_post_parent_id($order_id);
	  if ($parent_id) {
	  	$order_id = $parent_id;
	  }

		//print the details in a new page
		
		$pdf->Cell(0,5,'ORDER #'.$order_id,0,1,'R');
		$pdf->Cell(10,5,'FROM:',0,0);
		$pdf->Cell(0,5,'WEIGHT: '.$weight.'kg',0,1,'R');
		$pdf->Cell(0,5,$shipper_name,0,0);
		$pdf->Cell(0,5,'TRACKING NUMBER: '.$tracking_number,0,1,'R');
		$pdf->MultiCell(0,5,$shipper_address,0);
		$pdf->Ln(20);
		$pdf->SetX(90);
		$pdf->MultiCell(0,5,'TO: '.$customer_address,0);
		$pdf->Ln(20);
		$pdf->Cell(0,1,'','T');
		$pdf->Ln(20);
	}

	//display it
	$pdf->Output();
	exit;
}

/**
 * compute next payout date for user
 *
 * @param float $date_completed: date the last payout was completed
 * @return float $payout_date: the next payout date
 */
function hdgs_get_next_payout($date_completed) {

  $recur_options = get_option('hdgs_payout_pattern');

  //get the string to feed
  $duration_blurb = hdgs_duration_to_time($recur_options['duration']).'s';
  $date_string = '+ '. $recur_options['every'].' '.$duration_blurb;

	$payout_date = strtotime($date_string, $date_completed);

  return $payout_date;

}

/**
 * get the total payout for the order
 *
 * @param int $order_id
 * @return float $total_payout: the net payout amount, inclusive of charges
 */
function hdgs_calculate_total_payout($order_id) {

	$order = wc_get_order( $order_id );
	$user_id = hdgs_get_merchants_from_order($order_id)[0];

	$gross_sales = $order->get_subtotal() - $order->get_total_tax();
  $discount = 0 - $order->get_total_discount();
  $cust_shipping = $order->get_shipping_total();
  $net_sales = $gross_sales + $discount + $cust_shipping;

  $charge_fees = get_option('hdgs_get_basic_charge_fees');
  $charge_status = get_user_meta( $user_id, 'hdgs_charge_status', true );

  //get regular charges, like shipping fees
  $charges = array();
  if (is_array($charge_status)) {
  	foreach ($charge_status as $name => $status) {
	    if ('no' == $status) {
	      continue;
	    }
	    if ('shipping_fee' == $name){
	      if (hdgs_get_total_weight($order_id)) {

	      	$data = hdgs_get_courier_data($order_id);

          //normal shipping is shouldered by seller. for own packaging, half is shouldered by the buyer.
          $shipping_fee = hdgs_get_shipping_cost($order_id);
          if ($data['extra_charge']) {
            $shipping_fee = $shipping_fee/2;
          }

          $charges[$name] = $shipping_fee;
	      } else {
	        continue;
	      }
	    } elseif ('hd_commission' == $name) {
        $charges[$name] = hdgs_get_hd_commission($order_id);

	    } else {

        if (!isset($charge_fees[$name])) {
          $charge_fees[$name] = 0;
        }
        $charges[$name] = $charge_fees[$name]*$net_sales/100;  
      }

      $charges[$name] = 0 - abs($charges[$name]);
	  }
  }  

  //add extra charges
  $new_charges = get_post_meta($order_id,'hdgs_new_charges', true);
  if (!$new_charges) {
    $new_charges = array();
  }

  $total_payout = $net_sales + array_sum($charges) + array_sum($new_charges);

  return $total_payout;
}

/**
 * get current page url. alternative to screen variable since that breaks site
 *
 * @param none
 * @return string: current url of the current page
 */
function hdgs_get_current_url(){
	$current_rel_uri = add_query_arg( NULL, NULL );

	return $current_rel_uri;
}

/**
 * log the views of a product or variation, assigns it to a $key
 *
 * @param int $post_id: can be product_id, variation_id or any post_type
 * @param string $key
 * @return boolean: true on success
 */
function hdgs_log_product_view($post_id, $key) {
	$now = current_time( 'timestamp', 0 );
	$stats = get_post_meta( $post_id, 'hdgs_product_stats', true );
	$stats[$key][] = $now;
	return update_post_meta( $post_id, 'hdgs_product_stats', $stats );
}

/**
 * log the pageviews of a product. assigns it to a $key
 *
 * @param none
 * @return boolean: true on success
 */
add_action( 'woocommerce_after_single_product', 'hdgs_log_pageview', 100 );
function hdgs_log_pageview() {
	$product_id = get_the_ID();
	return hdgs_log_product_view($product_id, 'page');
}

/**
 * log whenever product is added to cart
 *
 * @param array $cart_item_data
 * @param int $product_id
 * @param int $variation_id
 * @return array $cart_item_data: passthrough since it's a filter
 */
add_filter('woocommerce_add_cart_item_data', 'hdgs_log_add_to_cart', 100);
function hdgs_log_add_to_cart($cart_item_data, $product_id, $variation_id) {
  hdgs_log_product_view($product_id, 'cart');
  if ($variation_id) {
  	hdgs_log_product_view($variation_id, 'cart');	
  }

  return $cart_item_data;
}

/**
 * log whenever product is checked out
 *
 * @param none
 * @return none
 */
add_action('woocommerce_after_checkout_form', 'hdgs_log_checkout', 100);
function hdgs_log_checkout() {
	$items = WC()->cart->get_cart();
	foreach( $items as $cart_item ){
    $product_id = $cart_item['product_id'];
	  hdgs_log_product_view($product_id, 'checkout');

	  //also log variation bought
	  $variation_id = $cart_item['variation_id'];
	  if ($variation_id) {
	   hdgs_log_product_view($variation_id, 'checkout');	
	  }
  }
}


/**
 * check if date falls within the range required
 * this uses dates relative to the current timestamp
 *
 * @param string $datestring: duration to check. 
 * @return float $duration: array of start_date and end_date of duration
 */
function hdgs_date_happened($datestring) {
	$duration = array();
	$firstday = 'Sunday';

	$now = current_time( 'timestamp', 0 );
	$firstday = strtotime('last '.$firstday.' midnight', $now);

	//return custom values if present
	if (isset($_GET['start_date'])) {
		$start_date = strtotime($_GET['start_date']);
		if (isset($_GET['end_date'])) {
			$end_date = strtotime($_GET['end_date']);
		}
		if (!$end_date) {
			$end_date = $now;
		}
		if ($start_date < $end_date) {
			$duration['start'] = $start_date;
			$duration['end'] = $end_date;
			return $duration;
		}
	}

	switch ($datestring) {
		case 'Last Week':
			$duration['start'] = strtotime('-7 days', $firstday);
			$duration['end'] = strtotime('+7 days -1 second', $duration['start']); 
		break;

		case 'This Week':
			$duration['start'] = $firstday;
			$duration['end'] = strtotime('+7 days -1 second', $duration['start']);  
		break;

		case 'Last Month':
			$duration['start'] = strtotime('first day of this month midnight',strtotime('-1 months'));
			$duration['end'] = strtotime('first day of next month midnight -1 second',strtotime('-1 months'));
		break;

		case 'This Month':
			$duration['start'] = strtotime('first day of this month midnight',$now);
			$duration['end'] = strtotime('first day of next month midnight -1 second',$now);
		break;
		
	}

	return $duration;
}


/**
 * draws fancy charts
 *
 * @param string $chart_name
 * @param string $labels: labels for the axes
 * @param string $data: data to plot
 * @param string $colors: colors to use for the plot
 * @return none
 */
function hdgs_show_chart ($chart_name, $labels, $data, $colors = array()) {

	//set line color
	$bordercolor = array();
	$chart_data = array();

	if (!is_array($colors)) {
		//last in first out, so listed in reverse
		$colors = array('palegreen','forestgreen'); 	
	}
	$colors = array_reverse($colors); 		

	//encode data and labels to string
	foreach($data as $data_set => $data_points) {
		$chart_data[$data_set] = json_encode(array_values($data_points));
		$bordercolor[$data_set] = array_pop($colors);
	}
	$chart_labels = json_encode(array_values($labels));
	

	//draw the chart. PS: class is same as chart name
	?>
		<canvas id="<?php echo $chart_name; ?>" class="<?php echo $chart_name; ?>"></canvas>
		<script>
		var ctx = document.getElementById("<?php echo $chart_name; ?>").getContext('2d');
		var myChart = new Chart(ctx, {
		    type: 'line',
		    data: {
		        labels: <?php echo $chart_labels; ?>,
		        datasets: 
		        [
		        <?php foreach($chart_data as $data_set => $data_points): ?>
			        {
			        	label: '<?php echo $data_set; ?>',
			          data: <?php echo $data_points; ?>,
			          borderColor: '<?php echo $bordercolor[$data_set]; ?>',
			          fill: false,
			          /*lineTension: 0.1*/
			        },
			      <?php endforeach; ?>
		        ]
		    },
		    options: {
		      scales: {
	          yAxes: 
	          [
	          	{
	            	ticks: {
	                beginAtZero:true
	              }
	            }
	          ]
		      }
		    }
		});
		</script>

	<?php
}


/**
 * get delivery details for order via API
 *
 * @param int $order_id
 * @return array $data: the delivery data for that courier
 */
function hdgs_get_delivery_data($order_id) {

	$courier_data = hdgs_get_courier_data($order_id);
	$courier_name = strtolower($courier_data['courier']);
	$data = array('date' 		=>	0, 
								'remarks'	=>	'', 
								'status'	=>	'', 
								);

	switch ($courier_name) {
		case 'dpx':
			$data = hdgs_get_dpx_data($order_id);

		break;

		case 'lmbm':
			$data = hdgs_get_lmbm_data($order_id);
		break;

		case 'quad x':
		case 'quadx':
		case 'quad_x':
			$data = hdgs_get_quadx_data($order_id);
		break;
		
	}

	return $data;

}

/**
 * get data from DPX api to show current status of delivery
 *
 * @param int $order_id
 * @return array $data: includes the delivery and pick-up statuses of the order
 */
function hdgs_get_dpx_data($order_id) {
	if (!$order_id) {
		return false;
	}
	$tracking_number = get_post_meta($order_id,'_wcst_order_trackno',true);

	$data = array('date' 		=>	0, 
								'remarks'	=>	'', 
								'status'	=>	'', 
							);
	$status = '';

	//get the data via wp curl
	$url = "http://dpxadmin.com/TrackWaybillAction?waybillNo=".$tracking_number;
	$html = wp_remote_get($url);
	$api_data = json_decode($html['body']);

	//shift to first element, which is $data['WAYBILLINFO']
	$api_data = current($api_data);  

	//build the status array
	foreach ($api_data as $event) {
		$event_date = strtotime($event->date);
		$event_status = strtolower($event->status);	

		//replace if next element is more recent
		if ($event_date > $data['date']) {
			$data['date'] = $event_date;	
			$data['remarks'] = $event->remarks;
			if ($event_status) {
				$status = $event_status;	
			}
		}

		//log pickup date
		if(strpos($event_status, 'accept') !== FALSE) {
			$pickup_date = get_post_meta($order_id,'pickup_date',true);
			if (!$pickup_date) {
				update_post_meta($order_id,'pickup_date', $event_date);
			}
		}
	}

	//translate the status

	//pickup status
	//Picked up Status	From Courier API: Pending; Successful; Unsuccessful
	//Delivery Status	3 options (Delivered, RTS, Pending). This should be linked with the API of the couriers.
	if(strpos($status, 'fail') !== FALSE OR 
			strpos($status, 'return to sender') !== FALSE ) {
		$data['pickup_status'] = 'unsuccessful';
		$data['delivery_status'] = 'pending';
	} elseif(strpos($status, 'success') !== FALSE) {
		$data['pickup_status'] = 'successful';
		$data['delivery_status'] = 'delivered';
	} elseif ($status) {
		$data['pickup_status'] = 'pending';
		$data['delivery_status'] = 'ready-to-ship';
	} else {
		$data['pickup_status'] = 'pending';
		$data['delivery_status'] = 'pending';
	}

	return $data;

}

/**
 * get data from LMBM api to show current status of delivery
 *
 * @param int $order_id
 * @return array $data: includes the delivery and pick-up statuses of the order
 */
function hdgs_get_lmbm_data($order_id) {
	if (!$order_id) {
		return false;
	}
	$tracking_number = get_post_meta($order_id,'_wcst_order_trackno',true);

	$status = '';
	$data = array('date' 		=>	0, 
								'remarks'	=>	'', 
								'status'	=>	'', 
							);

	//get the data via wp curl
	$url = "http://express.api.easydeal.ph/api/c8b23f6e46a96998/waybill-history?waybillNumber=".$tracking_number;
	$html = wp_remote_get($url);
	if (!is_wp_error($html)) {
		$api_data = json_decode($html['body']);	
		$api_data = $api_data->data;
	}

	//build the status array
	if (isset($api_data)) {
		foreach ($api_data as $event) {
			$event_date = strtotime($event->Transaction_Date);
			$event_status = strtolower($event->Activity);

			//replace if next element is more recent
			if ($event_date > $data['date']) {
				$data['date'] = $event_date;	
				$data['remarks'] = $event->Remarks;
				if ($event_status) {
					$status = $event_status;
				}
			}

			//log pickup date
			if(strpos($event_status, 'pickup') !== FALSE) {
				$pickup_date = get_post_meta($order_id,'pickup_date',true);
				if (!$pickup_date) {
					update_post_meta($order_id,'pickup_date', $event_date);
				}
			}
		}
	}

	//translate the status
	//Picked up Status	From Courier API: Pending; Successful; Unsuccessful
	//Delivery Status	3 options (Delivered, RTS, Pending). This should be linked with the API of the couriers.
	if(strpos($status, 'success') !== FALSE) {
		$data['pickup_status'] = 'successful';
		$data['delivery_status'] = 'delivered';
	} elseif(strpos($status, 'fail') !== FALSE) {
		$data['pickup_status'] = 'unsuccessful';
		$data['delivery_status'] = 'pending';
	} elseif(strpos($status, 'pickup') !== FALSE) {
		$data['pickup_status'] = 'successful';
		$data['delivery_status'] = 'ready-to-ship';
	} else {
		$data['pickup_status'] = 'pending';
		$data['delivery_status'] = 'pending';
	}

	return $data;
}

/**
 * get data from Quad X api to show current status of delivery
 *
 * @param int $order_id
 * @return array $data: includes the delivery and pick-up statuses of the order
 */
function hdgs_get_quadx_data($order_id) {
	if (!$order_id) {
		return false;
	}
	$tracking_number = get_post_meta($order_id,'_wcst_order_trackno',true);

	$status = '';
	$args['httpversion'] = '1.1';
	$data = array('date' 		=>	0, 
								'remarks'	=>	'', 
								'status'	=>	'', 
							);

	//get the data via wp curl
	$url = 'https://api.lbcx.ph/v1/orders/'.$tracking_number;
	$now = current_time( 'timestamp', 0 );
	
	//get api credentials
	$api_credentials = get_option('hdgs_api_credentials');
	$api_key = $api_credentials['quadx']['key'];
	$secret = $api_credentials['quadx']['secret'];

	//build the authorization header
	//header first
	$header = array('alg' => 'HS256',  
									'typ' => 'JWT', 
								);
	$header = base64_encode(json_encode($header));
	//then payload
	$iat = $now;
	$jti = wp_create_nonce('quadx_api-'.$now);
	$sub = $api_key;
	$payload = array('iat'	=> $iat, 
									'jti'		=> $jti, 
									'sub'		=> $sub, 
									);
	$payload = base64_encode(json_encode($payload));
	//then sig
	$signature = hash_hmac('sha256', $header.'.'.$payload, $secret, true);
	$signature = base64_encode($signature);
	//put it together
	$token = $header.'.'.$payload.'.'.$signature;
	$args['headers'] = 'Authorization: Bearer '.$token;

	//get the data
	$html = wp_remote_get($url, $args);
	if (isset($html['body'])) {
		$api_data = json_decode($html['body']);	
	}

	//shift to event element
	if (isset($api_data->events)) {
		$api_data = $api_data->events;	

		//build the status array
		foreach ($api_data as $event) {
			$event_date = strtotime($event->created_at);
			$event_status = strtolower($event->status);

			//replace if next element is more recent
			if ($event_date > $data['date']) {
				$data['date'] = $event_date;	
				$data['remarks'] = $event->remarks;
				if ($event_status) {
					$status = $event_status;	
				}
			}

			//log pickup date
			if(strpos($event_status, 'picked_up') !== FALSE) {
				$pickup_date = get_post_meta($order_id,'pickup_date',true);
				if (!$pickup_date) {
					update_post_meta($order_id,'pickup_date', $event_date);
				}
			}
		}	
	}

	//translate the status
	//Picked up Status	From Courier API: Pending; Successful; Unsuccessful
	//Delivery Status	3 options (Delivered, RTS, Pending). This should be linked with the API of the couriers.
	if(strpos($status, 'delivered') !== FALSE) {
		$data['pickup_status'] = 'successful';
		$data['delivery_status'] = 'delivered';
	} elseif(strpos($status, 'fail') !== FALSE) {
		$data['pickup_status'] = 'unsuccessful';
		$data['delivery_status'] = 'pending';
	} elseif($status) {
		$data['pickup_status'] = 'successful';
		$data['delivery_status'] = 'ready-to-ship';
	} else {
		$data['pickup_status'] = 'pending';
		$data['delivery_status'] = 'pending';
	}

	return $data;

}

