<?php 

class GFS_Admin {
    public static function init() {
        
        add_action('admin_menu', array(__CLASS__, 'admin_menu'));
        add_action("admin_init", array(__CLASS__, "display_theme_panel_fields"));

        
    }

    public static function admin_menu() {
        add_options_page( __('GFS Settings', 'gfs'), __('GFS Settings', 'gfs'), 'manage_options', 'gfs-settings', array(__CLASS__, 'render_settings_page') );
    }

    public static function render_settings_page() {
        include('options-html.php');
    }

    public static function render_gfs_no_of_reviews() {
        ?>
    	<input type="text" name="gfs_no_of_reviews" id="gfs_no_of_reviews" value="<?php echo get_option('gfs_no_of_reviews', 3); ?>"/>
        <?php
    }

    public static function render_gfs_api_key() {
        ?>
    	<input type="text" name="gfs_api_key" id="gfs_api_key" value="<?php echo get_option('gfs_api_key'); ?>" class="regular-text"/>
        <?php
    }

    public static function render_gfs_business_id() {
        ?>
    	<input type="text" name="gfs_business_id" id="gfs_business_id" value="<?php echo get_option('gfs_business_id'); ?>" class="regular-text"/>
        <?php
    }

    public static function render_gfs_client_id() {
        ?>
    	<input type="text" name="gfs_client_id" id="gfs_client_id" value="<?php echo get_option('gfs_client_id'); ?>" class="regular-text"/>
        <?php
    }

    public static function render_gfs_view_more_review_link() {
        ?>
    	<input type="text" name="gfs_view_more_review_link" id="gfs_view_more_review_link" value="<?php echo get_option('gfs_view_more_review_link'); ?>" class="regular-text"/>
        <?php
    }

    public static function render_gfs_minimum_rating() {
        ?>
    	<input type="text" name="gfs_minimum_rating" id="gfs_minimum_rating" value="<?php echo get_option('gfs_minimum_rating'); ?>"/>
        <?php
    }


    public static function display_theme_panel_fields() {

        add_settings_section("section", "All Settings", null, "gfs-options");
        add_settings_field("gfs_no_of_reviews", __("# of reviews", "gfs"), array(__CLASS__, "render_gfs_no_of_reviews"), "gfs-options", "section");
        add_settings_field("gfs_api_key", __("API Key", "gfs"), array(__CLASS__, "render_gfs_api_key"), "gfs-options", "section");
        add_settings_field("gfs_business_id", __("Business ID", "gfs"), array(__CLASS__, "render_gfs_business_id"), "gfs-options", "section");
        add_settings_field("gfs_client_id", __("Client ID", "gfs"), array(__CLASS__, "render_gfs_client_id"), "gfs-options", "section");
        add_settings_field("gfs_view_more_review_link", __("View more link", "gfs"), array(__CLASS__, "render_gfs_view_more_review_link"), "gfs-options", "section");
        add_settings_field("gfs_minimum_rating", __("Minimum rating", "gfs"), array(__CLASS__, "render_gfs_minimum_rating"), "gfs-options", "section");

        register_setting("section", "gfs_no_of_reviews");
        register_setting("section", "gfs_api_key");
        register_setting("section", "gfs_business_id");
        register_setting("section", "gfs_client_id");
        register_setting("section", "gfs_view_more_review_text");
        register_setting("section", "gfs_minimum_rating");
    }

}

GFS_Admin::init();