<?php 

class GetFiveStarsSimpleApi {

    const URL = 'https://getfivestars.com/api';
    const TIMEOUT = 30; // 30 seconds timeout

    /**
     * Init
     * 
     * @param string $clientId
     * @param string $privateKey
     */

    function __construct($clientId, $privateKey) {
        $this->clientId = $clientId;
        $this->privateKey = $privateKey;
    }

    /**
     * Do HTTP API request
     * 
     * @param string $resource
     * @param array[string] $request
     * 
     * @return string JSON response
     */
    function doRequest($resource, $request) {
        $request['clientId'] = $this->clientId;
        $request['hash'] = $this->signRequest($request, $this->privateKey);
        return $this->doJsonPost($resource, $request);
    }

    /**
     * JSON POST HTTP request
     * 
     * @param string $resource
     * @param array[string] $request
     * 
     * @return string JSON response
     */
    protected function doJsonPost($resource, $request) {
        $ch = curl_init(self::URL . $resource);

        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => self::TIMEOUT,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($request)
        ));

        return curl_exec($ch);
    }

    /**
     * Sign request with private key
     * 
     * @param array[string] $request
     * @param string $privateKey
     * 
     * @return string Hash sign
     */
    protected function signRequest($request, $privateKey) {
        ksort($request);

        $control = $privateKey;
        foreach ($request as $key => $value) {
            $control .= $key . $value;
        }

        return hash('sha256', $control);
    }

}