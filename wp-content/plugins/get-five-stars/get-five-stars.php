<?php
/*
Plugin name: Get Five Stars
Plugin URI: https://www.getfivestars.com/
Author: sewpafly
Author URI: https://www.getfivestars.com/
Version: 1.0
Description: The GetFiveStars platform helps you automate the customer feedback process to capture your Net Promoter Score®, testimonials and online reviews.  We help you reach out to your customers, ask them about their experience, learn from their responses and market your positive feedback and reviews across the web.
*/


$plugin = plugin_basename( __FILE__ );
add_filter( "plugin_action_links_$plugin", 'gfs_plugin_add_settings_link' );


function gfs_plugin_add_settings_link( $links ) {
    $settings_link = '<a href="options-general.php?page=gfs-settings">' . __( 'Settings' ) . '</a>';
    array_push( $links, $settings_link );
    return $links;
}


function gfs_wp_enqueue_scripts() {
    wp_register_style( 'gfs-style', plugins_url( 'assets/css/style.css', __FILE__ ), array());
}

add_action( 'wp_enqueue_scripts', 'gfs_wp_enqueue_scripts' );



if (is_admin()) {
    include 'admin/admin.php';
} else {
    require_once 'lib/api.php';
}

function gfs_render_reviews($atts) {
    $atts = shortcode_atts( array(
        'no_of_reviews' => get_option('gfs_no_of_reviews', 3),
        'tag' => '',
        'order' => 'latest'
    ), $atts, 'bartag' );

    $privateKey = get_option('gfs_api_key');
    $clientId = get_option('gfs_client_id');



    $api = new GetFiveStarsSimpleApi($clientId, $privateKey);
    $page = 1;
    $response = json_decode($api->doRequest('/feedbacks/get', array('page' => $page)));


    $review_link = get_option('gfs_view_more_review_link');
    $no_of_reviews = $atts['no_of_reviews'];
    $sum_of_rating = 0;
    $agg_of_rating = 0;

    if ($no_of_reviews > $response->{"count"} ) {
        $no_of_reviews = $response->{"count"};
    }

    for ($i = 1; $i <= $response->{"count"}; $i++) {
        $sum_of_rating = $sum_of_rating + $response->{"rating$i"};
    }

    $agg_of_rating = $sum_of_rating / $response->{"count"};

    $outHtml = $response->{"widgetHTML"};
    $outHtml .= '<div class="gfs-container" itemscope="" itemtype="https://schema.org/LocalBusiness">';
    $outHtml .= '
    <div class="gfs-hide" itemprop="aggregateRating" itemscope="" itemtype="https://schema.org/AggregateRating"> 
        <span itemprop="reviewCount">'. $response->{"count"} .'</span>
        <span itemprop="ratingValue">'. $agg_of_rating .'</span> 
    </div>';
    $outHtml .= gfs_business_info();

    $outHtml .= '<svg id="stars" style="display: none;" version="1.1"> <symbol id="stars-full-star" viewBox="0 0 102 18"><path d="M9.5 14.25l-5.584 2.936 1.066-6.218L.465 6.564l6.243-.907L9.5 0l2.792 5.657 6.243.907-4.517 4.404 1.066 6.218"/></symbol> <symbol id="stars-half-star" viewBox="0 0 102 18"><path d="M9.5 14.25l-5.584 2.936 1.066-6.218L.465 6.564l6.243-.907L9.5 0l2.792" fill="#e9ba26"/></symbol> <symbol id="stars-all-star" viewBox="0 0 102 18"><use xlink:href="#stars-full-star"/><use xlink:href="#stars-full-star" transform="translate(21)"/><use xlink:href="#stars-full-star" transform="translate(42)"/><use xlink:href="#stars-full-star" transform="translate(63)"/><use xlink:href="#stars-full-star" transform="translate(84)"/></symbol> <symbol id="stars-0-0-star" viewBox="0 0 102 18"><use xlink:href="#stars-all-star" fill="#9b9b9b"/></symbol> <symbol id="stars-0-5-star" viewBox="0 0 102 18"><use xlink:href="#stars-0-0-star"/><use xlink:href="#stars-half-star"/></symbol> <symbol id="stars-1-0-star" viewBox="0 0 102 18"><use xlink:href="#stars-0-0-star"/><use xlink:href="#stars-full-star" fill="#e9ba26"/></symbol> <symbol id="stars-1-5-star" viewBox="0 0 102 18"><use xlink:href="#stars-1-0-star"/><use xlink:href="#stars-half-star" transform="translate(21)"/></symbol><symbol id="stars-2-0-star" viewBox="0 0 102 18"><use xlink:href="#stars-1-0-star"/><use xlink:href="#stars-full-star" fill="#e9ba26" transform="translate(21)"/></symbol><symbol id="stars-2-5-star" viewBox="0 0 102 18"><use xlink:href="#stars-2-0-star"/><use xlink:href="#stars-half-star" transform="translate(42)"/></symbol><symbol id="stars-3-0-star" viewBox="0 0 102 18"><use xlink:href="#stars-2-0-star"/><use xlink:href="#stars-full-star" fill="#e9ba26" transform="translate(42)"/></symbol><symbol id="stars-3-5-star" viewBox="0 0 102 18"><use xlink:href="#stars-3-0-star"/><use xlink:href="#stars-half-star" transform="translate(63)"/></symbol><symbol id="stars-4-0-star" viewBox="0 0 102 18"><use xlink:href="#stars-3-0-star"/><use xlink:href="#stars-full-star" fill="#e9ba26" transform="translate(63)"/></symbol><symbol id="stars-4-5-star" viewBox="0 0 102 18"><use xlink:href="#stars-4-0-star"/><use xlink:href="#stars-half-star" transform="translate(84)"/></symbol><symbol id="stars-5-0-star" viewBox="0 0 102 18"><use xlink:href="#stars-all-star" fill="#e9ba26"/></symbol></svg>';
    $outHtml .= '<div class="gfs-row">';
    for ($i = 1; $i <= $no_of_reviews; $i++) {
        $outHtml .= '<div class="gfs-col gfs-item" itemprop="review" itemscope="" itemtype="https://schema.org/Review">';

        $outHtml .= '<div class="gfs-rating" itemprop="reviewRating" itemscope="" itemtype="https://schema.org/Rating">';
        $outHtml .= '<svg class="gfs-stars"><use xlink:href="#stars-'. $response->{"rating$i"}  .'-0-star"></svg>';
        $outHtml .= '<span itemprop="ratingValue">'. $response->{"rating$i"}  .' out of <span itemprop="bestRating">5</span> stars</span>';
        $outHtml .= '<meta itemprop="worstRating" content="1">';
        $outHtml .= '</div>';
        $outHtml .= '<span class="gfs-author">';
        $outHtml .= '<span itemprop="author" itemscope="" itemtype="https://schema.org/Person">';
        $outHtml .= '<span itemprop="name">'. $response->{"authorName$i"} .'</span>';
        $outHtml .= '</span>';
        $outHtml .= ' - <abbr title="'.  date('Y-m-d', strtotime($response->{"dateOfReview$i"})) .'">'. date('F j, Y', strtotime($response->{"dateOfReview$i"})) .'</abbr>';
        $outHtml .= '<meta itemprop="datePublished" content="'.  date('Y-m-d', strtotime($response->{"dateOfReview$i"})) .'">';
        $outHtml .= '</span>';

        $outHtml .= '<p itemprop="reviewBody">'. $response->{"body$i"} .'</p>';
        $outHtml .= '</div>';
    }
    $outHtml .= '</div>';

    if($review_link):
        $outHtml .= '<a href="'. $review_link .'" target="_blank" class="gfs-morelink">'.__('View More Reviews').'</a>';
    endif;

    $outHtml .= '</div>';


    if ( $privateKey || $clientId ) {
        wp_enqueue_style( 'gfs-style' );
        return $outHtml;
    }
}

add_shortcode('gfs-reviews', 'gfs_render_reviews');


function gfs_business_info() {

    $privateKey = get_option('gfs_api_key');
    $clientId = get_option('gfs_client_id');

    $api = new GetFiveStarsSimpleApi($clientId, $privateKey);
    $response = json_decode($api->doRequest('/business/get', array('businessId' => get_option('gfs_business_id'))));
    $outHtml = '';
    if ($response) {
        $outHtml .= '<link itemprop="image" href="'. $response->emailLogo .'">';
        $outHtml .= '<div class="gfs-address" itemprop="address" itemscope="" itemtype="https://schema.org/PostalAddress"> 
                        <meta itemprop="streetAddress" content="'. $response->streetAddress .'"> 
                        <meta itemprop="addressLocality" content="'. $response->city .'"> 
                        <meta itemprop="addressRegion" content="'. $response->state .'"> 
                        <meta itemprop="postalCode" content="'. $response->zip .'"> 
                        <meta itemprop="telephone" content="'. $response->phone .'"> 
                        <meta itemprop="url" content="'. $response->websiteURL .'"> 
                    </div>';

        $outHtml .= '<h2 class="gfs-businessname gfs-hide" itemprop="name">'. $response->name .'</h2>';
    }

    return $outHtml;
}

add_shortcode('gfs-business-info', 'gfs_business_info');