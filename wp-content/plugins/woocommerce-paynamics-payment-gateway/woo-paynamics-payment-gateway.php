<?php
/*
Plugin Name:       WooCommerce Paynamics Payment Gateway
Description:       Paynamics Payment Gateway WooCommerce Extension
Version:           1.0
Author:            JST
*/

function wc_ppg_init()
{
    global $woocommerce;

    if (!isset($woocommerce)) {
        return;
    }

    if (!defined('ABSPATH')) exit;
    if (!class_exists('WC_Gateway_Paynamics_Payment_Gateway')): class WC_Gateway_Paynamics_Payment_Gateway extends WC_Payment_Gateway
    {

        public function __construct()
        {
            $this->id = 'ppg';
            $this->icon = apply_filters('woocommerce_ppg_icon', '');
            $this->has_fields = false;
            $this->method_title = __('Paynamics Payment Gateway', 'wc_ppg');
            $this->order_button_text = apply_filters('woocommerce_ppg_order_button_text', __('Place order', 'wc_ppg'));
            $this->init_form_fields();
            $this->init_settings();
            $this->title = $this->get_option('title');
            $this->description = $this->get_option('description');
            $this->sender_url = $this->get_option('sender_url');

            //advanced
            $this->sandbox = $this->get_option('sandbox');
            $this->descriptor = $this->get_option('descriptor');
            $this->ip_address = $this->get_option('ip_address');
            $this->notif_url = $this->get_option('notif_url');
            $this->response_url = $this->get_option('response_url');
            $this->cancel_url = $this->get_option('cancel_url');
            $this->mtac_url = $this->get_option('mtac_url');
            $this->mlogo_url = $this->get_option('mlogo_url');

            //Sandbox
            $this->test_mid = $this->get_option('test_mid');
            $this->test_mkey = $this->get_option('test_mkey');
            $this->test_wpf_url = $this->get_option('test_wpf_url');

            //Live
            $this->live_mid = $this->get_option('live_mid');
            $this->live_mkey = $this->get_option('live_mkey');
            $this->live_wpf_url = $this->get_option('live_wpf_url');

            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
            add_action('woocommerce_thankyou_ppg', array($this, 'thankyou'));
        }

        function admin_options()
        {
            ?>
            <h3><?php _e('Paynamics Payment Gateway', 'wc_ppg'); ?></h3>
            <p><?php _e('Paynamics Inc. Your trusted online payment partner.', 'wc_ppg'); ?></p>
            <table class="form-table">
                <?php $this->generate_settings_html(); ?>
            </table>
        <?php
        }

        public function init_form_fields()
        {
            $shipping_methods = array();

            if (is_admin()) {
                foreach (WC()->shipping->load_shipping_methods() as $method) {
                    $shipping_methods[$method->id] = $method->get_title();
                }
            }

            $this->form_fields = array(
                'enabled' => array(
                    'title' => __('Enable/Disable', 'wc_ppg'),
                    'type' => 'checkbox',
                    'label' => __('Enable Paynamics Payment Gateway', 'wc_ppg'),
                    'default' => 'yes'
                ),
                'title' => array(
                    'title' => __('Title', 'wc_ppg'),
                    'type' => 'text',
                    'description' => __('Payment method title which the customer will see during checkout', 'wc_ppg'),
                    'default' => __('Paynamics Payment Gateway', 'wc_ppg'),
                    'desc_tip' => true,
                ),
                'description' => array(
                    'title' => __('Description', 'wc_ppg'),
                    'type' => 'textarea',
                    'description' => __('Payment method description which the customer will see during checkout', 'wc_ppg'),
                    'default' => __('Credit Cards, BancNet, DragonPay, G-Cash', 'wc_ppg'),
                    'desc_tip' => true,
                ),
                'sender_url' => array(
                    'title' => __('Sender URL', 'wc_ppg'),
                    'type' => 'textarea',
                    'description' => __('sender.php file location', 'wc_ppg'),
                    'default' => __('', 'wc_spg'),
                    'desc_tip' => true,
                ),

                //advanced settings
                'advanced' => array(
                    'title' => __('Advanced settings', 'wc_ppg'),
                    'type' => 'title',
                    'description' => 'Web Payment Configuration',
                ),
                'sandbox' => array(
                    'title' => __('Enable/Disable', 'wc_ppg'),
                    'type' => 'checkbox',
                    'label' => __('Enable Sandbox', 'wc_ppg'),
                    'default' => 'yes',
                    'description' => 'Sandbox is used to test payments. Disabling this will redirect you to production environment and requires live merchant credentials.',
                ),
                'descriptor' => array(
                    'title' => __('Billing Descriptor', 'wc_ppg'),
                    'type' => 'text',
                    'description' => __('The way a company name appears on a credit card statement', 'wc_ppg'),
                    'default' => __('', 'wc_ppg'),
                    'desc_tip' => true,
                ),
                'ip_address' => array(
                    'title' => __('IP Address', 'wc_ppg'),
                    'type' => 'text',
                    'description' => __('Merchant Host IP Address', 'wc_ppg'),
                    'default' => __('', 'wc_ppg'),
                    'desc_tip' => true,
                ),
                'notif_url' => array(
                    'title' => __('Notification Receiver URL', 'wc_ppg'),
                    'type' => 'textarea',
                    'description' => __('payment_notification.php location where PAYGATE would POST base64 encoded xml notification update for their transaction or its final status', 'wc_ppg'),
                    'default' => __('', 'wc_ppg'),
                    'desc_tip' => true,
                ),
                'response_url' => array(
                    'title' => __('Response URL', 'wc_ppg'),
                    'type' => 'textarea',
                    'description' => __('Merchant landing page where PAYGATE will redirect the transaction after the result page', 'wc_ppg'),
                    'default' => __('', 'wc_ppg'),
                    'desc_tip' => true,
                ),
                'cancel_url' => array(
                    'title' => __('Cancel URL', 'wc_ppg'),
                    'type' => 'textarea',
                    'description' => __('Merchant landing page where PAYGATE will redirect the transaction after the result page once the customer pressed cancel button', 'wc_ppg'),
                    'default' => __('', 'wc_ppg'),
                    'desc_tip' => true,
                ),
                'mtac_url' => array(
                    'title' => __('Terms and Conditions URL', 'wc_ppg'),
                    'type' => 'textarea',
                    'description' => __('This URL will be a link to the Merchants Terms and Conditions page and must be hosted in https environment.', 'wc_ppg'),
                    'default' => __('', 'wc_ppg'),
                    'desc_tip' => true,
                ),
                'mlogo_url' => array(
                    'title' => __('Merchant Logo URL', 'wc_ppg'),
                    'type' => 'textarea',
                    'description' => __('URL of merchant logo and must be hosted in https environment', 'wc_ppg'),
                    'default' => __('', 'wc_ppg'),
                    'desc_tip' => true,
                ),

                //merchant credentials
                'credentials' => array(
                    'title' => __('Merchant Credentials', 'wc_ppg'),
                    'type' => 'title',
                    'description' => '',
                ),
                'test_mid' => array(
                    'title' => __('Sandbox/Test Merchant ID', 'wc_ppg'),
                    'type' => 'text',
                    'description' => __('The merchant identifier with Paygate and it comes with a key.', 'wc_ppg'),
                    'default' => __('', 'wc_ppg'),
                    'desc_tip' => true,
                ),
                'test_mkey' => array(
                    'title' => __('Sandbox/Test Merchant Key', 'wc_ppg'),
                    'type' => 'text',
                    'description' => __('The merchant key always comes with merchant id', 'wc_ppg'),
                    'default' => __('', 'wc_ppg'),
                    'desc_tip' => true,
                ),
                'test_wpf_url' => array(
                    'title' => __('Sandbox/Test Web Payment Frontend URL', 'wc_ppg'),
                    'type' => 'textarea',
                    'description' => __('This is the sandbox URL provided by Paynamics. Default URL is: https://testpti.payserv.net/webpaymentv2/default.aspx', 'wc_ppg'),
                    'default' => __('', 'wc_ppg'),
                ),
                'live_mid' => array(
                    'title' => __('Live Merchant ID', 'wc_ppg'),
                    'type' => 'text',
                    'description' => __('The merchant identifier with Paygate and it comes with a key.', 'wc_ppg'),
                    'default' => __('', 'wc_ppg'),
                    'desc_tip' => true,
                ),
                'live_mkey' => array(
                    'title' => __('Live Merchant Key', 'wc_ppg'),
                    'type' => 'text',
                    'description' => __('The merchant key always comes with merchant id', 'wc_ppg'),
                    'default' => __('', 'wc_ppg'),
                    'desc_tip' => true,
                ),
                'live_wpf_url' => array(
                    'title' => __('Live Web Payment Frontend URL', 'wc_ppg'),
                    'type' => 'textarea',
                    'description' => __('This is the live payment page URL provided by Paynamics.', 'wc_ppg'),
                    'default' => __('', 'wc_ppg'),
                    'desc_tip' => true,
                ),
            );
        }

        public function is_available()
        {
            if (!empty($this->enable_for_methods)) {

                $chosen_shipping_methods_session = WC()->session->get('chosen_shipping_methods');

                if (isset($chosen_shipping_methods_session)) {
                    $chosen_shipping_methods = array_unique($chosen_shipping_methods_session);
                } else {
                    $chosen_shipping_methods = array();
                }

                $check_method = false;

                if (is_page(wc_get_page_id('checkout')) && !empty($wp->query_vars['order-pay'])) {

                    $order_id = absint($wp->query_vars['order-pay']);
                    $order = new WC_Order($order_id);

                    if ($order->shipping_method)
                        $check_method = $order->shipping_method;

                } elseif (empty($chosen_shipping_methods) || sizeof($chosen_shipping_methods) > 1) {
                    $check_method = false;
                } elseif (sizeof($chosen_shipping_methods) == 1) {
                    $check_method = $chosen_shipping_methods[0];
                }

                if (!$check_method)
                    return false;

                $found = false;

                foreach ($this->enable_for_methods as $method_id) {
                    if (strpos($check_method, $method_id) === 0) {
                        $found = true;
                        break;
                    }
                }

                if (!$found)
                    return false;
            }

            return parent::is_available();
        }

        public function process_payment($order_id)
        {
            $order = new WC_Order($order_id);
            $order->update_status(apply_filters('wc_ppg_default_order_status', 'pending'), __('Awaiting payment', 'wc_ppg'));
            $order->reduce_order_stock();
            //WC()->cart->empty_cart();
            return array(
                'result' => 'success',
                'redirect' => $this->get_return_url($order)
            );
        }

        public function thankyou($order_id)
        {
            $countries = new WC_Countries;
            $order = new WC_Order($order_id);
            $billing_raw_state = get_post_meta($order_id, '_billing_state', true);
            $billing_country = get_post_meta($order_id, '_billing_country', true);
            $billing_state = ($billing_country && $billing_raw_state && isset($countries->states[$billing_country][$billing_raw_state])) ? $countries->states[$billing_country][$billing_raw_state] : $billing_raw_state;

            $customer_json = array();
            $customer_json[] = array(
                'orderid' => $order_id,
                'fname' => get_post_meta($order_id, '_billing_first_name', true),
                'lname' => get_post_meta($order_id, '_billing_last_name', true),
                'address1' => get_post_meta($order_id, '_billing_address_1', true),
                'address2' => get_post_meta($order_id, '_billing_address_2', true),
                'city' => get_post_meta($order_id, '_billing_city', true),
                'state' => $billing_state,
                'postcode' => get_post_meta($order_id, '_billing_postcode', true),
                'country' => get_post_meta($order_id, '_billing_country', true),
                'email' => get_post_meta($order_id, '_billing_email', true),
                'phone' => get_post_meta($order_id, '_billing_phone', true),
                'ip_address' => get_post_meta($order_id, '_billing_phone', true),
                'shipping' => get_post_meta($order_id, '_order_shipping', true),
                'order_count' => count($order->get_items())
            );

            $order_array = array();
            $i = 0;
            foreach ($order->get_items() as $item) {
                $order_array['item_id' . $i] = $item['product_id'];
                $order_array['item_name' . $i] = $item['name'];

                $product = new WC_Product($item['product_id']);
                $order_array['item_price' . $i] = $product->get_price();//get_post_meta($item['product_id'], "_price", true);

                $order_array['item_qty' . $i] = $item['qty'];
                $order_array['item_total' . $i] = $item['line_total'];
                $i++;
            }

            $order_array['discount'] = get_post_meta($order_id, '_cart_discount', true);
            $order_array['amount'] = get_post_meta($order_id, '_order_total', true);
            $order_array['currency'] = get_post_meta($order_id, '_order_currency', true);
            $order_array['tax'] = get_post_meta($order_id, '_order_tax', true);

            //sender.php config
            $merchant_array = array();
            $merchant_array['mid'] = "";
            $merchant_array['mkey'] = "";
            $merchant_array['wpf_url'] = "";

            if ($this->sandbox == "yes") {
                $merchant_array['mid'] = $this->test_mid;
                $merchant_array['mkey'] = $this->test_mkey;
                $merchant_array['wpf_url'] = $this->test_wpf_url;
            } else {
                $merchant_array['mid'] = $this->live_mid;
                $merchant_array['mkey'] = $this->live_mkey;
                $merchant_array['wpf_url'] = $this->live_wpf_url;
            }

            $merchant_array['descriptor'] = $this->descriptor;
            $merchant_array['ip_address'] = $this->ip_address;
            $merchant_array['notif_url'] = $this->notif_url;
            $merchant_array['response_url'] = $this->response_url;
            $merchant_array['cancel_url'] = $this->cancel_url;
            $merchant_array['mtac_url'] = $this->mtac_url;
            $merchant_array['mlogo_url'] = $this->mlogo_url;

            $order_json[] = $order_array;
            $merchant_json[] = $merchant_array;
            $customer_jsonstring = json_encode($customer_json);
            $order_jsonstring = json_encode($order_json);
            $merchant_jsonstring = json_encode($merchant_json);

            header('Location: ' . $this->sender_url . '?cust=' . base64_encode($customer_jsonstring) . '&ord=' . base64_encode($order_jsonstring) . '&mer=' . base64_encode($merchant_jsonstring));
        }
    }
    endif;
}

add_action('plugins_loaded', 'wc_ppg_init');

function add_ppg($methods)
{
    $methods[] = 'WC_Gateway_Paynamics_Payment_Gateway';
    return $methods;
}

add_filter('woocommerce_payment_gateways', 'add_ppg');

