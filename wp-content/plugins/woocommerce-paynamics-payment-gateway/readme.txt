﻿Paynamics Payment WooCommerce ExtensionNote:

1. Paste or upload the plugin folder to Wordpress plugins directory.

2. WooCommerce Payment Gateway Settings - Sender URL: Where the 'sender' PHP file is hosted.

3. Set the sender.php with the merchant credentials provided by your contact person. (this can be done in woocommerce admin settings)

4. Confirm with your contact person the webpayment URL you need to use. Default is: https://testpti.payserv.net/webpaymentV2/Default.aspx

5. Included is the sample code for notification receiver(payment_notification.php) file. Place this on your wordpress
   root directory. Then in your woocommerce admin settings in the notification url field set the url where the "payment_notification.php" file 
   is hosted.
	Open the file set the merchant key in this parameter $_cert = ""; then go to your woocommerce admin settings
        and set the url where the receiver is hosted. Ex. notification receiver url = "www.sample.com/payment_notification.php"