<?php

session_start();

require_once('wp-config.php');

try {
    //$body = $_POST['paymentresponse'];
    $body = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48U2VydmljZVJlc3BvbnNlV1BGIHhtbG5zOnhzZD0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEiIHhtbG5zOnhzaT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEtaW5zdGFuY2UiPjxhcHBsaWNhdGlvbj48bWVyY2hhbnRpZD4wMDAwMDAxMzA2MTY1MkEyRDFDOTwvbWVyY2hhbnRpZD48cmVxdWVzdF9pZD4xODI4NDMwNjE1PC9yZXF1ZXN0X2lkPjxyZXNwb25zZV9pZD5QMjI5OTg2OTg5OTY0Mjc2MjM5MTwvcmVzcG9uc2VfaWQ+PHRpbWVzdGFtcD4yMDE3LTA0LTE4VDExOjI4OjIyLjAwMDAwMCswODowMDwvdGltZXN0YW1wPjxyZWJpbGxfaWQgLz48c2lnbmF0dXJlPjQxZGI0OGVkYTU2NTQyNGQ3ZGJkMDkzOWJlYTIyOWYxMDA4MTRiOTVmYTQ2MDJmM2Y2ZDZmM2E4YzczNWEyZWJjZmQ0ZDM0ZDgwMzRiNWRlNWI4MWY5MWVmZWZlZWNmNzhmMzRhM2RjNzkwZjY3ZjU4MWE4M2EyNGFkZDljNWJiPC9zaWduYXR1cmU+PHB0eXBlIC8+PC9hcHBsaWNhdGlvbj48cmVzcG9uc2VTdGF0dXM+PHJlc3BvbnNlX2NvZGU+R1IwMDE8L3Jlc3BvbnNlX2NvZGU+PHJlc3BvbnNlX21lc3NhZ2U+VHJhbnNhY3Rpb24gU3VjY2Vzc2Z1bDwvcmVzcG9uc2VfbWVzc2FnZT48cmVzcG9uc2VfYWR2aXNlPlRyYW5zYWN0aW9uIGlzIGFwcHJvdmVkPC9yZXNwb25zZV9hZHZpc2U+PHByb2Nlc3Nvcl9yZXNwb25zZV9pZD4yMDAwMDEwOTA4PC9wcm9jZXNzb3JfcmVzcG9uc2VfaWQ+PHByb2Nlc3Nvcl9yZXNwb25zZV9hdXRoY29kZT42MTA0ODQ8L3Byb2Nlc3Nvcl9yZXNwb25zZV9hdXRoY29kZT48L3Jlc3BvbnNlU3RhdHVzPjxzdWJfZGF0YSAvPjx0cmFuc2FjdGlvbkhpc3Rvcnk+PHRyYW5zYWN0aW9uIC8+PC90cmFuc2FjdGlvbkhpc3Rvcnk+PC9TZXJ2aWNlUmVzcG9uc2VXUEY+";

    echo "INITIAL DATA : " . $body;

    $base64 = str_replace(" ", "+", $body);
    $body = base64_decode($base64); // this will be the actual xml
    $data = simplexml_load_string($body);

    echo "<br/><br/>RECEIVED DATA : " . $body;
    echo "<br/><br/>RESPONSE CODE : " . $data->responseStatus->response_code;
    echo "<br/>";

    $pos = strpos($data->application->request_id, 'PLO');

    if ($pos !== false) {
        $order_id = substr($data->application->request_id, 4);
    } else {
        $order_id = $data->application->request_id;
	//$order_id = substr($data->application->request_id, 8);
    }

    //$order = new WC_Order($order_id);
    $order = new WC_Order(absint($order_id));

    try {
        $forSign = $data->application->merchantid . $data->application->request_id . $data->application->response_id . $data->responseStatus->response_code . $data->responseStatus->response_message . $data->responseStatus->response_advise . $data->application->timestamp . $data->application->rebill_id;
        $cert = ""; //<-- your merchant key
        $_sign = hash("sha512", $forSign . $cert);

        echo "<br/><br/>signedXMLResponse: " . $data->application->signature;
        echo "<br/><br/>Signature : " . $_sign;
        //if signature verified and equal, update database
        //query and update here

        if($data->application->signature == $_sign)
        {
            // check if successful payment
            if ($data->responseStatus->response_code == 'GR001' || $data->responseStatus->response_code == 'GR002') {
                echo "<br/><br/>GR001 or GR002";
                //update here
                // Payment complete
                $order->payment_complete();
            }
            // check if pending payment
            else if ($data->responseStatus->response_code == 'GR033') {
                echo "<br/><br/>PENDING";
                //update here
                $order->update_status(apply_filters('wc_ppg_default_order_status', 'pending'), __('Awaiting payment', 'wc_ppg'));
            }
            // check if payment was cancelled
            else if ($data->responseStatus->response_code == 'GR053') {
                echo "<br/><br/>CANCELLED";
                //update here
                $order->update_status(apply_filters('wc_ppg_default_order_status', 'cancelled'), __('Cancelled payment', 'wc_ppg'));
            }
            //check if failed payment
            else {
                echo "<br/><br/>FAILED";
                //update here
                $order->update_status(apply_filters('wc_ppg_default_order_status', 'failed'), __('Payment failed', 'wc_ppg'));
            }
        }
        else {
            echo "Signatures don't match.";
        }

    } catch (Exception $ex) {
        echo $ex;
    }
} catch (Exception $ex) {
    echo $ex;
}