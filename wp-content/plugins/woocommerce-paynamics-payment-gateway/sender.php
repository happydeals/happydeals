<html>
<head>
    <meta charset="UTF-8">
    <title>Paygate Web Payment System</title>
    <link href="assets/css/loader.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        window.onload = function () {
            var button = document.getElementById('btn-submit');
            button.form.submit();
        }
    </script>
</head>
<body>
<div class="Absolute-Center">
    <div style="text-align: center">
        <h3>Please wait..</h3>
        <div class="sk-fading-circle">
            <div class="sk-circle1 sk-circle"></div>
            <div class="sk-circle2 sk-circle"></div>
            <div class="sk-circle3 sk-circle"></div>
            <div class="sk-circle4 sk-circle"></div>
            <div class="sk-circle5 sk-circle"></div>
            <div class="sk-circle6 sk-circle"></div>
            <div class="sk-circle7 sk-circle"></div>
            <div class="sk-circle8 sk-circle"></div>
            <div class="sk-circle9 sk-circle"></div>
            <div class="sk-circle10 sk-circle"></div>
            <div class="sk-circle11 sk-circle"></div>
            <div class="sk-circle12 sk-circle"></div>
        </div>
    </div>
</div>
</body>
</html>
<?php

function generateRandomID($length = 8)
{
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

//order & customer & merchant details
$customer_details = base64_decode($_GET['cust']);
$order_details = base64_decode($_GET['ord']);
$merchant_details = base64_decode($_GET['mer']);

$array_customer = json_decode($customer_details, true);
$array_order = json_decode($order_details, true);
$array_merchant = json_decode($merchant_details, true);

$orderid = $array_customer[0]["orderid"];
$firstname = $array_customer[0]["fname"];
$lastname = $array_customer[0]["lname"];
$address1 = $array_customer[0]["address1"];
$address2 = $array_customer[0]["address2"];
$city = $array_customer[0]["city"];
$state = $array_customer[0]["state"];
$zip = $array_customer[0]["postcode"];
$country = $array_customer[0]["country"];
$email = $array_customer[0]["email"];
$phone = $array_customer[0]["phone"];
$ip_address = $array_customer[0]["ip_address"];
$order_count = $array_customer[0]["order_count"];
$order_total = $array_order[0]["amount"];
$order_tax = $array_order[0]["tax"];
$currency = $array_order[0]["currency"];
$shipping = $array_customer[0]["shipping"];
$discount = $array_order[0]["discount"];

if ($shipping == null || $shipping == ""){
    $shipping = "0.00";
}

//merchant config
$descriptor = $array_merchant[0]["descriptor"];
$ip_address = $array_merchant[0]["ip_address"];
$notif_url = $array_merchant[0]["notif_url"];
$response_url = $array_merchant[0]["response_url"];
$cancel_url = $array_merchant[0]["cancel_url"];
$mtac_url = $array_merchant[0]["mtac_url"];
$mlogo_url = $array_merchant[0]["mlogo_url"];

$mid = $array_merchant[0]["mid"];
$mkey = $array_merchant[0]["mkey"];
$wpf_url = $array_merchant[0]["wpf_url"];

$_mid = $mid; //<-- your merchant id
$_requestid = $orderid;
$_ipaddress = $ip_address;
$_noturl = $notif_url;
$_resurl = $response_url;
$_cancelurl = $cancel_url;
$_fname = $firstname;
$_mname = "";
$_lname = $lastname;
$_addr1 = $address1;
$_addr2 = $address2;
$_city = $city;
$_state = $state;
$_country = $country;
$_zip = $zip;
$_sec3d = "try3d";
$_email = $email;
$_phone = $phone;
$_mobile = $phone;
$_clientip = $_SERVER['REMOTE_ADDR'];
$_amount = number_format(($order_total), 2, '.', $thousands_sep = '');
$_shipping = number_format(($shipping), 2, '.', $thousands_sep = '');
$_discount = number_format(($discount), 2, '.', $thousands_sep = '');
$_currency = $currency;

$forSign = $_mid . $_requestid . $_ipaddress . $_noturl . $_resurl . $_fname . $_lname . $_mname . $_addr1 . $_addr2 . $_city . $_state . $_country . $_zip . $_email . $_phone . $_clientip . number_format(($_amount), 2, '.', $thousands_sep = '') . $_currency . $_sec3d;
$cert = $mkey; //<-- your merchant key

//	echo $_mid . "<hr />";
//	echo $cert . "<hr />";
// echo $forSign . "<hr />";

$_sign = hash("sha512", $forSign . $cert);
$xmlstr = "";

$strxml = "";

$strxml = $strxml . "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
$strxml = $strxml . "<Request>";
$strxml = $strxml . "<orders>";
$strxml = $strxml . "<items>";

for($i=0; $i<$order_count; $i++)
{
    $strxml = $strxml . "<Items>";
    $strxml = $strxml . "<itemname>" . $array_order[0]["item_name" . $i] . "</itemname><quantity>" . $array_order[0]["item_qty" . $i] . "</quantity><amount>" . number_format($array_order[0]["item_price" . $i], 2, '.', $thousands_sep = '') . "</amount>";
    $strxml = $strxml . "</Items>";
}

if ($shipping != "0.00"){
    $strxml = $strxml . "<Items>";
    $strxml = $strxml . "<itemname>Shipping and Handling Fee</itemname><quantity>" . "1" . "</quantity><amount>" . number_format($_shipping, 2, '.', $thousands_sep = '') . "</amount>";
    $strxml = $strxml . "</Items>";
}

if ($_discount != "0.00"){
    $strxml = $strxml . "<Items>";
    $strxml = $strxml . "<itemname>Discount</itemname><quantity>" . "1" . "</quantity><amount>-" . number_format($_discount, 2, '.', $thousands_sep = '') . "</amount>";
    $strxml = $strxml . "</Items>";
}

$strxml = $strxml . "</items>";
$strxml = $strxml . "</orders>";
$strxml = $strxml . "<mid>" . $_mid . "</mid>";
$strxml = $strxml . "<request_id>" . $_requestid . "</request_id>";
$strxml = $strxml . "<ip_address>" . $_ipaddress . "</ip_address>";
$strxml = $strxml . "<notification_url>" . $_noturl . "</notification_url>";
$strxml = $strxml . "<response_url>" . $_resurl . "</response_url>";
$strxml = $strxml . "<cancel_url>" . $_cancelurl . "</cancel_url>";
$strxml = $strxml . "<mtac_url>" . $mtac_url . "</mtac_url>"; // pls set this to the url where your terms and conditions are hosted
$strxml = $strxml . "<descriptor_note>" . $descriptor . "</descriptor_note>"; // pls set this to the descriptor of the merchant
$strxml = $strxml . "<fname>" . $_fname . "</fname>";
$strxml = $strxml . "<lname>" . $_lname . "</lname>";
$strxml = $strxml . "<mname>" . $_mname . "</mname>";
$strxml = $strxml . "<address1>" . $_addr1 . "</address1>";
$strxml = $strxml . "<address2>" . $_addr2 . "</address2>";
$strxml = $strxml . "<city>" . $_city . "</city>";
$strxml = $strxml . "<state>" . $_state . "</state>";
$strxml = $strxml . "<country>" . $_country . "</country>";
$strxml = $strxml . "<zip>" . $_zip . "</zip>";
$strxml = $strxml . "<secure3d>" . $_sec3d . "</secure3d>";
$strxml = $strxml . "<trxtype>sale</trxtype>";
$strxml = $strxml . "<email>" . $_email . "</email>";
$strxml = $strxml . "<phone>" . $_phone . "</phone>";
$strxml = $strxml . "<mobile>" . $_mobile . "</mobile>";
$strxml = $strxml . "<client_ip>" . $_clientip . "</client_ip>";
$strxml = $strxml . "<amount>" . number_format(($_amount), 2, '.', $thousands_sep = '') . "</amount>";
$strxml = $strxml . "<currency>" . $_currency . "</currency>";
$strxml = $strxml . "<mlogo_url>" . $mlogo_url . "</mlogo_url>"; // pls set this to the url where your logo is hosted
$strxml = $strxml . "<pmethod></pmethod>";
$strxml = $strxml . "<signature>" . $_sign . "</signature>";
$strxml = $strxml . "</Request>";
$b64string = base64_encode($strxml);
//print_r(simplexml_load_string($strxml));

//echo "<textarea> ". $strxml ." </textarea> <hr />";
// "<pre>" . $b64string . "</pre><hr />";
//echo "<hr />";

echo '<form name="form1" method="post" action="' . $wpf_url . '">
            <input type="hidden" name="paymentrequest" id="paymentrequest" value="' . $b64string . '" style="width:800px; padding: 20px;">
            <input id="btn-submit" type="submit" class="btn btn-2 btn-2a" value="Proceed to Payment" style="visibility: hidden">
            </form>';
?>
