<?php 
if ( isset($_GET['deal_redirect']) ) {
	fw_ssd_hande_deal_redirect();
}

if ( is_user_logged_in() ) {
        if(!is_checkout()) {
        $current_user = wp_get_current_user();
        $user_id = $current_user->ID;
        $approved_status = get_user_meta($user_id, 'wcemailverified', true);
        global $wp;
        $current_url = home_url(add_query_arg(array(),$wp->request));
                if ( !$approved_status){
                   wp_logout();
                   wp_redirect( $current_url );
                   exit;
                }
        }                 
}      

?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>" />

	<!-- Open Graph -->
	<?php fw_ssd_og_header(); ?>

    <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>

    <?php wp_head(); ?>
	<link href="https://fonts.googleapis.com/css?family=Gorditas" rel="stylesheet">
    <!-- Slider -->
	<link rel="stylesheet" type="text/css" media="print" href="<?php bloginfo('stylesheet_directory'); ?>/print.css" />
	<link href="https://fonts.googleapis.com/css?family=Titan+One" rel="stylesheet">
<!--	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>     -->
	



        	<!-- PLEASE SEE LINKED STYLESHEETS AND SCRIPTS -->
 	<!--<link rel="stylesheet" href="/wp-includes/css/jquery-ui.css">-->
 		<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.number.min.js"></script> 
		<script src="<?php bloginfo('stylesheet_directory'); ?>/js/selectize.js"></script>
 		<script src="<?php bloginfo('stylesheet_directory'); ?>/js/custom.js"></script>   
		<!--<link href="/wp-includes/css/materialize.css" rel="stylesheet" />  -->
	        <link href="<?php bloginfo('stylesheet_directory'); ?>/css/custom.css" rel="stylesheet" />

	    <!--      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
                      <!--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->

</head>
<?php 
$body_classes = array();

$woocommerce_enable = fw_ssd_get_option('woocommerce-switch');
if ( is_singular('deal') && $woocommerce_enable['woocommerce-picker'] == 'yes' ) {
	array_push($body_classes, 'woocommerce');
}

$url_user = get_option('woocommerce_myaccount_page_id') ? get_permalink( get_option('woocommerce_myaccount_page_id') ) : '';
		
			$url_login = get_permalink( get_option('woocommerce_myaccount_page_id') ) ? get_permalink( get_option('woocommerce_myaccount_page_id') ) : '';

			$url_logout = wc_get_page_id( 'myaccount' ) ? wp_logout_url( get_permalink( wc_get_page_id( 'myaccount' ) ) ) : '';
   $image = fw_ssd_get_option('header-search-image');
	if ( $image ) {
		$bg_image = wp_get_attachment_image_src( $image['attachment_id'], 'ssd_single-post-image' );
		$image_url = $bg_image['0'];
	} else {
		$image_url = '';
	}
	

	// Categories
	$term_args = array( 'hide_empty' => 0 );
	$deal_cats = get_terms('deal_category', $term_args );   
	$deal_country = get_terms('deal_country', $term_args ); 

?>
	<body <?php body_class($body_classes);?>>
	
     <div class="navigation-wrapper">
        <div style="float:right;background-color: #45BB99;height: 75px;width: 50%;position: absolute;right: 0;"></div>
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="site-logo">
					<a href="<?php echo esc_url(home_url('/')); ?>">
						<?php
						$logo_image = fw_ssd_get_option('logo_image');
						$logo_name = fw_ssd_get_option('logo_name');
						if( !empty( $logo_image ) ) {
							?>
							<img src="<?php echo esc_url( $logo_image['url'] ); ?>" alt="<?php echo esc_attr( $logo_name ); ?>">
							<?php } else if( !empty( $logo_image ) ) { ?>
							<h1><?php echo esc_attr( $logo_name ); ?></h1>
							<?php } else { ?>
							<h1><?php echo get_bloginfo( 'name', 'display' ); ?></h1>
							<?php } ?>
						</a>
					</div>
			</div><!-- end col-sm-3 -->   
                                        
			<div class="col-sm-9 navbar-right">
                                <div class="navbar-right-bg"></div>
                                <div class="navbar-right-content">
                                <div class="col-md-7" style="padding-left: 0;">
                                <!-- Search -->
                                        <div class="inner-addon left-addon" >
                                                <form id="searchform" action="<?php echo esc_url( home_url( "/" ) ); ?>" method="get"  autocomplete="off">
                                                <div style="color: grey; border-radius: 25px; background-color: #fff; height: 28px;">
                                                    <i class="glyphicon glyphicon-search search_icon"></i>  
                                                    <input id="deal_search" type="text" autocomplete="off" value="<?php echo isset($_REQUEST['s']) ? str_replace('\\', '', $_REQUEST['s']) : ''; ?>" style="border:none;background: none;" name="s" class="form-control"  placeholder="<?php esc_attr_e('Search Deals', 'couponhut');?>" />
                                                    <i class="glyphicon glyphicon-search search_glyph_button" style="background-color: #F5944F; border-radius: 25px; color: white;margin-left: 6px; pointer-events: auto;" id="submitSearchButton" ></i>  
                                                
                                                </div>
                                                    
                                                <input type="hidden" name="post_type" value="product" />
                                                </form>
												<div class="header-search_result"></div>
												<div class="header-search_suggestions"><?php echo get_search_suggestions(); ?></div>
                                        </div>
                                        
                                </div>
                                <div class="col-md-5" style="padding: 0">
                                        <div class="col-xs-4" style="padding: 0;">
                                     	<a href="/cart" class="shoppingCart shoppingCarttop">
					 	<i class="glyphicon glyphicon-shopping-cart" ><span><?php echo sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?></span></i>
					 </a> 
                                         </div>           
                                        <div class="col-xs-8 nav-top" style="padding: 0 0 0 10px;"> 
                                        <ul class="nav navbar-nav usermenu">
                                       
                                        <li class="menu-item-has-children menu-item-login-register">
				<?php if ( is_user_logged_in() ) : ?>
					<?php 
					$current_user = wp_get_current_user();
					?>
					<a href="<?php echo $url_user; ?>" style="padding: 0"><i class='glyphicon glyphicon-user'></i><span class="userlogin"><?php echo substr($current_user->first_name, 0, 10); ?></span></a>
                                        <i class="fa fa-angle-down"></i>
				<?php else: ?>
					<a href="<?php echo $url_login; ?>" style="padding: 0"><i class='glyphicon glyphicon-user'></i></a>   
                                        <a style="padding: 0;" href="<?php echo $url_login; ?>"><?php esc_html_e('Log In / Register', 'couponhut') ?></a>                                        
				<?php endif; ?>      
                                <?php if ( is_user_logged_in() ) : ?>
				<ul class="sub-menu">
					<?php  $array_menu = wp_get_nav_menu_items('User menu'); ?>
                                                <?php  foreach ($array_menu as $m)  { ?>
                                                        <li><a href="<?php echo $m->url; ?>"><?php echo $m->title; ?></a></li>
                                                <?php } ?>
					
						<?php 

						$allow_no_reg = fw_ssd_get_option('member-submit-switch');
						$allow_no_company = fw_ssd_get_option('member-submit-without-company-switch');
						$show_submit_menu = true;

						if ( fw_ssd_woocommerce() && !is_user_logged_in() && !$allow_no_reg ) {
							$show_submit_menu = false;
						}

						if ( fw_ssd_woocommerce() && is_user_logged_in() && !$allow_no_reg && !get_user_meta( get_current_user_id(), 'user_company_name', true ) && !$allow_no_company  ) {
							$show_submit_menu = false;
						}	

					
						$template_id = fw_ssd_get_submit_template_id(); ?>
					
						<li><a href="<?php echo $url_logout; ?>"><?php esc_html_e('Log Out', 'couponhut') ?></a></li>
						
					
				</ul>    
                                <?php endif; ?>  
			</li>
                                        
                                      </ul>
                                      </div>
                                </div>
			</div><!-- end col-sm-9 -->
		</div><!-- end row -->
                
                <div class="row">
                    	<div class="col-sm-12">
				<?php
				if ( has_nav_menu('main-navigation') ) : ?>
				<nav id="main-navigation" class="main-navigation">
					<?php
					wp_nav_menu( array(
						'theme_location'  => 'main-navigation',
						'container' 	  => false,
						'menu_class'      => 'is-slicknav',
						'walker'		  => new CouponHut_Main_Menu_Walker
						));
						?>
					</nav>
				<?php endif; ?>

				<span class="mpc-count"></span>

			</div><!-- end col-sm-9 -->
                </div>
                </div><!-- end row -->
	</div><!-- end container -->
	

</div><!-- end navigation-wrapper -->




<script language="javascript" type="text/javascript">
    function submitDetailsForm() {
       $("#searchform").submit();
    }
</script>

	


<?php 
$show_search = false;

$search_screen = fw_ssd_get_option('search-screen');

switch ( get_post_type(get_the_ID()) ) {
	case 'post':
		$show_search = isset($search_screen['post']) ? $search_screen['post'] : false;
		break;
	case 'deal':
		$show_search = isset($search_screen['deal']) ? $search_screen['deal'] : false;
		break;
	case 'page':
		if ( is_page_template( 'template-submit-deal.php' ) ) {
			$show_search = false;
		} else {
			$show_search = couponhut_get_field('search_header') == 'show' ? true : false;
		}	
		break;
}

if ( is_archive()  ) {
	$show_search = isset($search_screen['archive']) ? true : false;
}

if ( is_search() ) {
	$show_search = isset($search_screen['search']) ? true : false;
}

if (
	is_archive('product') 
	|| is_product() 
	|| is_home() 
	|| is_front_page() 
	|| is_page('whats-new') 
	|| is_page('above-30-off') ) {
	get_template_part( 'templates/module', 'wishlist' );
}



// if ( $show_search ) {
// 	get_template_part( 'partials/content', 'header-screen' );
// }

