(function($) {
    function insertParam(key, value) {
        key = encodeURI(key);
        value = encodeURI(value);

        var kvp = document.location.search.substr(1).split('&');


        var i = kvp.length;
        var x;
        while (i--) {
            x = kvp[i].split('=');

            if (x[0] == key) {
                x[1] = value;
                kvp[i] = x.join('=');
                break;
            }
        }

        if (i < 0) { kvp[kvp.length] = [key, value].join('='); }

        if (document.location.search.substr(1).indexOf('s') == -1) {
            kvp[kvp.length] = ['s', ''].join('=');
        }

        if (document.location.search.substr(1).indexOf('post_type') == -1) {
            kvp[kvp.length] = ['post_type', 'product'].join('=');
        }
        var kvp2 = [];
        for (var i = 0; i < kvp.length; i++) {
            if (kvp[i] != '') {
                kvp2.push(kvp[i]);
            }
        }
        kvp = kvp2;
        //this will reload the page, it's likely better to store this until finished

        // console.log(document.location.search);
        location.replace(HappyDeals.base_url + '?' + kvp.join('&'));
        // document.location.search = kvp.join('&');
    }


    $(document).ready(function() {

        function positionCart() {
            var $el = $('.navigation-wrapper'); //record the elem so you don't crawl the DOM everytime
            var $el2 = $('#bravowp-woo-floatingcart');
            var top = $el.position().top + $el.outerHeight(true);
            $('#cart-right-display .filter-container').css({
                top: top + 'px'
            });
        }


        $(window).scroll(positionCart()).trigger('scroll');



        $('body').on('click', function(e) {
            if (!$(e.target).closest('#searchform').length) {
                $('.header-search_result, .header-search_suggestions').hide();
            }
        });

        $('#submitSearchButton').on('click', function() {
            $("#searchform").submit();
        });
        var typingTimer;
        var doneTypingInterval = 1000;

        $("#deal_search").on('focus', function(e) {
            var $self = $(this);
            if ($self.val().length == 0) {
                $('.header-search_result').hide();
                $('.header-search_suggestions').show();

            }
        });
        $("#deal_search").on('keyup', function(e) {
            var $self = $(this);
            var term = $self.val();
            var $searchForm = $('#searchform');
            // $searchForm.remove('.header-search_suggestion-menu');
            clearTimeout(typingTimer);
            typingTimer = setTimeout(function() {
                //Call ajax when string length is more than 3 character
                if (term.length >= 1) {
                    $.ajax({
                        url: HappyDeals.ajaxurl,
                        type: 'POST',
                        data: { s: term, action: 'hd-advancea-search' },
                        success: function(response) {
                            $('.header-search_suggestions').hide();
                            $('.header-search_result').html(response);
                            $('.header-search_result').show();
                        },
                        error: function(a, b, c) {

                        }

                    });
                } else if (term.length == 0) {
                    $('.header-search_result').hide();
                    $('.header-search_suggestions').show();
                }
            }, doneTypingInterval);

        }).on('keydown', function() {
            clearTimeout(typingTimer);
        });

        $('#clearSearchHistory').on('click', function(e) {
            e.preventDefault();

            $.ajax({
                url: HappyDeals.ajaxurl,
                type: 'POST',
                data: { action: 'clear-search-history' },
                success: function(response) {
                    $('#searchSuggestions').empty();
                }
            })
            return false;
        });


        $('.filter-companies .form-control-checkbox input[type=checkbox]').on('change', function() {
            var $this = $(this);
            var val = [];
            $('.filter-companies .form-control-checkbox input[type=checkbox]:checked').each(function() {
                val.push($(this).val());
            })
            insertParam('comp', val.join('~'));
        });

        $('.filter-payment-shipping-options .form-control-checkbox input[type=checkbox]').on('change', function() {
            var $this = $(this);
            var val = [];
            $('.filter-payment-shipping-options .form-control-checkbox input[type=checkbox]:checked').each(function() {
                val.push($(this).val());
            })
            insertParam('pdo', val.join('~'));
        });

        jQuery.expr[':'].contains = function(a, i, m) {
            return jQuery(a).text().toUpperCase()
                .indexOf(m[3].toUpperCase()) >= 0;
        };


        $('#searchCompany').on('keyup', function() {
            var val = $(this).val();
            if (val.length >= 1) {
                $('.filter-companies li.form-control-checkbox > label > span:not(:contains("' + val + '"))').closest('li').hide();
            } else {
                $('.filter-companies li.form-control-checkbox').show();
            }
        });
        var min = parseFloat(HappyDeals.filter.price.min);
        var max = parseFloat(HappyDeals.filter.price.max);
        var valueMin = parseFloat($("#filterPriceFrom").val() > 0 ? $("#filterPriceFrom").val() : HappyDeals.filter.price.min);
        var valueMax = parseFloat($("#filterPriceTo").val() > 0 ? $("#filterPriceTo").val() : HappyDeals.filter.price.max);
        $("#slider-range-min").slider({
            range: true,
            min: min,
            max: max,
            values: [valueMin, valueMax],
            slide: function(event, ui) {
                $("#filterPriceFrom").val(ui.values[0]);
                $("#filterPriceTo").val(ui.values[1]);
            }
        });
        $("#submitFilterPriceRange").on('click', function() {
            var val = $('#filterPriceFrom').val() + '~' + $('#filterPriceTo').val();
            insertParam('price', val);
        });

        $('#filterBadge a').on('click', function(e) {
            e.preventDefault();
            positionCart();
            $('.sidebar-filter').toggleClass('open');
            return false;
        });

        // SEt the height of search wrapper
        $searchResultWrapper = $('.search-results-wrapper');
        if ($searchResultWrapper.length) {
            $searchResultWrapper.height($('.sidebar-filter .filter-container').outerHeight() + 20);
        }

        $(window).scroll(checkscroll);

        function checkscroll() {
            var top = $(window).scrollTop();
            var my_div = $("#cart-right-display");
            if (my_div.length) {
                var target_top = my_div.offset().top;

                if (top > target_top - 200) {

                    $('.sidebar-filter, #wishlist-container').show();
                } else {
                    $('.sidebar-filter, #wishlist-container').hide();
                }
            }
        }
        checkscroll();


    });
})(jQuery);