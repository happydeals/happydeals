jQuery( document ).on( 'change', '.woocommerce-cart-form input.qty', function() {
        var item_hash = jQuery( this ).attr( 'name' ).replace(/cart\[([\w]+)\]\[qty\]/g, "$1");
        var item_quantity = jQuery( this ).val();
        var currentVal = parseFloat(item_quantity);

        function qty_cart() {
		     
            jQuery.ajax({
                type: 'POST',
                url: cart_qty_ajax.ajax_url,
                data: {
                    action: 'qty_cart',
                    hash: item_hash,
                    quantity: currentVal
                },
                success: function(data) {              
                   jQuery( '#cart_total' ).html(data);
                }
            });  

        }

        qty_cart();
        
        
    

    });        
    
    
     jQuery( document ).on( 'click', '.remove_product', function(e) {  
    	e.preventDefault();
	var item_hash = jQuery(this).attr("rel");
      	bravowp_woo_floatingcart_displayloadingimage();
      	jQuery.ajax({
                type: 'POST',
                url: cart_qty_ajax.ajax_url,
                data: {
                    action: 'remove_product_cart',
                    hash: item_hash,
        
                },
                success: function(data) {              
                   jQuery( '#cart_items_table' ).html(data);
                   update_subtotal();  
                }
         });  
      	
      
    }); 
    
   jQuery( document ).on( 'click', '.variation_add', function(e) { 
       
       e.preventDefault();
       update_item_deliveryoption();
       jQuery('.single_add_to_cart_button').trigger("click");
       
       
   });  
   
   jQuery( document ).on( 'click', '#product_add_a', function(e) { 
       
       e.preventDefault();
       update_item_deliveryoption();
      
   });  
   
   
          
           
 
 function update_item_deliveryoption() {
 	    var pid_val = jQuery('#pid_o').val();
 	    var delivery_option_val = '';
 	    var store_branch_val = '';
 	    var variation_id_val = '';
 	    if (jQuery('.delivery_option').length > 0 ) {
	          delivery_option_val = jQuery('.delivery_option:checked').val();
	     }
	     
	     if (jQuery('.store_branch').length > 0 ) {
	     	if (jQuery('.store_branch').val() != 'Choose branch') {
	          store_branch_val = jQuery('.store_branch').val();
	        }
	     }
	     
	      if (jQuery('.variation_id').length > 0 ) {
	          variation_id_val = jQuery('.variation_id').val();
	     }
	     
	     
 	   jQuery.ajax({
                type: 'POST',
                url: cart_qty_ajax.ajax_url,
                data: {
                    action: 'update_item_deliveryoptions',
                    pid: pid_val,
                    delivery_option: delivery_option_val,
                    store_branch: store_branch_val,
                    variation_id: variation_id_val
                    
                }
            });  
 
 }  
 
 function update_subtotal() {
           jQuery.ajax({
                type: 'GET',
                url: cart_qty_ajax.ajax_url,
                data: {
                    action: 'update_subtotal_cart'
                },
                success: function(data) {              
                   jQuery( '#cart_total' ).html(data);
                   jQuery('#bravowp-woo-floatingcart-loader').hide();
                }
            });  

 }      
 
 
 /* FAVORITES */
 
jQuery( document ).ready(function($) {
     display_myfavorites(); 
     
         /*CAROUSEL*/
  var sync1 = $("#sync1");  
  var sync2 = $("#sync2");  
  var slidesPerPage = 4; //globaly define number of elements per page
  var syncedSecondary = true;

  sync1.owlCarousel({
    items : 3,
    slideSpeed : 2000,
    nav: true,
    autoplay: true,
    dots: true,
    loop: true,
    autoplay: true,
    autoplayHoverPause: true,
    autoplayTimeout: 5000,
    responsiveClass:true,
    responsiveRefreshRate : 200,
   navText: [
      "<i class='fa fa-chevron-left'></i>",
      "<i class='fa fa-chevron-right'></i>"
      ],
  }).on('changed.owl.carousel', syncPosition);     
  
  sync2.owlCarousel({
    items : 3,
    slideSpeed : 2000,
    nav: true,
    autoplay: true,
    dots: true,
    loop: true,    
    autoplay: true,  
    autoplayHoverPause: true,
    autoplayTimeout: 5000,
    responsiveClass:true,
    responsiveRefreshRate : 200,
   navText: [
      "<i class='fa fa-chevron-left'></i>",
      "<i class='fa fa-chevron-right'></i>"
      ],
  }).on('changed.owl.carousel', syncPosition2);
    

 
  function syncPosition(el) {
    //if you set loop to false, you have to restore this next line
    //var current = el.item.index;
    
    //if you disable loop you have to comment this block
    var count = el.item.count-1;
    var current = Math.round(el.item.index - (el.item.count/2) - .5);
    
    if(current < 0){
      current = count;
    }
    if(current > count){
      current = 0;
    }
    
    //end block
  }      
  
   function syncPosition2(el) {
    //if you set loop to false, you have to restore this next line
    //var current = el.item.index;
    
    //if you disable loop you have to comment this block
    var count = el.item.count-1;
    var current = Math.round(el.item.index - (el.item.count/2) - .5);
    
    if(current < 0){
      current = count;
    }
    if(current > count){
      current = 0;
    }
    
    //end block
  }
      
  /*END CAROUSEL*/
  
  if ($('.delivery_option').length > 0 ) {
  	var delivery_option_val = $('.delivery_option:checked').val();
        display_branch(delivery_option_val);
        
	$('.delivery_option').change(function() {
		
		var delivery_option_val = $(this).val();
		display_branch(delivery_option_val);
    	});
  
  }
  
  
 function display_branch(delivery_option_val) {

 	if ($('.store_branch').length > 0 && delivery_option_val == 'pickup' ) {
	  $('#store_branch_wrapper').show();
	} else {
	  $('#store_branch_wrapper').hide();
	}  
 } 
        
}); 


jQuery(document).on('click', '.remove-fav-link', function (e) {
         e.preventDefault();
         var remove_link = $(this).attr("href");
	 var r = confirm("Are you sure you want to remove this item?");
	 if (r == true) {
	      jQuery.ajax({
	        type: 'GET',
	        url:  remove_link,
	        success: function(data) {              
	            display_myfavorites();
	        }
	     });  
	} 
});


jQuery(document).on('change', '.pp_cart .qty', function($) {
   	$('#product_add_a').attr('data-quantity',$(this).val()) ;
});

    
    
function display_myfavorites() {
	 jQuery.ajax({
                type: 'GET',
                url: cart_qty_ajax.ajax_url,
                data: {
                    action: 'update_favorites_container'
                },
                success: function(data) {              
                   jQuery( '#myfavorites_container' ).html(data);
                }
        });  	
}



/* END FAVORITES */