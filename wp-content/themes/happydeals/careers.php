<?php
/**
 Template Name: Careers
 */
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */
// set_query_var( 'show_form', true ); 
get_header(); 
// if (!is_user_logged_in()) {
//         $counter = 0;
// } else {
//         $favorite_post_ids = wpfp_get_user_meta();
//         $counter = count($favorite_post_ids);
// }
while(have_posts()): the_post();
?>

<div class="section section-main-banner section-main-banner-career" style='background-image: url("<?php the_field('main_banner_background'); ?>"); padding-left: 0; padding-right: 0;'>
	<div class="container">
		<div class="col-md-6">
			<h1><?php the_field('main_banner_heading'); ?></h1>
			<?php the_field('main_banner_sub-heading'); ?>
		</div>
	</div>	
</div>
<div class="section section-main-content section-careers-main-content">
	<div class="container">
		<?php the_content(); ?>
	</div>	
</div>
<?php 
endwhile;
get_footer();?>
