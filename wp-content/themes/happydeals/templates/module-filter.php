<?php 

    if (is_archive()) {
        $obj = get_queried_object();
        $cat = $obj->slug;
    } else {
        $cat = isset($_REQUEST['cat']) ? $_REQUEST['cat'] : false;    
    }
    
    $comp = isset($_REQUEST['comp']) ? $_REQUEST['comp'] : '';
    $pdo = isset($_REQUEST['pdo']) ? $_REQUEST['pdo'] : '';
    $price = isset($_REQUEST['price']) ? $_REQUEST['price'] : '';


    $cat_ancestors = array();
    $category = 0;
    if ($cat) {
        $category = get_term_by('slug', $cat, 'product_cat')->term_id;
        $cat_ancestors = get_ancestors($category, 'product_cat');
        
    }

    $selected_companies = array();
    if ($comp != '') {
        $selected_companies = explode('~', $comp);
    }

    $selected_pdo = array();
    if ($pdo != '') {
        $selected_pdo = explode('~', $pdo);
    }


    $prices = array(0, 0);
    if ($price != '') {
        $prices = explode('~', $price);
    }

        

?>
<div class="sidebar-filter <?php echo $cat || $comp || $pdo || $price ? 'open' : ''; ?>">
		<div class="filter-badge" id="filterBadge">
			<div class="badge-filter">
				<a href="javascript:void(0);">F<br/>i<br/>l<br/>t<br/>e<br/>r</a>
			</div>
		</div>
		<div class="filter-container">
			<div class="filter filter-categories">
				<h4><?php _e("Categories", "happydeals"); ?></h4>
				<?php
					echo '<ul>';
					echo get_category_thread(0, $category, $s);
					echo '</ul>';
				?>
			</div>
			<div class="filter filter-companies">
				<h4><?php _e("Companies", "happydeals"); ?></h4>	
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" id="searchCompany" class="form-control">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
				</div>
				
				<?php 
				$categories = get_terms(array('taxonomy' => 'deal_company', 'parent' => 0));
				if ($categories) {
					echo '<ul>';
					foreach ($categories as $term )
					{
						$count = count_category_posts_by_search($term->term_id, $s);
						$checked = in_array($term->slug, $selected_companies) ? 'checked' : '';
						echo '<li class="form-control-checkbox"><label for="company'. $term->slug .'"><input type="checkbox" name="company'. $term->slug .'" id="company'. $term->slug .'" value="'. $term->slug .'" '.$checked.'><span>'. $term->name .' ('. $count .')</span></label></li>';
					}
					echo '</ul>';
				}
				?>
			</div>
			
			<div class="filter filter-price">
				<h4>Price (IN <?php echo get_woocommerce_currency_symbol(); ?>)</h4>
				<div id="slider-range-min"></div>
				<div class="range-input-group">
					<div class="sldr__input-min">
						<div class="input-group">
							<input type="number" id="filterPriceFrom" value="<?php echo $prices[0]; ?>">
						</div>
					</div>
					<div class="sldr__input-divider">-</div>
					<div class=" sldr__input-max">
						<div class="input-group">
							<input type="number" id="filterPriceTo" value="<?php echo $prices[1]; ?>">
							<span id="submitFilterPriceRange" class="input-group-addon"><i class="glyphicon glyphicon-triangle-right"></i></span>
						</div>
					</div>
				</div>
				
			</div>
			<div class="filter filter-payment-shipping-options">
				<h4><?php _e("Payment & Delivery Option"); ?></h4>
				<ul>
				<?php 
				$pd_options = array(
					'shiptome' => array('meta_key' => '_wc_local_pickup_plus_local_pickup_product_availability', 'meta_value' => 'disallowed', 'title' => __("Ship To Me", "happydeals")),
					'local_pickup' => array('meta_key' => '_wc_local_pickup_plus_local_pickup_product_availability', 'meta_value' => 'required', 'title' => __("Local pick-up", "happydeals")),
					'cod' => array('meta_key' => 'eligible_for_cash_on_delivery', 'meta_value' => '1', 'title' => __("COD", "happydeals")),
					// 'installment' => array('meta_key' => 'woo_inst_tiers_list', 'title' => __("Installment", "happydeals")),
				);
				foreach ($pd_options as $k => $v) {
					$checked = in_array($k, $selected_pdo) ? 'checked' : ''; 
					echo '<li class="form-control-checkbox"><label for="company'. $k .'"><input type="checkbox" name="company'. $k .'" id="company'. $k .'" value="'. $k .'" '.$checked.'><span>'. $v['title'] .' ('. count_payment_delivery_options($v['meta_key'], $v['meta_value'], $s) .')</span></label></li>';
				}
				?>
				</ul>
			</div>
		</div>
	</div>