<?php
get_header();

get_template_part( 'content', 'sliders' );

?>



<div id="cart-right-display" class="search-results-wrapper">

<?php 
	get_template_part( 'templates/module', 'filter' ); 
	/**
	*  Posts Loop
	*/
	if (have_posts()) : ?>

	<div class="grid-wrapper is-ajax-deals-content">
		<div class="container">
		<?php while  (have_posts() ) : the_post(); ?>

			<div class="deal-item-wrapper col-xs-6 col-sm-6 col-md-4">
				<?php get_template_part('loop/content', 'deal'); ?>
			</div>
				
		<?php
		endwhile;
		?>
		</div>
	</div><!-- end grid-wrapper -->

	<?php
	// Pagination
	// $ajax = fw_ssd_get_option('ajax-switch');
	// fw_ssd_paging_nav($deals_query, $ajax);

	wp_reset_postdata();

	else :
	?>
	<h2 class="text-center"><?php esc_html_e('No results found.', 'couponhut'); ?></h2>
<?php
endif; //have_posts()
?>
</div>


<?php
get_footer();
