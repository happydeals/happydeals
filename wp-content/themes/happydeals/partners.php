<?php
/**
 Template Name: Our Partners
 */
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */
set_query_var( 'show_form', true ); 
get_header(); 


while(have_posts()): the_post();

    if (!is_user_logged_in()) {
            $counter = 0;
    } else {
            $favorite_post_ids = wpfp_get_user_meta();
            $counter = count($favorite_post_ids);
    }

?>

<div class="section section-main-banner" style='background-image: url("/images/partners.jpg"); padding-left: 0; padding-right: 0;'>
	<div class="container">
		<h1><?php the_title(); ?></h1>
		<?php the_content(); ?>
	</div>	
</div>
<div class="section section-main-content">
	<div class="container">
        <div class="row">
            <div id="tabs-container">
                <h2>Our ambassadors of happiness can be seen below:</h2>
                <ul class="tabs-menu">
                    <li class="current"><a href="#tab-1">Dining</a></li>
                    <li><a href="#tab-2">Hotels</a></li>
                    <li><a href="#tab-3">Get Aways</a></li>
                    <li><a href="#tab-4">Beauty & Wellness</a></li>
                </ul>
            </div>
	    </div>
        <div class="row">
        <div class="col-md-6">
            <div class="tab">


                <div id="tab-1" class="tab-content">
                    <?php
                        $args = array( 'post_type' => 'product',  'product_cat' => 'dining', 'orderby' => 'rand' );
                        $loop = new WP_Query( $args );
                        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
                        
                
                                        <p style="margin-bottom: 10px !important;"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></p> </a>
                

                    <?php endwhile; ?>
                    <?php wp_reset_query(); ?>

                </div>


                <div id="tab-2" class="tab-content">
                    <?php
                        $args = array( 'post_type' => 'product', 'product_cat' => 'hotels', 'orderby' => 'rand' );
                        $loop = new WP_Query( $args );
                        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
                        
                
                                        <p style="margin-bottom: 10px !important;"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></p> </a>
                

                    <?php endwhile; ?>
                    <?php wp_reset_query(); ?>
                
                </div>
                <div id="tab-3" class="tab-content">
                    <?php
                        $args = array( 'post_type' => 'product', 'product_cat' => 'getaways', 'orderby' => 'rand' );
                        $loop = new WP_Query( $args );
                        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
                        
                
                                        <p style="margin-bottom: 10px !important;"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></p> </a>
                

                    <?php endwhile; ?>
                    <?php wp_reset_query(); ?>
                </div>
                <div id="tab-4" class="tab-content">
                    <?php
                        $args = array( 'post_type' => 'product', 'product_cat' => 'beauty-and-wellness', 'orderby' => 'rand' );
                        $loop = new WP_Query( $args );
                        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
                        
                
                                        <p style="margin-bottom: 10px !important;"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></p> </a>
                

                    <?php endwhile; ?>
                    <?php wp_reset_query(); ?>
                </div>




                    </div>

            </div>
        </div>
        </div>
    </div>
    </div>
</div>


  <script>
  $( function() {
    $( "#tabs" ).tabs();
  } );
  </script>



<style type="text/css">
  


  .tabs-menu {
      height: 30px;
      float: left;
      clear: both;

  }

  .tabs-menu li {
      height: 30px;
      line-height: 30px;
      float: left;
      margin-right: 10px;
      color: #45BB99;
      /*background-color: #ccc;*/
  /*      border-top: 1px solid #d4d4d1;
      border-right: 1px solid #d4d4d1;*/
      border-bottom: 1px solid #45BB99;
      width: 220px;
  }

  .tabs-menu li.current {
      position: relative;
      background-color: #fff;
      border-bottom: 1px solid #000;
      width: 220px;
      z-index: 5;
  }

  .tabs-menu li a {
      padding: 10px;
      text-transform: uppercase;
      color: #45BB99;
      text-decoration: none; 

  }

  .tabs-menu .current a {
      color: #45BB99;
  }

  .tab {
      /*border: 1px solid #d4d4d1;*/
      background-color: #fff;
      float: left;
      margin-bottom: 20px;
      width: auto;
      color: #45BB99;
  }

  .tab-content {
      /*width: 660px;*/
      padding: 20px;
      display: none;
      
  }
  .tab-content p a{
  color: #45BB99;

  }

  #tab-1 {
   display: block;   
  }

</style>


<script type="text/javascript">
  $(document).ready(function() {
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
    });
});
</script>

<?php
endwhile;
get_footer();?>