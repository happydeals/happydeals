<?php
/**
 Template Name: why partner with us
 */
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); 


while(have_posts()): the_post();
?>
<div class="section section-main-banner why-partner-with-us-banner" style='background-image: url("<?php the_field('main_banner_background'); ?>"); padding-left: 0; padding-right: 0;'>
	<div class="container">
		<h1><?php the_field('main_banner_heading'); ?></h1>
		<?php the_field('main_banner_sub-heading'); ?>
	</div>	
</div>
<div class="section section-main-content">
	<div class="container">
		<?php the_content(); ?>
	</div>
</div>
<div class="section section-contact-banner text-center"  style='background-image: url("<?php the_field('contact_us_banner_background'); ?>"); padding-left: 0; padding-right: 0;'>
	<div class="container">
		<div class="col-md-10 col-offset-1">
			<h2><?php the_field('contact_us_banner_heading'); ?></h2>
			<h1><?php the_field('contact_us_banner_title'); ?></h1>
		</div>
	</div>
</div>
<div class="section section-contact-form">
		<div class="container">
			<?php echo do_shortcode(get_field('contact_form_shortcode')); ?>
		</div>
</div>
<div class="section section-contact-info">
	<div class="container">
		<hr/><br/>
		<h1> Take your brand further </h1>
		<div class="row">
			<div class="col-md-6">
				<h3>Tel No: <?php the_field('tel_no'); ?></h3>
			</div>
			<div class="col-md-6">
				<h3>Email Address: <?php the_field('email_address'); ?></h3>
			</div>
		</div>
	</div>
</div>

<?php 
endwhile;
get_footer();?>