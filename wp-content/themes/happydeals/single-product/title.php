<?php
/**
 * Single Product title
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/title.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author     WooThemes
 * @package    WooCommerce/Templates
 * @version    1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $product;
$companies = get_the_terms( $product->id, 'deal_company' );
$stocks = get_post_meta( $product->id, '_stock', true );
$validity = 'Valid now';
if (get_field('product_validity')) {
        $validity = get_field('product_validity');
} 

?>

<?php 
if (product_isdeal($product->id)) {
        if ($product->is_in_stock()) { ?>
        <div class="product-info-more">
            <span style="text-transform:uppercase; max-width: 150px; min-width: 100px;"><?php echo $validity; ?></span><span style="width: 80px;"><?php echo number_format($stocks);  ?> LEFT</span>
        </div>
<?php } 

} else if (!$product->is_in_stock()) { ?>
    <div class="product-info-more">
        <span style="color: red; text-transform:uppercase; width: 100px;">Out of stock</span>   
     </div>
<?php
}
the_title( '<h1 class="product_title entry-title">', '</h1>' );
?>
<div class="product_company">
    <h2><?php  echo wp_kses_post($companies[0]->name); ?></h2>
    	<p>
	<?php echo wp_kses_post($companies[0]->description); ?>
	</p>
</div>

