<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;


if ($product->is_on_sale()) {
$percentage = round( ( ( $product->regular_price - $product->sale_price ) / $product->regular_price ) * 100 );
?><?php if (!$product->has_child()) { ?>
<div class="discount-details">
    <ul class="row">
        <li class="col-sm-4 nopadding">Value<br/><?php echo wc_price( $product->get_regular_price() ); ?></li>
        <li class="col-sm-4 nopadding">Discount<br/><?php echo $price . sprintf( __('%s', 'woocommerce' ), $percentage . '%' );  ?></li>
        <li class="col-sm-4 nopadding">Discount<br/><?php echo  wc_price($product->get_regular_price() - $product->get_sale_price());  ?></li>
    </ul>
</div>
<?php }} ?>


