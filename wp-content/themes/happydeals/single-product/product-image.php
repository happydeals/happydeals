<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post, $product;
$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
$thumbnail_size    = apply_filters( 'woocommerce_product_thumbnails_large_size', 'full' );
$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
$full_size_image   = wp_get_attachment_image_src( $post_thumbnail_id, $thumbnail_size );
$placeholder       = has_post_thumbnail() ? 'with-images' : 'without-images';
$wrapper_classes   = apply_filters( 'woocommerce_single_product_image_gallery_classes', array(
	'woocommerce-product-gallery',
	'woocommerce-product-gallery--' . $placeholder,
	'woocommerce-product-gallery--columns-' . absint( $columns ),
	'images',
) );
$add_class = '';
if (product_isdeal($product->id)) {
    $thumbnail = 'shop_single';   
     
} else {
    $thumbnail = 'goods_thumbnail';
    $add_class = 'shop_goods';        
}

?>     
<div class="<?php echo esc_attr( implode( ' ', array_map( 'sanitize_html_class', $wrapper_classes ) ) ); ?> <?php echo $add_class; ?>" data-columns="<?php echo esc_attr( $columns ); ?>" style="opacity: 0; transition: opacity .25s ease-in-out; width: 100%;">      
	<figure class="woocommerce-product-gallery__wrapper">
		<?php
		$attributes = array(
			'title'                   => get_post_field( 'post_title', $post_thumbnail_id ),
			'data-caption'            => get_post_field( 'post_excerpt', $post_thumbnail_id ),
			'data-src'                => $full_size_image[0],
			'data-large_image'        => $full_size_image[0],
			'data-large_image_width'  => $full_size_image[1],
			'data-large_image_height' => $full_size_image[2],
		);   
                
            if (product_isdeal($product->id)) {  
                $attachment_ids = $product->get_gallery_image_ids();
            ?>    
                <div class="flexslider">
                  <ul class="slides">
                        <?php if ( has_post_thumbnail() ) { ?>
                            <li>
                              <img src="<?php echo  get_the_post_thumbnail_url( $post->ID, 'shop_single' ); ?>" />
                            </li>
                         <?php } ?>
                         
                         <?php if ( $attachment_ids && has_post_thumbnail() ) { 
                                        foreach ( $attachment_ids as $attachment_id ) {
                                	$thumbnail = wp_get_attachment_image_src( $attachment_id, 'shop_single' ); ?>
                                        <li>
                                         <img src="<?php echo $thumbnail[0]; ?>" />
                                        </li>
                                        <?php
                                          }
                                } ?>
                  </ul>
                </div>
                <?php } else { ?>
               <?php
		if ( has_post_thumbnail() ) { 
                 $attachment_ids = $product->get_gallery_image_ids();
                 $add_class2 = '';
                 if (count($attachment_ids) > 0) {
                        $add_class2 = ' class="hasgallery" ';
                 }
                 $small_size_image = wp_get_attachment_image_src(  $post->ID, 'shop_goods' );
                 
		        $html .= '<div style="width:412px; margin-left: 130px;">';	
			$html .= '<img id="zoom_01" src="'.get_the_post_thumbnail_url( $post->ID, $thumbnail ) .'" data-zoom-image="' . esc_url( $full_size_image[0] ) . '" /><div id="gallery_01" '.$add_class2.'>'; 
                        $html .= '<a href="#" data-image="'.get_the_post_thumbnail_url( $post->ID, $thumbnail ).'" data-zoom-image="' . esc_url( $full_size_image[0] ) . '"><img id="zoom_01" src="'.get_the_post_thumbnail_url( $post->ID, 'shop_thumbnail' ) .'"/></a>'; 
		
		} else {
			$html  = '<div class="woocommerce-product-gallery__image--placeholder">';
			$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src() ), esc_html__( 'Awaiting product image', 'woocommerce' ) );
			$html .= '</div>';
		}

		echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, get_post_thumbnail_id( $post->ID ) );

		do_action( 'woocommerce_product_thumbnails' );
                
                }
		?>
	</figure>
</div>
