<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

echo wc_get_stock_html( $product );

if ( $product->is_in_stock() ) : ?>

	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="cart pp_cart" method="post" enctype='multipart/form-data'>
		<?php
			/**
			 * @since 2.1.0.
			 */
			do_action( 'woocommerce_before_add_to_cart_button' );

			/**
			 * @since 3.0.0.
			 */
			do_action( 'woocommerce_before_add_to_cart_quantity' );

	
			/**
			 * @since 3.0.0.
			 */
		      do_action( 'woocommerce_after_add_to_cart_quantity' );
		?>

		<!--<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>-->
                <div class="row">
                
                
                
                        <?php 
                                $oldPrice = $product->get_regular_price();
                                $price =  $product->get_price(); 
                                $onsale_add = '';
                         ?>
                       
                </div>
                <div class="col-sm-8" style="padding: 10px 0 0 0;">
                   <?php 
                   
                   if ($product->is_on_sale()) { 
                        $onsale_add = ' onsale';
                   ?>    
                         <p class="price" style="margin-bottom: 10px; text-align: right;">
                                <del><span class="woocommerce-Price-amount amount"><?php echo wc_price($oldPrice); ?></del>
                         </p>
                   
                   <?php } ?> 
                
                   <p class="price" style="text-align: right"><span class="woocommerce-Price-amount amount"><?php echo wc_price( $price ); ?></span></p>  
                </div>
                <div class="col-sm-4 nopadding <?php echo $onsale_add; ?>">
                <?php echo sprintf( '<a href="%s" id="product_add_a" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-quantity="%s" class="button %s product_type_%s">%s</a>',
														        esc_url( '/?add-to-cart='.$product->get_id() ),
														        esc_attr( $product->get_id() ),
														        '',
														        esc_attr( '1' ),
														        'add_to_cart_button ajax_add_to_cart img-btn',
														        esc_attr( 'simple' ),
														        '<img src="'. get_stylesheet_directory_uri().'/assets/images/buynow_btn.png">'
														    );
														    ?>	
               </div>
               </div>   
		<?php
			/**
			 * @since 2.1.0.
			 */
			do_action( 'woocommerce_after_add_to_cart_button' );
		?>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
        

<?php endif; ?>




