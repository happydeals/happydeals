<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
$payment_terms = get_field('payment_information');
$contact_details = get_field('contact_us'); 
?>
<div class="product_meta">      
<div class="product_info_toc">
        <?php echo $payment_terms; ?>
        <?php if (!product_isdeal($product->id)) { ?>
        <p><a href="/terms-and-conditions/" target="_blank" class="toc_r">Read Terms & Conditions</a></p>
        <?php } ?>
       
</div>

<div class="product_info_contact">
        <h3>Contact Info</h3>
        <?php echo $contact_details ?>
</div>
</div>
