<?php
$s = isset($_REQUEST['s']) ? $_REQUEST['s'] : '';
$cat = isset($_REQUEST['cat']) ? $_REQUEST['cat'] : false;
$comp = isset($_REQUEST['comp']) ? $_REQUEST['comp'] : '';
$pdo = isset($_REQUEST['pdo']) ? $_REQUEST['pdo'] : '';
$price = isset($_REQUEST['price']) ? $_REQUEST['price'] : '';

$args = array(
	'post_type' => 'product',
	
);



$deals_query = new WP_Query($args); // The Query
?>
<div id="cart-right-display" class="search-results-wrapper">

	<?php 
		get_template_part( 'templates/module', 'filter' ); 
	?>

	<?php 

		/**
		*  Posts Loop
		*/
		if ($deals_query->have_posts()) : ?>

		<div class="grid-wrapper is-ajax-deals-content">
			<div class="container">
			<?php while  ( $deals_query->have_posts() ) : $deals_query->the_post(); ?>

				<div class="deal-item-wrapper col-xs-6 col-sm-6 col-md-4">
					<?php get_template_part('loop/content', 'deal'); ?>
				</div>
					
			<?php
			endwhile;
			?>
			</div>
		</div><!-- end grid-wrapper -->

		<?php
		// Pagination
		$ajax = fw_ssd_get_option('ajax-switch');
		fw_ssd_paging_nav($deals_query, $ajax);

		wp_reset_postdata();

		else :
		?>
		<div class="grid-wrapper is-ajax-deals-content"></div>
		<div class="ajax-deals-notice">
			<h3><?php esc_html_e('No deals found.', 'couponhut'); ?></h3>
		</div>
	<?php
	endif; //have_posts()
	?>
</div>