<!-- Image Thumbnail -->

<?php 
global $post;
$_product = wc_get_product( $post->ID );
$companies = get_the_terms( $post->ID, 'deal_company' );
$qty = $_product->get_stock_quantity();
if ( has_post_thumbnail() ) : ?>
	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"> </a>
	<div class="deal-thumb-image" class="img-responsive"> 
		<div class="prices-bg" style="height: 100%; width: 100%;background-repeat: no-repeat;"> 
			<div class="prices">
				<?php 
				$oldPrice = $_product->get_regular_price();
				$price =  $_product->get_price(); 


				if ($_product->is_on_sale()): ?>
					<p><b>(from </b><?php echo wc_price($oldPrice); ?></b></p>
					<h1><?php echo wc_price($price); ?></h1>
				<?php else: ?>
					<b><h2><?php echo wc_price($price); ?></h2></b>
				<?php endif; ?>
			</div>
		</div><!-- Image -->

		<?php the_post_thumbnail('product-list-size')?>


		<!-- End Image --> 
		<div class="overlay" style=" background-image: url('<?php echo home_url() . '/hover.png'; ?>');">
			<div class="text2" style="display: inline-block;">
			<ul style="margin-top: 25px;">
				<li style="padding-left: 0;"><a href="<?php the_permalink(); ?>"><img src="<?php echo home_url() . '/icon/search.png'; ?>"></a>
				<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><img src="<?php echo home_url() . '/icon/share.png'; ?>"></a></li> 
				<li><a href="<?php the_permalink(); ?>"><?php wpfp_link() ?></a></li>
				<li>
				<?php echo sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-quantity="%s" class="button %s product_type_%s">%s</a>',
				esc_url( '/?add-to-cart='.$post->ID ),
				esc_attr( $post->ID ),
				'',
				esc_attr( '1' ),
				'add_to_cart_button ajax_add_to_cart',
				esc_attr( 'simple' ),
				'<img src="'. home_url() . '/icon/addtocart.png">'
				);
				?>	
				</li>
			</ul> 

		</div>
	</div>

</div>
<!-- End Price -->
<?php endif; ?>
<div class="product-desc-f"> 
	<?php if ($companies) :?>
	<p class="mname"><?php  echo wp_kses_post($companies[0]->name); ?></b></p>
	<?php endif; ?>
	<!-- Get Title -->
	<p><a href="<?php the_permalink();?>"><?php the_title(); ?></a></p>
	<div class="col-sm-8 nopadding">
		<!-- Rating -->    
		<?php echo do_shortcode('[WPCR_SHOW POSTID="'.$post->ID.'" SHOWFORM="0" HIDEREVIEWS="1" HIDERESPONSE="1"]'); ?>
	</div>
	<?php if ($qty): ?>
		<div class="col-sm-4 nopadding" style="text-align: right;">
			<!-- Stocks Let -->  
			<span class="stocks-left-f"><?php echo  $qty . ' LEFT';  ?></span>
			<!-- Stocks Let End-->          
		</div>
	<?php endif; ?>
</div>
<!-- Get Category -->






