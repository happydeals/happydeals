<?php
/*
Template name: Browse Deals
*/
get_header();
?>

<?php 

		get_template_part( 'content', 'sliders' );

	?>

<!-- Best Deals Section -->

<div>

	<?php 
	/**
	*  Posts Loop
	*/
	get_template_part( 'loop/loop', 'deals' );
	?>

</div>

<?php
get_footer();
