<?php
/**
 Template Name: about us
 */
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */
// set_query_var( 'show_form', true ); 
get_header(); 
// if (!is_user_logged_in()) {
//         $counter = 0;
// } else {
//         $favorite_post_ids = wpfp_get_user_meta();
//         $counter = count($favorite_post_ids);
// }
while(have_posts()): the_post();
?>

<div class="section section-main-banner section-main-banner-about-us" style='background-image: url("<?php the_field('main_banner_background'); ?>"); padding-left: 0; padding-right: 0;'>
	<div class="container">
		<div class="col-md-7">
			<h1>
				<?php the_field('main_banner_heading'); ?>
				<?php if (get_field('main_happy_logo')): ?>
				<img src="<?php the_field('main_happy_logo'); ?>"  width="150" style="vertical-align: middle;"/>
				<?php endif; ?>
			</h1>
			<?php the_content(); ?>
		</div>
	</div>	
</div>

<div class="section section-main-content section-makes-happy-main-content">
	<div class="container">
	<h1>
		<?php the_field('we_make_you_happy_heading'); ?>
		<?php if (get_field('we_make_you_happy_logo')): ?>
		<img src="<?php the_field('we_make_you_happy_logo'); ?>"  width="120" style="vertical-align: middle;"/>
		<?php endif; ?>
	</h1>
	<?php
	// check if the repeater field has rows of data
	if( have_rows('we_make_you_happy') ):
		

		?>
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<?php // loop through the rows of data
			$row = 0;
			while ( have_rows('we_make_you_happy') ) : the_row();
				$title = get_sub_field('title');

				$first_char = substr($title, 0, 1);
				$new_title =  '<span class="first-char">'. $first_char . '</span>'. substr($title, 1, strlen($title));
			?>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading-<?php echo $row; ?>">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $row; ?>" aria-expanded="true" aria-controls="collapse-<?php echo $row; ?>">
							<span class="triangle">&nbsp;</span><?php echo $new_title; ?>
						</a>
					</h4>
				</div>
				<div id="collapse-<?php echo $row; ?>" class="panel-collapse collapse <?php echo $row == 0 ? 'in' : ''; ?>" role="tabpanel" aria-labelledby="heading-<?php echo $row; ?>">
					<div class="panel-body">
					<?php the_sub_field('content'); ?>
					</div>
				</div>
			</div>
			<?php 
			$row++;
			endwhile; ?>
			
		</div>
		<?php endif;?>
	</div>	
</div>
<?php 
endwhile;
get_footer();?>