<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); 

global $post, $product;


?>



<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(). '/assets/fancybox/jquery.fancybox.css'; ?>" />  


<?php if (product_isdeal($product->id)) { ?>
<script type='text/javascript' src='/wp-content/plugins/woocommerce/assets/js/flexslider/jquery.flexslider.min.js?ver=2.6.1'></script>  
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(). '/assets/flexslider/flexslider.css'; ?>" />        
<?php } ?>

<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(). '/assets/rcarousel/css/rcarousel.css'; ?>" />
<script src="<?php bloginfo('stylesheet_directory'); ?>/assets/rcarousel/lib/jquery.ui.widget.js"></script> 
<script src="<?php bloginfo('stylesheet_directory'); ?>/assets/rcarousel/lib/jquery.ui.rcarousel.js"></script> 
<script src="<?php bloginfo('stylesheet_directory'); ?>/assets/elevatezoom-master/jquery.elevatezoom.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/assets/fancybox/jquery.fancybox.pack.js"></script>
                                                                

	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );    
	?>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>

		<?php endwhile; // end of the loop. ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action( 'woocommerce_sidebar' );
	?>

 <script>
(function($) {
$( window ).load(function() {

      <?php if (product_isdeal($product->id)) { ?>  
        
        $('.flexslider').flexslider({
                animation: "slide",
                animationLoop: true,
                slideshowSpeed: 3000,	
                startAt: 0,
                start: function(slider){
                  resize_summary_height();
                }
        });  
        
        <?php } ?>    
        
        resize_summary_height();  

     
});    

$(document).ready(function() {

$("#zoom_01").elevateZoom({gallery:'gallery_01', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true}); 

$("#zoom_01").bind("click", function(e) {  
  var ez =   $('#zoom_01').data('elevateZoom');	
  $.fancybox(ez.getGalleryList());
  return false;
});


$(".hasgallery").rcarousel({
	orientation: "vertical",
        width: 80,
        height: 80,
        visible: 4
});

$( "#ui-carousel-next" )
	.add( "#ui-carousel-prev" )
	.hover(
		function() {
			$( this ).css( "opacity", 0.7 );
		},
		function() {
			$( this ).css( "opacity", 1.0 );
		}
	);					

});




function resize_summary_height() {
	var pageheight = $('.page-wrapper').height();
	if ($('.entry-summary').length) {              
	$('.entry-summary').css('height',pageheight);
	console.log('HERE');	
	}  
	
}  
})(jQuery);
 </script>
<?php get_footer( 'shop' );
                               
/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */      
